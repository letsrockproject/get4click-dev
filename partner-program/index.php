<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage('PARTNER_PROGRAMM'));
?>
<?
$APPLICATION->IncludeComponent('letsrock:welcome', 'partner_program', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:job', '', [
    'TOP_SECTION' => IB_JOB_PARTNERS_TOP_SECTION,
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:histories', 'edge', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:partners', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:request', '', []);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>