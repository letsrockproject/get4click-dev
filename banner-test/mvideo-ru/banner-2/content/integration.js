var w = window;
w.DAS = {
	clientId: '12333',
	cookieName: 'kfp_login',
	url: 'https://static.mvideo.ru/media/js/das.js?v2.3',
	waitForUserId: 'true' === 'true',
	_userId: null,
	_userName: null,
	_loaded: false,

	load: function () {
		var _self = this,
			script = w.parent.document.createElement('script'),
			head = w.parent.document.getElementsByTagName('head')[0];

		if (w.kfp && typeof w.kfp === 'object') {
			_self._loaded = true;
			this._onLoginAttempt();
			return;
		}

		script.src = this.url;
		script.type = 'text/javascript';

		script.onload = script.onreadystatechange = function () {
			if (!_self.loaded && (!this.readyState ||
					this.readyState === 'loaded' || this.readyState === 'complete')) {
				_self._loaded = true;
				_self._onLoginAttempt();
			}
		};
		head.insertBefore(script, head.firstChild);
	},

	setUserId: function (userId) {
		this._userId = userId;
		return this._onLoginAttempt();
	},

	setUserName: function (userName) {
		return this._loaded ? this._setCookie(userName) : this._userName = userName;
	},

	_login: function (userId) {
		var userName = this._getCookie(this.cookieName);
		if (userName) { 
			this._deleteCookie(this.cookieName);
			w.kfp.login(this.clientId, w.kfp.generate_uuid(), userId || userName, userName, 'success');
			delete w.DAS;
		}
	},

	_onLoginAttempt: function () {
		if (this._loaded) {
			if (this._userName) {
				this._setCookie(this._userName);
			}
			if (!this.waitForUserId || (this.waitForUserId && this._userId !== null)) {
				this._login(this._userId);
			}
		}
	},

	_setCookie: function (value) {
		w.kfp.cookies.set(this.cookieName, value);
	},
	_getCookie: function () {
		return w.kfp.cookies.get(this.cookieName);
	},
	_deleteCookie: function () {
		w.kfp.cookies.del(this.cookieName);
	}
};





if ((w.dataLayer && w.dataLayer[0] && w.dataLayer[0].userId) || (~w.location.href.indexOf('/login'))) {
	w.DAS.load();
}
