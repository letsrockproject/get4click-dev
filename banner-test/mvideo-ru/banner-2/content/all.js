(function(w, $){
    try {
        var TM_ID = '2708387', VK_ID = 'VK-RTRG-13886-dpN24';
        var checkJquery = function(callback) {if (window.jQuery) {$=window.jQuery; callback();} else { window.setTimeout(function(){checkJquery(callback);}, 100); }};
        var _loopArray = function(args, cback){for(var i=0,item=args[0];i<args.length;item=args[++i]) if(cback(item,i)===false)break;};
        var _loadImg = function(){for(var i=0;i<arguments.length;i++) (window.Image ? (new Image()) : document.createElement('img')).src = arguments[i];};
        var _matchInString = function(match, testString){return (match instanceof RegExp && match.test(testString)) || (typeof match == "string" && testString.toLowerCase().indexOf(match.toLowerCase())>-1);};
        var waitForObject=function(check_func,callback,ttl){(function _wait(startTime){if(check_func())callback();else setTimeout(function(){((new Date().getTime())-startTime)<=(ttl||5000)&&_wait(startTime);},100);}(new Date().getTime()));};
        var fbq_helper = {
            cache: [], pxInit: false,
            init: function(pixel_id, callback){
                waitForObject(function(){return !!window.fbq;}, function(){
                    if(!fbq_helper.pxInit){
                        fbq('init',pixel_id);
                        fbq('track', "PageView");
                        fbq_helper.pxInit = true;
                        while(fbq_helper.cache.length) fbq_helper.trackCustom(fbq_helper.cache.shift());
                    }
                    callback instanceof Function && callback();
                }, 10000);
            },
            trackCustom: function(event){
                if(fbq_helper.pxInit)fbq('trackCustom', event.replace(/[a-z]{3}:/gi,''));
                else fbq_helper.cache.push(event);
            }
        };
        var twttr_conversion_helper = {
            cache: [], pxInit: false,
            init: function(pixel_id, callback){
                waitForObject(function(){return !!window.twttr;}, function(){
                    if(!twttr_conversion_helper.pxInit){
                        twq('init',pixel_id);
                        twq('track','PageView');
                        twttr_conversion_helper.pxInit = true;
                        while(twttr_conversion_helper.cache.length) twttr_conversion_helper.trackCustom(twttr_conversion_helper.cache.shift());
                        callback instanceof Function && callback();
                    }
                }, 10000);
            },
            trackCustom: function(event){
                if(twttr_conversion_helper.pxInit)twttr.conversion.trackPid(event.replace(/[a-z]{3}:/gi,''), { tw_sale_amount: 0, tw_order_quantity: 0 });
                else twttr_conversion_helper.cache.push(event);
            }
        };
        var _pathMatch = function(){
            for (var i= 0,item=arguments[0];i<arguments.length;item=arguments[++i]){
                if((item instanceof RegExp && item.test(location.pathname)) ||
                    (typeof item == 'string' && location.pathname.indexOf(item) > -1)){
                    return true;
                }
            }
            return false;
        };
        // <!-- Twitter universal website tag code -->
        !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
        },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
        a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');

        // <!-- Rating@Mail.ru counter -->
        window._tmr = window._tmr || [];
        _tmr.push({id: TM_ID, type: "pageView", start: (new Date()).getTime()});
        (function (d, w) {
           var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
           ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
           var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
           if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
        })(document, window);
        var reachGoal = function(){
            for(var i=0,isFbq,isTwq;i<arguments.length;i++){
                if(/(^|=)[\w*\/]{171}-(&pixel_id=\d+)?$/.test(arguments[i])){
                    _loadImg('https://vk.com/rtrg?r='+arguments[i].replace('//vk.com/rtrg?r=',''));
                }
                else if(/^vk-rtrg-\d+-[a-z\d]+$/i.test(arguments[i])){
                   _loadImg('https://vk.com/rtrg?p='+arguments[i]);
                }
                else if(/^vk:/.test(arguments[i]) && typeof VK_ID != 'undefined'){
                    if(/^vk:\d+$/.test(arguments[i])){
                       _loadImg('https://vk.com/rtrg?p='+VK_ID+'&audience='+arguments[i].split('vk:')[1]);
                    }
                    else {
                       _loadImg('https://vk.com/rtrg?p='+VK_ID+'&event='+arguments[i].split('vk:')[1]);
                    }
                }
                else if(/^\/\/www\.facebook\.com/.test(arguments[i])){
                    _loadImg(arguments[i].replace(/&amp;/g,'&'));
                }
                else {
                    isFbq = /fbq:/.test(arguments[i]);
                    isFbq && fbq_helper.trackCustom(arguments[i]);
                    isTwq = /twq:/.test(arguments[i]);
                    isTwq && twttr_conversion_helper.trackCustom(arguments[i]);
                    if(/tmr:/.test(arguments[i])){
                        window._tmr = window._tmr || [];
                        _tmr.push({ id: TM_ID, type: 'reachGoal', goal: arguments[i].replace(/[a-z]{3}:/gi,'')});
                    }
                }
            }
        };
        (function(){
            var AB = {
                push: function(){
                    for(var i=0,item=arguments[0];i<arguments.length;item=arguments[++i]){
                        if('type' in item){
                            if('basketView' == item['type']){
                                if(item['callback']) AB._init_basket(item['callback']);
                            }
                            else if('itemView' == item['type']) AB._tmr_remarket(item);
                        }
                        else if(item && item.length == 2){
                            AB.order_success.apply(this, item);
                        }
                    }
                },
                _send_retarget_multilist:function(list, callback){
                    var runs = 0,
                    _send = function(){
                        if(callback(list.shift()) === false || !list.length) return;
                        runs += 1;
                        window.setTimeout(function(){ _send();},(runs % 5 === 0?1300:300));
                    };
                    _send();
                },
                _init_basket:function(c){ c(reachGoal, _loadImg, checkJquery, _loopArray, waitForObject, fbq_helper, _matchInString);},
                _tmr_remarket: function(data){
                    AB._send_retarget_multilist([6,5,3,4], function(list){
                        data['id']='2708387';
                        data['list']= list;
                        window._tmr = window._tmr || [];
                        _tmr.push(data);
                    });
                },
                order_success: function(orderNum, orderSum){
                   try {
                       reachGoal('order-success','TZc3md8lXpgEVXfTAMqkEtkFdHzeSFxZoZUbvZHFZO5TLUORzKSh1aZlldh2ORuqt*XDmczuHRglJgGvhkfuLjn78iOg*ZKvmEWlGz13LuJXNR84E2hsVJRYBdnDqswWiO3focGqXcaXdAFlEVrKpMwycJXvlbJme4uY06Tb1oI-');
                       orderSum = parseInt(orderSum) || '0';
                       orderNum = orderNum || '';
                       var script = document.createElement('script');
                       script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +
                               unescape('bn.adblender.ru%2Fpixel.js%3Fclient%3Dmvideo%26cost%3D') + escape(orderSum) +
                               unescape('%26order%3D') + escape(orderNum) + unescape('%26r%3D') + Math.random();
                       document.getElementsByTagName('head')[0].appendChild(script);
                   } catch (e) {}
               }
            };
            AB.push.apply(this, (Object.prototype.toString.call(w.Adblender) === '[object Array]' && w.Adblender.slice()) || []);
            window.Adblender = AB;
        }());
        reachGoal('tmr:all-pages', VK_ID);
        var main = function(){
            var reachGoalByCategoryOrUrl = (function(){
             var matchString = location.pathname + decodeURIComponent(location.search) + $('.c-breadcrumbs__list .c-breadcrumbs__item').text().toLowerCase().replace(/\s+/g,' ');
             matchString += $('.c-breadcrumbs__list a.c-breadcrumbs__item-link')
                 .map(function(ind, elm){
                     return $(elm).attr('href');
                 }).get().join('|').toLowerCase();

             return function(){
                 _loopArray(arguments, function(item){
                     if(_matchInString(item[0], matchString)){
                         reachGoal.apply(this, item[1]);
                     }
                 });
             };
            }());

            reachGoalByCategoryOrUrl(
             ['Телевизоры', ['tmr:tv','twq:nvqml','bZvjjmBW4D5o6sb0DsJnq1JIFAkOWTts3Jk8wg6r6eX1PYvIRINU99C7l*49MHoLcwzvztlou0M*CJZYqmIHdQuKgHt9Fqn0sKdHz4CRSSefnALNR9dOm1d9n7MdkIoybsCZVdsKda*KmlT29IVXj2wMT6doyXfi9ihLdplPoNA-']],
             ['Холодильники',['twq:nvqn2','holodilniki','sypbFtZw5rVLFufjs9QDXAA3V2CCrnNYO3L9Di0Jpy2t0K*gnsq8kXmj1YJApDuBaINkxB4xqmYS*j3XqLqno8*2rwCMSJveCUV9/tTqJ92EL/uSlpiSxhy46kKeyfsAN6PrhTRoPO*7cmm9i7RjMVvKWVG1Oaf3/mRgnRwWjBQ-']],
             ['Морозильные камеры',['tmr:morozilnye-kamery','k*7zgrrf4fn93HQ/1DmSg/0/Ydc1UVI9u1jWkBN1Xd*DnKJC0DvRrk1PNv7HduPnxPpe3dspGrM69tbBfFqJZ/2VrWLVZ1HymtHs649h2HAvCc5S5Nh2I0uywIP*C3oyQ92L6sNjEvxOP6iulpiS3f2sJQy6vacbaeroINYJ6d0-']],
             ['Ноутбуки',['twq:nvqmq','tmr:notebooks','YmaoVCwmbP3CgrFXMhxC0r0B*XQpTrrVDM/ciNgoRaIb38LAwXXPjlW13oRNyV2NPhROI0jHB9FoZA149fPBOVg*lt0bjSgImY/0F2czDs2YZpWJcqe6loJ53RMPN30YGnQfa1ViVg7JoszZbVW7l8LHmH6JOXVtd82xr7GE6t0-']],
             ['Стиральные и сушильные машины',['twq:nvqn3','stiralnye-i-sushilnye','Zw5DNRTOCr4ri5Iknimz4WKhTOloPWTBYV5o/dTD7XDCT2cmWWizqUPOyjG4CWIuzbeb6v4VVGh5ShNs*3FqzhjL0f5ZsO/YLsLWwQ0XNfOmvilfyl2Hp5TuY0l9TBFJodLt9CkYx7R5BJFBds5Ekd6Os7wy9nzZhzkGtsfCLWk-']],
             ['Смартфоны',['tmr:smartphones','EJP5P2D9b0jQ/9I18Lxv8YUZxXbx3jhgo3e2k1UWIsGkSl6V8roee0swYq05K2*gXnjKvQq4K3tYvOf6xHqNw3d32qnT0fveJeuXYe5ekUWMKRJVb2wR4T7abDSpkmekn2WYOZe/bJTB48oltlLXbtzBDQ7d6IGZz5yW5S6xBLQ-']],
             ['Планшеты',['tmr:tablets','iEAMx0XWpQCJo6NWi3Qoy6Sj30oXc9jpI6iLrAIhq37Jzrd36EM7fEEzqA//p5CyA3X2TCipYsFpUHueZp6*ckjVDQ2059QpO8HrPmsIzQBPuz9dFm31MdiHH4ecdCPHds2wTJsQ2nWf5ce5ALXKgQ3*xFwvtaGjNdSW7*uCmfc-']],
             ['Ноутбуки Apple MacBook',['tmr:apple-notebooks','Zu9WiqYaPFcpO2QOFdsUblMh65D0LXZiSeethq1her102BlUUw2JcDkcQP0RSshH8sEH5YE*sr0yt8Sr0NFF4l4bG7bwaGeAg8EKuwnVVF5kbW9KSYzF2jUhC*JT/Erfzc8CALoeIJXd/siViTCV2MWo2QNf*m7PKZgb8IDu9tQ-']],
             ['category=iphone', ['twq:nvqmv','tmr:apple-iphone','Uv1/aBehuqVUTtAos8VK6GyEm25xrzZGY3nRblXQKv1G*fcRz9PbFvvKshyCbfCx2pt3D63ZcgDmhppcX8T*tnRx6FeeuPICu6*NTAjLs7idqbQfsIkHx*QOr1JHHfB6bzzzPqF/*q5NQmNXreRHslQtPRRVBkbjcudyu58OAZw-']],
             ['Планшеты Apple',['tmr:apple-ipad','YbiGIlTUtcbsxCcwL0mQJ81gn/9XlEui7Uu6ZqWTaIyKArFI1FSAl2hjRDEUX5LAP2WpUfZDbWdCcB*3wfEW1NUepH6lVxp2eePK36CVBSNgqQhKd7rUBnxT7FJzLe7kYmSwTlnjlEFYJdMQAASDVy35BSfZfYKu/4ROahSd/2U-']],
             ['apple-iphone-5s',['tmr:iphone-5s','SPCSTEPv3yw*zfSQUrIt5wqVispiKx79voZT9Kmos1HHqn*MC9JSzYqACuypLY39Ee/rSewyD8/bhoksIE74nGB672RMNGiYIUP39oogepORC3hScbXaykN3mn8w/o2rftj2WxfRrwKCWs9*2GjN2lncFoTgE2sZdZGqK3xIFao-']],
             ['apple-iphone-6',['tmr:iphone-6','o0Q4amM589U9xTqagRSwg1p0UYYO9WiDyWJ*jNlbnGX*Qno5m4Qc/BTOXCOE46NnQCD2oJ2YL0F5b/CnKdpDjBPAwtcpR2bKwFtqX39Qh8w730TvjAPY00rCwNFEJWyzVFjEH*iZuDUwMk89CJrS0ObJ5GeBnrs3BAqouBXV/Qw-']],
             ['Телевизоры Samsung',['tmr:tv-samsung','mR/2vAFrNlrl3HhR*jfhMhDnumPMcfajvI2FV5dU7NRXaW3rP55Z7DTD6uffgTVpooDoOnLA8Zu//SlB9BV91gz/TbL2zMJvLf2fL*VWdhw71z4o5v3i4L0UHaXzDqZPt/i9Fmd2/HlEXTHlBtrY/GPT7wMsHX0YZT0u3S3BdKg-']],
             ['Телевизоры LG',['tmr:tv-lg','KWtmEJrHRLgmAnOSxoaJUn2rOjeCSFCdpnm2rasyOR0RLpXxMUzdgSD25DAFnFTwjF1nKlBnOBHH02JWi0SpHEyShXS3jloOZz/6dPKy/2yyyMdulz/DwQeQ*M1t0NxuEeWzp/dEiqpyykxl*uFzmdFTIjqOTQ/QOMnRiAwcBLw-']],
             ['Телевизоры Sony',['tmr:tv-sony','JrBvmZ9TG*cHd35l4LlqRyeBiHcKz7GKKXLO98xSbghJZEAFA3F5*Z*n5lSbD8kFic/fD7ztrXxNhj4HhXKapJSjwCp51HU9ZUQFllZaCOcWTS0iJNf82o2inh6pg8fKdZdx4ExYlHAXvJHoRbrCLCmhvFMNqVhQlq1GHYbsmV8-']],
             ['/purchase',['tmr:purchase-page','UBBSyXR9/TqMCkHTVbMsff78s9nQFcorUG/scCboBg9QE8F4JCQhfVqZmNxT96vRAFcDjndblDnKusXfb5OyYi4ZE6pI*bOw2TI6UXKZXwDsLgLVSGpVwyDpVJreq9vL2vZK376Mc3XpCNMRJMlSVgW2opzx0uqCpotjbi3aEes-']],
             ['Кондиционеры',['tmr:kondicionery','vk:air-conditioning']],
             ['Sony Playstation',['tmr:sony-playstation','bqoyTMHqbj9DPj1x5OP9zlocnRF3Jc7rwN*3TwEb1jYWK0PQWKkiii1c*6cs9y3nsIvL2cH3XwcslXRhP8krhmR/6VH1WNFSEI/fGVeHfiYHREOhcA2Lf01lnRIqfeZF2FTuXVD0jF9LkHzUs8KmdbVuJOtxm13AOhyOrDHmEFk-']],
             ['XBOX',['tmr:xbox','PF8HS4gqjs2wZdQUYB4HPa7YRCRdCS76TMg0b9zIt8V1k0ExchnUCn5t6UXj9Wyp5Gr9NZKAo8f6KUlZM6MQBcnkmP9F/FT5kovX09j62TEXdu/ZgJLcfrRpCkSKHDaohwAJhs4mmv*4b*JjxpLFaqGxSdEFmnSDfZqyqrokK3Y-']],
             ['Игры для PlayStation 4',['tmr:ps4-games','yUAt9cXKGTnfqwKRn1bmdNPU3qrLlYRE2EeYJGXJI6GYeKjK4iCXI5M2lAjdjd1jMYAr8M5LaLqFGDEuNAMsUk6LNThaJRWWcNLah9Y3A*T16BBcSalIy/uLWSxvZL/oMRWyoA1gBknfTW0GmxgsQnZCcQ2NsMpohTxaYgo2GGI-']],
             ['фотоаппараты',['twq:nvqmz','tmr:fbq:digital-cameras','Mb5KEkGkorZNyZApWk2/nEG*gSMgvPwkTrZ1iLgCD4kOqUXFIM9fkxwp/6uVAq34heMWyWN5jbjrF0bhiNA3*vRP8fP2qc857N*9xRn*x1DjprbScOMRfHSS9l5nHuFF4h*Nm51pAu5UevQQTaAEN8U2lW0N7geBwuXeUHazRXI-']],
             ['Acer',['tmr:Acer-products','fbq:acer-products','L/JMx1N9nEHYIrd*PJdFqyU*ACFAqgZZJ*kONNMIj14/82rjWb74pdpJ3mzrH9XTnAtWopdyv3cGpT3viI8eUUhunzG*pgj2DN0HeY2Xg7cxPv5DtVE9TooT/sO0oyQ97qNm1NU90LacNDF1ecdGZwA7HMjerSa/RhhkyCWVZmM-']],
             [/apple-iphone-7|seriya_iphone=iphone7|t=iphone.7/i,['tmr:fbq:iphone7']],
             ['Кофемашин',['tmr:fbq:kofemashina']],
             [/(brand=)?de[-\s]longhi|N=4294965409/i, ['tmr:fbq:kofemashina-delonghi']],
             ['Кухонные комбайны',['tmr:fbq:kuhonnye-mashiny']],
             ['Hi-Fi техника',['tmr:fbq:hifi-tehnica','IyLCNJ9/Gkzu9B2WV5K0gbahEQf/X1IPmApnJ6j0u0q7y5pMFZQczfCIUPHy1h7pvNU9ocI4H2Np3iz23RyhsRTIrVsS1s1MUV4u5F1pnKRMh8iWjSVSsuhPastovpzZlIBz2b/rjWAvPFREUnT9DeYgvkDlXXErsQXbsPMTSgg-&pixel_id=1000035513']],
             ['Комплекты Hi-Fi',['tmr:fbq:hifi-complekty','Pc8TA05dLoylZF3re5dPE4ioR*KF1Isgkzmhb693nvxJ/Dst4wED5Z4cyXuG*RfRsd6ZIQkj0g8*fPy3dKKdhRQx4Y1EqUDaKcHfRJPSu5uRgWrtV0KKqR4gymhRriIDtJ7bCnHECgDrJVyfLeG9DgllYLKtjVL96Gq576LKY94-&pixel_id=1000035514']],
             ['Крупная кухонная техника',['twq:nvqn6','XQe05*xpomTvfNBFDtqVj*q1iii9bf47TAW4mSBg70dWd2ClDtc9fgtuq3KzFBmTiU/ajkEPTPvv0*kQ6e5hsqNz8WSFvF/L6o67U5MxEpLeoZCUXP88aN3zkXKUfmYyaAGmIfzBEI6eckvgUjBorERzThSliRMsk9SO/7C/F5E-&pixel_id=1000042131']],
             ['Встраиваемая техника',['twq:nvqn4','PIYIpov3gsL3KeXOhZNKtaexxQEd0Js09/cXpoe2Y87Tl7xhH955l7glTW1G9VzFNhBebvsKDZtkfFH2IIAeIoDxuU2ZVMQyNw5YcoJWRQVEMD4IacnkmnsSCw9iiiSV2JmZEQbRYEMPW2/U1o18fqZXeYogXFnSEoSnIdvXXNQ-&pixel_id=1000042134']],
             ['/melkaya-kuhonnaya-tehnika-3',['twq:nvqnb','hPYSJgfzXN6e3LcToyxiBcj/9JPxW6G4m5CBQorjbfmTiY74r8oAa5Xj/kEc3aOubV/KfV2CKJ9BPEWtiNOvalgshrZtZWAj/o3O7nxj569SkDsyc/tf0viau443oLenxdE3WhjxM6GXmD9Oqws*xDJHSenv8jThIKbBrIy5lRU-&pixel_id=1000042141']],
             ['Климатическая техника',['twq:nvqn7','GHq8/U12k4PajF**FdZAMRHYTWQUyOw1nIPwYy/nWobyCdkSvboorrRLzsoavoVmyQQLP6fkie/3j6mZY1uu/DAjL/J9/ef2ux0UKFeYpGlRVRdDsNJs6I3WxCL1ZkNU6jS7*cYkDnHjDuPOIeiW*x*G0tjSvV6R9ZcwZkA3N90-&pixel_id=1000042902']],
             ['Товары для красоты',['twq:nvqnf','u/djo3l0VbM75vCjFwFYww3V4tKbwQKUiMMDkOxgII5IXGG1p6GVCzyFc2rpqP0Mdu6SNLB*oZ1upzKgCunrSsq1gX4ugDH/cb6kKNqoTTf1LTB869olKKAOFElV52qUMOUCahTmTtubI73tWr/SHZayThTEWy5d3CVN7ffkdBM-&pixel_id=1000042903']],
             ['Товары для здоровья',['twq:nvqng','A5vdNqsX0PIOp8AAes4dTRBnEdniZj3hGN5q6qmy6BNXnkqLQilozGGtC*tbp8*JGL7ndKqnA//3iwxEppav37L3MZ4nEUvhkmFD9lfYIeqGaIpkMeGe8PHl4x5YCIKhpmLrRQ89tVRPgV8dECKw6AF6rMpO1BwCK7fpitrUpf0-&pixel_id=1000042906']],
             ['Товары для укладки волос',['twq:nvqne']],
             ['Товары для ухода за одеждой',['twq:nvqnd','tmr:malaya-bytovaya-tehnika','v2igKo9DqXU*wNKOW88AYSFJdkNJh/0YPas9OiD5sLzRlKuKlZBdwCFc*b4faiBkx1gDOBl/eAlyjFYk*0oiFeyfTFEwi6l79pcrcob9PmeKdy1DU3Xta58aNmZR1MSQt9Ih24Ultm5FDpMUa4Ks5xeTVeToJYN9V3pZxeq0p58-&pixel_id=1000042909']],
             ['Приготовление кофе',['twq:nvqnc']],
             ['Аудиотехника',['twq:nvqmm']],
             ['Наушники',['twq:nvqmn']],
             ['Портативное аудио',['twq:nvqmp']],
             ['Компьютерная техника Apple',['twq:nvqms']],
             ['Периферийные устройства',['twq:nvqmt']],
             ['Планшеты и электронные книги',['twq:nvqmr']],
             ['Смартфоны и связь',['twq:nvqmu']],
             ['Гаджеты',['twq:nvqmy']],
             ['Видеокамеры',['twq:nvqn1']],
             ['Пылесосы',['twq:nvqna']],
             ['Apple Watch',['vk:apple-watch']],
             ['Apple',['twq:nvqo3']],
             ['Игры и развлечения',['twq:nvqo2']],
             ['Навигаторы и автомобильная электроника',['twq:nvqo1']]
            );

            fbq_helper.init('1066687053377349', function(){
                if(window.dataLayer){
                    var pageInfo;
                    _loopArray(dataLayer, function(item){
                        if(item.pageType) pageInfo = item;
                    });
                    var citiesDict = {'Москва':'moscow','С.-Петербург':'spb'},
                        currentCity = '_' + citiesDict[pageInfo.cityName] || '';

                    var getIDs = function(ignoreCity){
                        var e = window.ecommerceData && ecommerceData.ecommerce.impressions;
                        return e && e.length && e.map(function(elm){return ignoreCity?elm.id:elm.id}) || [];
                    };

                    if(pageInfo){
                        if(pageInfo.pageType == 'ProductListing'){
                            twq('track','ViewContent',{
                                content_type: 'product_group',
                                content_ids: getIDs(true),
                                content_name: pageInfo.productCategoryName,
                                content_category: pageInfo.productGroupName,
                                currency: 'RUB'
                            });
                        }
                        else if(pageInfo.pageType == 'ProductPage'){
                            fbq('track','ViewContent',{
                                content_name: pageInfo.productName,
                                content_ids: [pageInfo.productId],
                                content_type: 'product',
                                value: pageInfo.productPriceLocal,
                                currency: 'RUB'
                            });
                            twq('track','ViewContent',{
                                content_type: 'product',
                                content_ids: [pageInfo.productId],
                                content_name: pageInfo.productName,
                                content_category: pageInfo.productGroupName,
                                value: pageInfo.productPriceLocal,
                                currency: 'RUB'
                            });
                        }
                        else if (pageInfo.pageType == 'Checkout'){
                            fbq('track','InitiateCheckout');
                            twq('track','InitiateCheckout',{
                                content_ids: pageInfo.checkoutProducts.map(function(elm){return elm.productId;}),
                                value: pageInfo.checkoutAmount + '',
                                currency: 'RUB',
                                num_items: pageInfo.checkoutProducts.length
                            });
                        }
                        else if(pageInfo.pageType == 'ThankYouPage'){
                            var trans = pageInfo.transactions[0];
                            var products = trans.transactionProducts.length && trans.transactionProducts || [];
                            fbq('track','Purchase',{
                                  content_ids: products.map(function(elm){return elm.id;}),
                                  content_type: 'product',
                                  value: trans.transactionTotal,
                                  currency: 'RUB',
                                  order_id: trans.transactionId
                            });
                            twq('track','Purchase',{
                                  content_ids: products.map(function(elm){return elm.id;}),
                                  content_type: 'product',
                                  value: trans.transactionTotal +'',
                                  currency: 'RUB',
                                  order_id: trans.transactionId
                            });
                        }
                    }
                }
                if(_pathMatch('/confirmation')){
                    waitForObject(
                      function(){return window.APRT_DATA && APRT_DATA.purchasedProducts && APRT_DATA.purchasedProducts.length;},
                      function(){
                        var reachGoalByProductName = (function(){
                            var productNames = $.map(window.APRT_DATA.purchasedProducts, function(elm, index){ return elm.name;}).join('|');
                            return function(){
                                _loopArray(arguments, function(item){
                                    if(_matchInString(item[0], productNames)){
                                        reachGoal.apply(this, item[1]);
                                    }
                                });
                            };
                        }());

                        reachGoalByProductName(
                            [/(^|\W)Acer(\W|$)/i,['tmr:fbq:acer-order','wIXrvGz6xqwBKkBf4sag*utFlZhQ*Zy1F9GabVJDgfZFRBPU9uncIvkpKKUY3tGb6oYutT*9FrgYvjqMXp1e7tF3X0f4S1QoXadn7utQTmz/WHFEh3B2oKXvw4UHF8mubhNFxd6ofQ7egeflrs7hjYTIENjmIxUCzZiPCKfyIaM-']],
                            [/iphone 7/i,['tmr:fbq:iphone7-order','Kd7f7LSuvWfCavf3OQiT7K1JAZ7TTNSj2FcoO3qoFzUajy5OJDFUHnfPhS5PsjWqJfpsiVkEwi7xBZMJzSOz*nGJmyhvMH5fS2Q5K*vZ/uAEMil01tNp6y5ctivGKwbkB/Fbj4FbcnTRvGLx97*LOHZzmITnMCIWQEOveI1aejs-&pixel_id=1000019066']],
                            [/Планшет/i,['fbq:tablet-order']],
                            [/Пылесос/i,['fbq:pylesos-order']],
                            [/МФУ/i,['fbq:MFU-order']],
                            [/Мультиварка/i,['fbq:multivarka-order']],
                            [/Конвектор|Обогреватель|Радиатор|Тепловентилятор|Электрокамин|Тепловая пушка/i,['fbq:obogrevatelnye-pribory-order']],
                            [/Телевизор/i,['DzDIyr7Nn/2*ZlsOjlY1oIxf8kBQuI2IPmomeAq3EB1FmnHJjU9EyyMEZwaDOCZduI/EB4HLiJo49WUQudiP/X6LeArRphOf85Vbn7adlbWsF*QqvI6hXzrbiTV6RQENGqnJUL3zxyMkS7rprNoJE1yIVEoXSpfzRjQbZtBiTdU-&pixel_id=1000078499']],
                            [/Холодильник/i,['fL*sMZMpZdsKU7lrIjue8owtDF5WHtkhHQyYk706JMnQ9KrilzcU9MBemOB*pjwK10zBRs8B00/7VyuzdEP0pWS5XbNCMtNb1ev2aiylDPqpxMrkfDtT6bdA5KD8KcXet*a8pTnzt9Ulfr0W1ZLkh/7mgXzbBXvqz4f2Ua/CC1Y-&pixel_id=1000078502']],
                            [/Утюг/i,['zVD4i/KXjbqLnv7maywWQzB/hsLBx26GlrKxdmME/JTquIQ9cq3qZcma1On6FzTJRwWHGbNPwqBGBqZKuVYhYqKhcDr3BbMCs*/9WWP1qZibh9amvJIVNNMHllyfwlGdP66xQ8LXOWsDTYuU5iYze6rZInvNHXIJogTqOtScr1s-&pixel_id=1000078503']],
                            [/Блендер/i,['AYIHe5RAXbvWzi5UFxC2oYgcPEALKMLAgCBHZhqmkxIkEgctRASGeYxxtFFoEkYgUmJJXtmpZSGKH88N5JT0dCvDXzV1ua9d4U*9Vx3oOWFu2Aq3ABgci2973YrT62IH0V8nXMb*dGzaFBvySm4qEqgaxA5BcQ2v30AFGAuBMO0-&pixel_id=1000078504']],
                            [/Смартфон/i,['DLoUjmjQ9tZv3TxnCkdWAEkUoHv7egOWz6*fCBuUZgQy52qPiaCcovx0j/usJ1la05CeWl7OBCmxlcX1m0UqkPbwYRvyBrt2lUor69mxQuHZDVopfptf8VC78c5iu*ygoloiB3lbaGML/xCZ4Md69JBLw*2phNK*9hX5j9YQnvw-&pixel_id=1000078505']],
                            [/Стиральная машина/i,['yL7Ymre/0WPv6qHPtI3vzW6NYsQJE6d9mGyqUdL4ni1Fc7kEqJ0pRr/XrbMrv6jXhliVfP3lpbdBWAdvkWAe23cwJqhNbyRmYgdnr5uSo9N7foPAl0HvHOt0uTHTgVHsbn06*ZdoWZjDg4sEe/mpc8YNjk6FPSooC4oEF9u226s-&pixel_id=1000078508']]
                        );
                    }, 30000);
                }
            });

            if(location.hostname == 'dream.mvideo.ru'){
                waitForObject(function(){return $('form div.success').is(':visible');}, function(){
                    reachGoal('tmr:dream-form-submitted','vk:dream-form-submitted');
                }, 500000);
            }
            if(_pathMatch(/\/products\/[^\/\?]+-\d+/)){
                reachGoal('tmr:prod-any');
            }
        };
        checkJquery(main);
    }
    catch(e){}
}(window, window.jQuery));
