<html xmlns="http://www.w3.org/1999/xhtml"><head>
<head>
    <meta charset="utf-8">
    <title>Get4Click - STAGING</title>
    <style type="text/css">
        body {
            background-color: ghostwhite;
            padding: 0;
            margin: 0;
        }
        .container-content {
            width: 960px;
            background-color: white;
            height: 100%;
            margin: 0 auto;
            border-left: solid 1px #E0E4E4;
            border-right: solid 1px #E0E4E4;
            text-align: center;
            position: relative;
        }
        .content {
            top: 48%;
            left: 50%;
            height: 340px;
            width: 680px;
            position: absolute;
            margin: -170px 0 0 -340px;
        }
        .container-logo {
            padding: 20px;
        }
        h1 {
            color: #676976;
            font-family: "Helvetica Neue", Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>

<div class="container-content">
    <div style="height: 100%">
        <div class="content">
            <div class="container-logo">
                <img src="http://get4click.ru/wp-content/themes/get4click/img/logo.png" alt="logo Get4Click">
            </div>
            <div>
                <h1>STAGING - Banner-Test</h1>
            </div>
        </div>
    </div>
</div>

</body>
</html>