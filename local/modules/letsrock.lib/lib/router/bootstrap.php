<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['ft']) && $_POST['ft'] == 'ws') {
        $name  = isset($_POST['ws_n']) ? $_POST['ws_n'] : '';
        $email = isset($_POST['ws_e']) ? $_POST['ws_e'] : '';

        // send request
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_URL, 'https://api.esv2.com/v2/Api/Subscribers/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml',
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, getAddSubscriberTemplate($email, $name));

        $response = curl_exec($ch);

        curl_close($ch);

        echo json_encode(array('success' => '1'));

    } else {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/modules/letsrock.lib/lib/router/mail.php");

        $admin_email = COption::GetOptionString('main','email_from');

        $mail = new Mail();
        //$mail->protocol = 'mail';
        $mail->protocol = 'smtp';
        $mail->smtp_hostname = 'tls://email-smtp.eu-west-1.amazonaws.com';
        $mail->smtp_username = 'AKIAISD3G6GUAWV6WQVA';
        $mail->smtp_password = 'AvUd+Pu5GfhBN2TTT214s5RRWUQ9bKesyUJSRmOm5vr7';
        $mail->smtp_port = 587;
        $mail->smtp_timeout = 5;

        //if ( $_SERVER['REMOTE_ADDR'] == '83.234.201.239' ) {
        //    $mail->setTo('r.knyazev@corp.kokocgroup.ru');
        //} else {
            $mail->setTo($admin_email);
        //}
        $mail->setFrom('no-reply@get4click.ru');
        $mail->setSender('Get4Click');
        $mail->setSubject('Запрос с сайта');
        //$mail->setReplyTo('no-reply@get4click.ru');
        $mail->setHtml(getTemplateMsg($_POST));
        $mail->send();

        echo json_encode(array('success' => '1'));
    }

}

/**
 * @param string $email
 * @param string $name
 * @return string
 */
function getAddSubscriberTemplate($email, $name)
{
    return '<ApiRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                <ApiKey>qL4xOASPdlJ3x3zqN6Un</ApiKey>
                <Data xsi:type="Subscriber">
                   <Mode>AddAndUpdate</Mode>
                   <Force>true</Force>
                   <ListId>67</ListId>
                   <Email>' . $email . '</Email>
                   <Firstname>' . $name . '</Firstname>
                   <Vendor>Get4Click</Vendor>
                </Data>
             </ApiRequest>';
}

/**
* @var array $params
* @return string
**/
function getTemplateMsg($params)
{
	if (isset($params['THEME'])) {
		switch ($params['THEME']) {
			case '1':
				$params['THEME'] = 'Клиентские рассылки';
				break;
			case '2':
				$params['THEME'] = 'Рост продаж';
				break;
			case '3':
				$params['THEME'] = 'Программа лояльности';
				break;
			case '4':
				$params['THEME'] = 'Сотрудничество';
				break;
			case '5':
				$params['THEME'] = 'Подписка на рассылки';
				break;
			default:
				break;
		}
	}

	$tmpl = '<div style="width:100%;height:100%;display:flex;justify-content:center;">' . PHP_EOL;
	$tmpl .= '<table style="background:#8EBA5B;align-self:center;border-radius:10px;padding:10px;">' . PHP_EOL;
	foreach ($params as $name => $value) {
		$tmpl .= '<tr>' . PHP_EOL;
		$tmpl .= '<td style="text-align:right; padding: 5px;">' . getFieldName($name) . '</td>' . PHP_EOL;
		$tmpl .= '<td style="padding:5px;border-bottom:1px solid #fff;">' . htmlspecialcharsEx($value) . '</td>' . PHP_EOL;
		$tmpl .= '</tr>' . PHP_EOL;
	}
	$tmpl .= '</table>' . PHP_EOL;
	$tmpl .= '</div>' . PHP_EOL;

	return $tmpl;
}

/**
* @var string $name
* @return string
**/
function getFieldName($name)
{
	$allowed_fields = array(
		'r_e'	=> 'Email',
		'r_n'	=> 'Name',
		'r_t'	=> 'Phone',
		'r_s'	=> 'Site',
		'r_c'	=> 'Comment',
		'r_th'	=> 'Theme',
		'wd_e'	=> 'Email',
		'wd_n'	=> 'Name',
		'wd_t'	=> 'Phone',
		'wd_th'	=> 'Theme',
		'imr_e'	=> 'Email',
		'imr_n'	=> 'Name',
		'imr_t'	=> 'Phone',
		'imr_s'	=> 'Site',
		'imr_c'	=> 'Comment',
		'imr_th'	=> 'Theme',
		'im_e'	=> 'Email',
		'im_n'	=> 'Name',
		'im_t'	=> 'Phone',
		'im_s'	=> 'Site',
		'im_c'	=> 'Comment',
		'im_th'	=> 'Theme',
		'ws_e'	=> 'Email',
		'ws_n'	=> 'Name',
		'ws_th'	=> 'Theme'
	);
	if (isset($allowed_fields[$name])) {
		return $allowed_fields[$name];
	} else {
		return $name;
	}
}