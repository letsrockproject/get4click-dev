<?php

namespace Letsrock\Lib\Controllers;

use Bitrix\Main\Loader;
use Letsrock\Lib\Models\Form;

Loader::includeModule('iblock');

/*
 * Class FormController
 * Контроллер форм
 */

class FormController
{
    public function formCommon($request)
    {
        $formModel = new Form(IB_REQUEST_MAIN);
        echo $formModel->addMainForm($request);
    }

    public function formSmartBuyers($request)
    {
        $formModel = new Form(IB_REQUEST_SMART_BUYERS);
        echo $formModel->addMainForm($request);
    }

    public function formTasksBuyers($request)
    {
        $formModel = new Form(IB_REQUEST_MAIN);
        echo $formModel->addMainForm($request);
    }
}