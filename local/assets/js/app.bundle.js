/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/scripts/index.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/base-component.js":
/*!***************************************!*\
  !*** ./src/scripts/base-component.js ***!
  \***************************************/
/*! exports provided: BaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return BaseComponent; });
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var BaseComponent =
/*#__PURE__*/
function () {
  function BaseComponent(element) {
    _classCallCheck(this, BaseComponent);

    this.$element = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default()(element);
    this.init();
  }

  _createClass(BaseComponent, [{
    key: "init",
    value: function init() {}
  }]);

  return BaseComponent;
}();



/***/ }),

/***/ "./src/scripts/base-page.js":
/*!**********************************!*\
  !*** ./src/scripts/base-page.js ***!
  \**********************************/
/*! exports provided: BasePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasePage", function() { return BasePage; });
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var BasePage =
/*#__PURE__*/
function () {
  function BasePage(element) {
    var _this = this;

    _classCallCheck(this, BasePage);

    this.$element = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default()('body');
    jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).ready(function ($) {
      _this.init();
    });
  }

  _createClass(BasePage, [{
    key: "init",
    value: function init() {//abstract
    }
  }]);

  return BasePage;
}();



/***/ }),

/***/ "./src/scripts/components/accordeon.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/accordeon.js ***!
  \*********************************************/
/*! exports provided: Accordeon */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Accordeon", function() { return Accordeon; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var Accordeon =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Accordeon, _BaseComponent);

  function Accordeon() {
    _classCallCheck(this, Accordeon);

    return _possibleConstructorReturn(this, _getPrototypeOf(Accordeon).apply(this, arguments));
  }

  _createClass(Accordeon, [{
    key: "init",
    value: function init() {
      var _this = this;

      this.links = this.$element.find(".js-accordeon-link");
      this.items = this.$element.find(".js-accordeon-item");
      this.dot = this.$element.find(".js-accordeon-dot");
      this.imageBlock = this.$element.find(".js-accordeon-image-block");
      this.dotTop = 0;
      this.oldContentHeight = 0;
      this.notWork = true;
      this.setActive(this.links.first());
      this.links.on('click', function (e) {
        var $el = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(e.target);

        if (!$el.hasClass('job__accordeon-item_active')) {
          _this.setActive($el);
        }
      });
    }
  }, {
    key: "setActive",
    value: function setActive($el) {
      var _this2 = this;

      var $currentItem = $el.closest(this.items);

      if (!$currentItem.hasClass('job__accordeon-item_active') && this.notWork) {
        this.notWork = false;
        this.dot.css('transform', 'translateY(' + this.dotTop + 'px) scale(0.5)');
        var $oldImage = this.$element.find(".js-accordeon-image");
        $oldImage.addClass('job__notebook_edit');
        var $newImage = $oldImage.clone().hide().attr('src', $currentItem.data('img'));
        this.dotTop = $currentItem.position().top;

        if ($currentItem.nextAll('.job__accordeon-item_active').length != 1) {
          this.dotTop = this.dotTop - this.oldContentHeight;
        }

        this.items.removeClass('job__accordeon-item_active');
        $el.closest(this.items).addClass('job__accordeon-item_active');
        this.dot.css('transform', 'translateY(' + this.dotTop + 'px) scale(0.5)');
        this.imageBlock.append($newImage);
        $newImage.fadeIn('normal', function () {
          $newImage.removeClass('job__notebook_edit');
          $oldImage.remove();
        });
        setTimeout(function () {
          _this2.dot.css('transform', 'translateY(' + _this2.dotTop + 'px) scale(1)');
        }, 550);
        setTimeout(function () {
          _this2.oldContentHeight = $currentItem.find('.js-accordeon-hidden').outerHeight();
          _this2.notWork = true;
        }, 800);
      }
    }
  }]);

  return Accordeon;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/btn.js":
/*!***************************************!*\
  !*** ./src/scripts/components/btn.js ***!
  \***************************************/
/*! exports provided: Btn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Btn", function() { return Btn; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var Btn =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Btn, _BaseComponent);

  function Btn() {
    _classCallCheck(this, Btn);

    return _possibleConstructorReturn(this, _getPrototypeOf(Btn).apply(this, arguments));
  }

  _createClass(Btn, [{
    key: "init",
    value: function init() {
      var $btn = this.$element.find(".js-btn-inner");
      $btn.mouseenter(function (e) {
        var parentOffset = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).offset();
        var relX = e.pageX - parentOffset.left;
        var relY = e.pageY - parentOffset.top;
        var $circle = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).prev(".btn__circle");
        $circle.css({
          "left": relX,
          "top": relY
        });
        $circle.removeClass("desplode-circle");
        $circle.addClass("explode-circle");
      });
      $btn.mouseleave(function (e) {
        var parentOffset = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).offset();
        var relX = e.pageX - parentOffset.left;
        var relY = e.pageY - parentOffset.top;
        var $circle = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).prev(".btn__circle");
        $circle.css({
          "left": relX,
          "top": relY
        });
        $circle.removeClass("explode-circle");
        $circle.addClass("desplode-circle");
      });
    }
  }]);

  return Btn;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/comments.js":
/*!********************************************!*\
  !*** ./src/scripts/components/comments.js ***!
  \********************************************/
/*! exports provided: Comments */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comments", function() { return Comments; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Comments =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Comments, _BaseComponent);

  function Comments() {
    _classCallCheck(this, Comments);

    return _possibleConstructorReturn(this, _getPrototypeOf(Comments).apply(this, arguments));
  }

  _createClass(Comments, [{
    key: "init",
    value: function init() {
      var res = this.$element.find('.js-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: "<a href=\"javascript:void(0);\" class='partners__prev arrow arrow_prev'></a>",
        nextArrow: "<a href=\"javascript:void(0);\" class='partners__next arrow arrow_next'></a>",
        responsive: [{
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 3,
            infinite: true,
            arrows: false
          }
        }, {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }]
      });
    }
  }]);

  return Comments;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/header.js":
/*!******************************************!*\
  !*** ./src/scripts/components/header.js ***!
  \******************************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Header =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Header, _BaseComponent);

  function Header() {
    _classCallCheck(this, Header);

    return _possibleConstructorReturn(this, _getPrototypeOf(Header).apply(this, arguments));
  }

  _createClass(Header, [{
    key: "init",
    value: function init() {
      var inner = this.$element.find('.js-header-inner');
      var toggleButton = this.$element.find('.js-hamburger');
      toggleButton.on('click', function () {
        inner.toggleClass('header__inner_opened');
        toggleButton.toggleClass("is-active");
      });
    }
  }]);

  return Header;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/histories.js":
/*!*********************************************!*\
  !*** ./src/scripts/components/histories.js ***!
  \*********************************************/
/*! exports provided: Histories */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Histories", function() { return Histories; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Histories =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Histories, _BaseComponent);

  function Histories() {
    _classCallCheck(this, Histories);

    return _possibleConstructorReturn(this, _getPrototypeOf(Histories).apply(this, arguments));
  }

  _createClass(Histories, [{
    key: "init",
    value: function init() {
      this.$element.find('.js-slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        fade: true,
        prevArrow: "<a href=\"javascript:void(0);\" class='histories__prev histories__arrow arrow arrow_prev'></a>",
        nextArrow: "<a href=\"javascript:void(0);\" class='histories__next histories__arrow arrow arrow_next'></a>",
        responsive: [{
          breakpoint: 1200,
          settings: {
            infinite: true,
            arrows: false
          }
        }]
      });
    }
  }]);

  return Histories;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/slider-blog.js":
/*!***********************************************!*\
  !*** ./src/scripts/components/slider-blog.js ***!
  \***********************************************/
/*! exports provided: SliderBlog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderBlog", function() { return SliderBlog; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var SliderBlog =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(SliderBlog, _BaseComponent);

  function SliderBlog() {
    _classCallCheck(this, SliderBlog);

    return _possibleConstructorReturn(this, _getPrototypeOf(SliderBlog).apply(this, arguments));
  }

  _createClass(SliderBlog, [{
    key: "init",
    value: function init() {// this.$element.slick({
      //     infinite: false,
      //     slidesToShow: 1,
      //     slidesToScroll: 1,
      //     arrows: false,
      //     adaptiveHeight: true
      // });
      // let dots = $(`.blog_navigate-item`);
      // dots.each((i, item) => {
      //     $(item).click(() => {
      //         this.deleteAllActive();
      //         $(item).addClass('active');
      //         this.$element.slick('slickGoTo', i)
      //     })
      // });
      // let _this = this;
      // this.$element.on('afterChange', function(event, slick, currentSlide, nextSlide){
      //     _this.deleteAllActive();
      //     dots[currentSlide].classList.add('active')
      // });
    }
  }, {
    key: "deleteAllActive",
    value: function deleteAllActive() {
      var dots = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default()(".blog_navigate-item");
      dots.each(function (i, item) {
        item.classList.remove('active');
      });
    }
  }]);

  return SliderBlog;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/slider.js":
/*!******************************************!*\
  !*** ./src/scripts/components/slider.js ***!
  \******************************************/
/*! exports provided: Slider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Slider", function() { return Slider; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Slider =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Slider, _BaseComponent);

  function Slider() {
    _classCallCheck(this, Slider);

    return _possibleConstructorReturn(this, _getPrototypeOf(Slider).apply(this, arguments));
  }

  _createClass(Slider, [{
    key: "init",
    value: function init() {
      this.$element.slick({
        infinite: false,
        slidesToShow: 7,
        slidesToScroll: 3,
        prevArrow: "<a href=\"javascript:void(0);\" class='partners__prev arrow arrow_prev'></a>",
        nextArrow: "<a href=\"javascript:void(0);\" class='partners__next arrow arrow_next'></a>",
        responsive: [{
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,
            arrows: false
          }
        }, {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: true,
            arrows: false
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }, {
          breakpoint: 450,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }]
      });
    }
  }]);

  return Slider;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/components/welcome.js":
/*!*******************************************!*\
  !*** ./src/scripts/components/welcome.js ***!
  \*******************************************/
/*! exports provided: Welcome */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Welcome", function() { return Welcome; });
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var particles_js_particles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! particles.js/particles */ "./node_modules/particles.js/particles.js");
/* harmony import */ var particles_js_particles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(particles_js_particles__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Welcome =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(Welcome, _BaseComponent);

  function Welcome() {
    _classCallCheck(this, Welcome);

    return _possibleConstructorReturn(this, _getPrototypeOf(Welcome).apply(this, arguments));
  }

  _createClass(Welcome, [{
    key: "init",
    value: function init() {
      // const $bg = this.$element.find(".js-bg");
      particlesJS("js-welcome-bg", {
        "particles": {
          "number": {
            "value": 30,
            "density": {
              "enable": true,
              "value_area": 700
            }
          },
          "color": {
            "value": "#2CB901"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 1,
              "color": "#82ED7B"
            },
            "polygon": {
              "nb_sides": 3
            },
            "image": {
              "src": "img/github.svg",
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.3928359549120531,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 5,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 288.6141709557941,
            "color": "#03C025",
            "opacity": 0.19240944730386272,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 1.603412060865523,
            "direction": "none",
            "random": true,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": true,
              "mode": "grab"
            },
            "onclick": {
              "enable": true,
              "mode": "push"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 400,
              "line_linked": {
                "opacity": 0.2
              }
            },
            "bubble": {
              "distance": 400,
              "size": 40,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      });
    }
  }]);

  return Welcome;
}(_base_component__WEBPACK_IMPORTED_MODULE_0__["BaseComponent"]);



/***/ }),

/***/ "./src/scripts/index.js":
/*!******************************!*\
  !*** ./src/scripts/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_index_less__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../styles/index.less */ "./src/styles/index.less");
/* harmony import */ var _styles_index_less__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_index_less__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _page_index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./page/index.js */ "./src/scripts/page/index.js");



/***/ }),

/***/ "./src/scripts/page/index.js":
/*!***********************************!*\
  !*** ./src/scripts/page/index.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-page */ "./src/scripts/base-page.js");
/* harmony import */ var _components_btn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/btn */ "./src/scripts/components/btn.js");
/* harmony import */ var _components_welcome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/welcome */ "./src/scripts/components/welcome.js");
/* harmony import */ var _components_accordeon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/accordeon */ "./src/scripts/components/accordeon.js");
/* harmony import */ var _components_slider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/slider */ "./src/scripts/components/slider.js");
/* harmony import */ var _components_histories__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/histories */ "./src/scripts/components/histories.js");
/* harmony import */ var _components_comments__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/comments */ "./src/scripts/components/comments.js");
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/header */ "./src/scripts/components/header.js");
/* harmony import */ var _components_slider_blog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/slider-blog */ "./src/scripts/components/slider-blog.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_9__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }












var Index =
/*#__PURE__*/
function (_BasePage) {
  _inherits(Index, _BasePage);

  function Index() {
    _classCallCheck(this, Index);

    return _possibleConstructorReturn(this, _getPrototypeOf(Index).apply(this, arguments));
  }

  _createClass(Index, [{
    key: "init",
    value: function init() {
      this.$element.find('.js-btn').each(function (i, el) {
        new _components_btn__WEBPACK_IMPORTED_MODULE_1__["Btn"](el);
      });
      this.$element.find('.js-accordeon').each(function (i, el) {
        new _components_accordeon__WEBPACK_IMPORTED_MODULE_3__["Accordeon"](el);
      });
      this.$element.find('.js-welcome').each(function (i, el) {
        new _components_welcome__WEBPACK_IMPORTED_MODULE_2__["Welcome"](el);
      });
      this.$element.find('.js-partners-slider').each(function (i, el) {
        new _components_slider__WEBPACK_IMPORTED_MODULE_4__["Slider"](el);
      });
      this.$element.find('.js-histories').each(function (i, el) {
        new _components_histories__WEBPACK_IMPORTED_MODULE_5__["Histories"](el);
      });
      this.$element.find('.js-news').each(function (i, el) {
        new _components_histories__WEBPACK_IMPORTED_MODULE_5__["Histories"](el);
      });
      this.$element.find('.js-comments').each(function (i, el) {
        new _components_comments__WEBPACK_IMPORTED_MODULE_6__["Comments"](el);
      });
      this.$element.find('.js-header').each(function (i, el) {
        new _components_header__WEBPACK_IMPORTED_MODULE_7__["Header"](el);
      });
      this.$element.find('.js-slider-blog').each(function (i, el) {
        new _components_slider_blog__WEBPACK_IMPORTED_MODULE_8__["SliderBlog"](el);
      });
      var maxTitleWord = 35;

      if (document.documentElement.clientWidth <= 400) {
        [].forEach.call(document.querySelectorAll(".card-blog__title"), function (item) {
          if (item.textContent.length >= maxTitleWord) {
            var newStr = item.textContent.slice(0, maxTitleWord);
            item.textContent = newStr + "...";
          }

          ;
        });
      }

      ;

      if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        jquery__WEBPACK_IMPORTED_MODULE_9___default()('.ie-11-img img').each(function () {
          var t = jquery__WEBPACK_IMPORTED_MODULE_9___default()(this),
              s = 'url(' + t.attr('src') + ')',
              p = t.parent(),
              d = jquery__WEBPACK_IMPORTED_MODULE_9___default()('<div></div>');
          p.append(d);
          d.css({
            'height': t.parent().css('height'),
            'background-size': 'cover',
            'background-repeat': 'no-repeat',
            'background-position': '50% 20%',
            'background-image': s
          });
          t.hide();
        });
      }
    }
  }]);

  return Index;
}(_base_page__WEBPACK_IMPORTED_MODULE_0__["BasePage"]);

var page = new Index();

/***/ }),

/***/ "./src/styles/index.less":
/*!*******************************!*\
  !*** ./src/styles/index.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvYmFzZS1jb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvYmFzZS1wYWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL2NvbXBvbmVudHMvYWNjb3JkZW9uLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL2NvbXBvbmVudHMvYnRuLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL2NvbXBvbmVudHMvY29tbWVudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvY29tcG9uZW50cy9oZWFkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvY29tcG9uZW50cy9oaXN0b3JpZXMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvY29tcG9uZW50cy9zbGlkZXItYmxvZy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9jb21wb25lbnRzL3NsaWRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9jb21wb25lbnRzL3dlbGNvbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvcGFnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3R5bGVzL2luZGV4Lmxlc3M/NjU4MiJdLCJuYW1lcyI6WyJCYXNlQ29tcG9uZW50IiwiZWxlbWVudCIsIiRlbGVtZW50IiwiJCIsImluaXQiLCJCYXNlUGFnZSIsIndpbmRvdyIsInJlYWR5IiwiQWNjb3JkZW9uIiwibGlua3MiLCJmaW5kIiwiaXRlbXMiLCJkb3QiLCJpbWFnZUJsb2NrIiwiZG90VG9wIiwib2xkQ29udGVudEhlaWdodCIsIm5vdFdvcmsiLCJzZXRBY3RpdmUiLCJmaXJzdCIsIm9uIiwiZSIsIiRlbCIsInRhcmdldCIsImhhc0NsYXNzIiwiJGN1cnJlbnRJdGVtIiwiY2xvc2VzdCIsImNzcyIsIiRvbGRJbWFnZSIsImFkZENsYXNzIiwiJG5ld0ltYWdlIiwiY2xvbmUiLCJoaWRlIiwiYXR0ciIsImRhdGEiLCJwb3NpdGlvbiIsInRvcCIsIm5leHRBbGwiLCJsZW5ndGgiLCJyZW1vdmVDbGFzcyIsImFwcGVuZCIsImZhZGVJbiIsInJlbW92ZSIsInNldFRpbWVvdXQiLCJvdXRlckhlaWdodCIsIkJ0biIsIiRidG4iLCJtb3VzZWVudGVyIiwicGFyZW50T2Zmc2V0Iiwib2Zmc2V0IiwicmVsWCIsInBhZ2VYIiwibGVmdCIsInJlbFkiLCJwYWdlWSIsIiRjaXJjbGUiLCJwcmV2IiwibW91c2VsZWF2ZSIsIkNvbW1lbnRzIiwicmVzIiwic2xpY2siLCJpbmZpbml0ZSIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwicHJldkFycm93IiwibmV4dEFycm93IiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsImFycm93cyIsIkhlYWRlciIsImlubmVyIiwidG9nZ2xlQnV0dG9uIiwidG9nZ2xlQ2xhc3MiLCJIaXN0b3JpZXMiLCJhZGFwdGl2ZUhlaWdodCIsImZhZGUiLCJTbGlkZXJCbG9nIiwiZG90cyIsImVhY2giLCJpIiwiaXRlbSIsImNsYXNzTGlzdCIsIlNsaWRlciIsIldlbGNvbWUiLCJwYXJ0aWNsZXNKUyIsIkluZGV4IiwiZWwiLCJtYXhUaXRsZVdvcmQiLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsImNsaWVudFdpZHRoIiwiZm9yRWFjaCIsImNhbGwiLCJxdWVyeVNlbGVjdG9yQWxsIiwidGV4dENvbnRlbnQiLCJuZXdTdHIiLCJzbGljZSIsImRvY3VtZW50TW9kZSIsInRlc3QiLCJuYXZpZ2F0b3IiLCJ1c2VyQWdlbnQiLCJqUXVlcnkiLCJ0IiwicyIsInAiLCJwYXJlbnQiLCJkIiwicGFnZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0JBQVEsb0JBQW9CO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQWlCLDRCQUE0QjtBQUM3QztBQUNBO0FBQ0EsMEJBQWtCLDJCQUEyQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQWdCLHVCQUF1QjtBQUN2Qzs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEpBOztJQUVNQSxhOzs7QUFDRix5QkFBWUMsT0FBWixFQUFxQjtBQUFBOztBQUNqQixTQUFLQyxRQUFMLEdBQWdCQyx5REFBQyxDQUFDRixPQUFELENBQWpCO0FBQ0EsU0FBS0csSUFBTDtBQUNIOzs7OzJCQUVNLENBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSYjs7SUFFTUMsUTs7O0FBQ0Ysb0JBQVlKLE9BQVosRUFBcUI7QUFBQTs7QUFBQTs7QUFDakIsU0FBS0MsUUFBTCxHQUFnQkMseURBQUMsQ0FBQyxNQUFELENBQWpCO0FBRUFBLDZEQUFDLENBQUNHLE1BQUQsQ0FBRCxDQUFVQyxLQUFWLENBQWdCLFVBQUNKLENBQUQsRUFBSztBQUNqQixXQUFJLENBQUNDLElBQUw7QUFDSCxLQUZEO0FBR0g7Ozs7MkJBRU0sQ0FDSDtBQUNIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2JMO0FBRUE7O0lBR01JLFM7Ozs7Ozs7Ozs7Ozs7MkJBRUs7QUFBQTs7QUFDSCxXQUFLQyxLQUFMLEdBQWEsS0FBS1AsUUFBTCxDQUFjUSxJQUFkLENBQW1CLG9CQUFuQixDQUFiO0FBQ0EsV0FBS0MsS0FBTCxHQUFhLEtBQUtULFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixvQkFBbkIsQ0FBYjtBQUNBLFdBQUtFLEdBQUwsR0FBVyxLQUFLVixRQUFMLENBQWNRLElBQWQsQ0FBbUIsbUJBQW5CLENBQVg7QUFDQSxXQUFLRyxVQUFMLEdBQWtCLEtBQUtYLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQiwyQkFBbkIsQ0FBbEI7QUFDQSxXQUFLSSxNQUFMLEdBQWMsQ0FBZDtBQUNBLFdBQUtDLGdCQUFMLEdBQXdCLENBQXhCO0FBQ0EsV0FBS0MsT0FBTCxHQUFlLElBQWY7QUFFQSxXQUFLQyxTQUFMLENBQWUsS0FBS1IsS0FBTCxDQUFXUyxLQUFYLEVBQWY7QUFFQSxXQUFLVCxLQUFMLENBQVdVLEVBQVgsQ0FBYyxPQUFkLEVBQXVCLFVBQUNDLENBQUQsRUFBTztBQUMxQixZQUFNQyxHQUFHLEdBQUdsQix5REFBQyxDQUFDaUIsQ0FBQyxDQUFDRSxNQUFILENBQWI7O0FBQ0EsWUFBSSxDQUFDRCxHQUFHLENBQUNFLFFBQUosQ0FBYSw0QkFBYixDQUFMLEVBQWlEO0FBQzdDLGVBQUksQ0FBQ04sU0FBTCxDQUFlSSxHQUFmO0FBQ0g7QUFDSixPQUxEO0FBTUg7Ozs4QkFFU0EsRyxFQUFLO0FBQUE7O0FBQ1gsVUFBTUcsWUFBWSxHQUFHSCxHQUFHLENBQUNJLE9BQUosQ0FBWSxLQUFLZCxLQUFqQixDQUFyQjs7QUFFQSxVQUFJLENBQUNhLFlBQVksQ0FBQ0QsUUFBYixDQUFzQiw0QkFBdEIsQ0FBRCxJQUF3RCxLQUFLUCxPQUFqRSxFQUEwRTtBQUN0RSxhQUFLQSxPQUFMLEdBQWUsS0FBZjtBQUNBLGFBQUtKLEdBQUwsQ0FBU2MsR0FBVCxDQUFhLFdBQWIsRUFBMEIsZ0JBQWdCLEtBQUtaLE1BQXJCLEdBQThCLGdCQUF4RDtBQUNBLFlBQU1hLFNBQVMsR0FBRyxLQUFLekIsUUFBTCxDQUFjUSxJQUFkLENBQW1CLHFCQUFuQixDQUFsQjtBQUNBaUIsaUJBQVMsQ0FBQ0MsUUFBVixDQUFtQixvQkFBbkI7QUFDQSxZQUFNQyxTQUFTLEdBQUdGLFNBQVMsQ0FBQ0csS0FBVixHQUFrQkMsSUFBbEIsR0FBeUJDLElBQXpCLENBQThCLEtBQTlCLEVBQXFDUixZQUFZLENBQUNTLElBQWIsQ0FBa0IsS0FBbEIsQ0FBckMsQ0FBbEI7QUFDQSxhQUFLbkIsTUFBTCxHQUFjVSxZQUFZLENBQUNVLFFBQWIsR0FBd0JDLEdBQXRDOztBQUVBLFlBQUlYLFlBQVksQ0FBQ1ksT0FBYixDQUFxQiw2QkFBckIsRUFBb0RDLE1BQXBELElBQThELENBQWxFLEVBQXFFO0FBQ2pFLGVBQUt2QixNQUFMLEdBQWMsS0FBS0EsTUFBTCxHQUFjLEtBQUtDLGdCQUFqQztBQUNIOztBQUVELGFBQUtKLEtBQUwsQ0FBVzJCLFdBQVgsQ0FBdUIsNEJBQXZCO0FBQ0FqQixXQUFHLENBQUNJLE9BQUosQ0FBWSxLQUFLZCxLQUFqQixFQUF3QmlCLFFBQXhCLENBQWlDLDRCQUFqQztBQUNBLGFBQUtoQixHQUFMLENBQVNjLEdBQVQsQ0FBYSxXQUFiLEVBQTBCLGdCQUFnQixLQUFLWixNQUFyQixHQUE4QixnQkFBeEQ7QUFDQSxhQUFLRCxVQUFMLENBQWdCMEIsTUFBaEIsQ0FBdUJWLFNBQXZCO0FBRUFBLGlCQUFTLENBQUNXLE1BQVYsQ0FBaUIsUUFBakIsRUFBMkIsWUFBTTtBQUM3QlgsbUJBQVMsQ0FBQ1MsV0FBVixDQUFzQixvQkFBdEI7QUFDQVgsbUJBQVMsQ0FBQ2MsTUFBVjtBQUNILFNBSEQ7QUFLQUMsa0JBQVUsQ0FBQyxZQUFNO0FBQ2IsZ0JBQUksQ0FBQzlCLEdBQUwsQ0FBU2MsR0FBVCxDQUFhLFdBQWIsRUFBMEIsZ0JBQWdCLE1BQUksQ0FBQ1osTUFBckIsR0FBOEIsY0FBeEQ7QUFDSCxTQUZTLEVBRVAsR0FGTyxDQUFWO0FBS0E0QixrQkFBVSxDQUFDLFlBQU07QUFDYixnQkFBSSxDQUFDM0IsZ0JBQUwsR0FBd0JTLFlBQVksQ0FBQ2QsSUFBYixDQUFrQixzQkFBbEIsRUFBMENpQyxXQUExQyxFQUF4QjtBQUNBLGdCQUFJLENBQUMzQixPQUFMLEdBQWUsSUFBZjtBQUNILFNBSFMsRUFHUCxHQUhPLENBQVY7QUFLSDtBQUVKOzs7O0VBMURtQmhCLDZEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTHhCO0FBQ0E7O0lBR000QyxHOzs7Ozs7Ozs7Ozs7OzJCQUVLO0FBQ0gsVUFBTUMsSUFBSSxHQUFHLEtBQUszQyxRQUFMLENBQWNRLElBQWQsQ0FBbUIsZUFBbkIsQ0FBYjtBQUVBbUMsVUFBSSxDQUFDQyxVQUFMLENBQWdCLFVBQVUxQixDQUFWLEVBQWE7QUFDekIsWUFBTTJCLFlBQVksR0FBRzVDLHlEQUFDLENBQUMsSUFBRCxDQUFELENBQVE2QyxNQUFSLEVBQXJCO0FBQ0EsWUFBTUMsSUFBSSxHQUFHN0IsQ0FBQyxDQUFDOEIsS0FBRixHQUFVSCxZQUFZLENBQUNJLElBQXBDO0FBQ0EsWUFBTUMsSUFBSSxHQUFHaEMsQ0FBQyxDQUFDaUMsS0FBRixHQUFVTixZQUFZLENBQUNaLEdBQXBDO0FBQ0EsWUFBTW1CLE9BQU8sR0FBR25ELHlEQUFDLENBQUMsSUFBRCxDQUFELENBQVFvRCxJQUFSLENBQWEsY0FBYixDQUFoQjtBQUNBRCxlQUFPLENBQUM1QixHQUFSLENBQVk7QUFBQyxrQkFBUXVCLElBQVQ7QUFBZSxpQkFBT0c7QUFBdEIsU0FBWjtBQUNBRSxlQUFPLENBQUNoQixXQUFSLENBQW9CLGlCQUFwQjtBQUNBZ0IsZUFBTyxDQUFDMUIsUUFBUixDQUFpQixnQkFBakI7QUFDSCxPQVJEO0FBVUFpQixVQUFJLENBQUNXLFVBQUwsQ0FBZ0IsVUFBVXBDLENBQVYsRUFBYTtBQUN6QixZQUFNMkIsWUFBWSxHQUFHNUMseURBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZDLE1BQVIsRUFBckI7QUFDQSxZQUFNQyxJQUFJLEdBQUc3QixDQUFDLENBQUM4QixLQUFGLEdBQVVILFlBQVksQ0FBQ0ksSUFBcEM7QUFDQSxZQUFNQyxJQUFJLEdBQUdoQyxDQUFDLENBQUNpQyxLQUFGLEdBQVVOLFlBQVksQ0FBQ1osR0FBcEM7QUFDQSxZQUFNbUIsT0FBTyxHQUFHbkQseURBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9ELElBQVIsQ0FBYSxjQUFiLENBQWhCO0FBQ0FELGVBQU8sQ0FBQzVCLEdBQVIsQ0FBWTtBQUFDLGtCQUFRdUIsSUFBVDtBQUFlLGlCQUFPRztBQUF0QixTQUFaO0FBQ0FFLGVBQU8sQ0FBQ2hCLFdBQVIsQ0FBb0IsZ0JBQXBCO0FBQ0FnQixlQUFPLENBQUMxQixRQUFSLENBQWlCLGlCQUFqQjtBQUNILE9BUkQ7QUFVSDs7OztFQXpCYTVCLDZEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKbEI7QUFFQTtBQUNBOztJQUdNeUQsUTs7Ozs7Ozs7Ozs7OzsyQkFDSztBQUNILFVBQUlDLEdBQUcsR0FBRyxLQUFLeEQsUUFBTCxDQUFjUSxJQUFkLENBQW1CLFlBQW5CLEVBQWlDaUQsS0FBakMsQ0FBdUM7QUFDN0NDLGdCQUFRLEVBQUUsS0FEbUM7QUFFN0NDLG9CQUFZLEVBQUUsQ0FGK0I7QUFHN0NDLHNCQUFjLEVBQUUsQ0FINkI7QUFJN0NDLGlCQUFTLEVBQUUsOEVBSmtDO0FBSzdDQyxpQkFBUyxFQUFFLDhFQUxrQztBQU03Q0Msa0JBQVUsRUFBRSxDQUNSO0FBQ0lDLG9CQUFVLEVBQUUsSUFEaEI7QUFFSUMsa0JBQVEsRUFBRTtBQUNOTix3QkFBWSxFQUFFLENBRFI7QUFFTkMsMEJBQWMsRUFBRSxDQUZWO0FBR05GLG9CQUFRLEVBQUUsSUFISjtBQUlOUSxrQkFBTSxFQUFFO0FBSkY7QUFGZCxTQURRLEVBVVI7QUFDSUYsb0JBQVUsRUFBRSxHQURoQjtBQUVJQyxrQkFBUSxFQUFFO0FBQ05OLHdCQUFZLEVBQUUsQ0FEUjtBQUVOQywwQkFBYyxFQUFFLENBRlY7QUFHTkYsb0JBQVEsRUFBRSxJQUhKO0FBSU5RLGtCQUFNLEVBQUU7QUFKRjtBQUZkLFNBVlE7QUFOaUMsT0FBdkMsQ0FBVjtBQTJCSDs7OztFQTdCa0JwRSw2RDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTnZCO0FBRUE7QUFDQTs7SUFHTXFFLE07Ozs7Ozs7Ozs7Ozs7MkJBRUs7QUFDSCxVQUFNQyxLQUFLLEdBQUcsS0FBS3BFLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixrQkFBbkIsQ0FBZDtBQUNBLFVBQU02RCxZQUFZLEdBQUcsS0FBS3JFLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixlQUFuQixDQUFyQjtBQUVBNkQsa0JBQVksQ0FBQ3BELEVBQWIsQ0FBZ0IsT0FBaEIsRUFBeUIsWUFBTTtBQUMzQm1ELGFBQUssQ0FBQ0UsV0FBTixDQUFrQixzQkFBbEI7QUFDQUQsb0JBQVksQ0FBQ0MsV0FBYixDQUF5QixXQUF6QjtBQUNILE9BSEQ7QUFJSDs7OztFQVZnQnhFLDZEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOckI7QUFFQTtBQUNBOztJQUdNeUUsUzs7Ozs7Ozs7Ozs7OzsyQkFFSztBQUNILFdBQUt2RSxRQUFMLENBQWNRLElBQWQsQ0FBbUIsWUFBbkIsRUFBaUNpRCxLQUFqQyxDQUF1QztBQUNuQ0MsZ0JBQVEsRUFBRSxLQUR5QjtBQUVuQ0Msb0JBQVksRUFBRSxDQUZxQjtBQUduQ0Msc0JBQWMsRUFBRSxDQUhtQjtBQUluQ1ksc0JBQWMsRUFBRSxJQUptQjtBQUtuQ0MsWUFBSSxFQUFFLElBTDZCO0FBTW5DWixpQkFBUyxFQUFFLGdHQU53QjtBQU9uQ0MsaUJBQVMsRUFBRSxnR0FQd0I7QUFRbkNDLGtCQUFVLEVBQUUsQ0FDUjtBQUNJQyxvQkFBVSxFQUFFLElBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTlAsb0JBQVEsRUFBRSxJQURKO0FBRU5RLGtCQUFNLEVBQUU7QUFGRjtBQUZkLFNBRFE7QUFSdUIsT0FBdkM7QUFrQkg7Ozs7RUFyQm1CcEUsNkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ054QjtBQUVBO0FBQ0E7O0lBR000RSxVOzs7Ozs7Ozs7Ozs7OzJCQUVLLENBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNIOzs7c0NBSWlCO0FBQ2QsVUFBSUMsSUFBSSxHQUFHMUUseURBQUMsdUJBQVo7QUFDQTBFLFVBQUksQ0FBQ0MsSUFBTCxDQUFVLFVBQUNDLENBQUQsRUFBSUMsSUFBSixFQUFhO0FBQ3BCQSxZQUFJLENBQUNDLFNBQUwsQ0FBZXhDLE1BQWYsQ0FBc0IsUUFBdEI7QUFDRixPQUZEO0FBR0g7Ozs7RUFwQ29CekMsNkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ056QjtBQUVBO0FBQ0E7O0lBR01rRixNOzs7Ozs7Ozs7Ozs7OzJCQUVLO0FBQ0gsV0FBS2hGLFFBQUwsQ0FBY3lELEtBQWQsQ0FBb0I7QUFDaEJDLGdCQUFRLEVBQUUsS0FETTtBQUVoQkMsb0JBQVksRUFBRSxDQUZFO0FBR2hCQyxzQkFBYyxFQUFFLENBSEE7QUFJaEJDLGlCQUFTLEVBQUUsOEVBSks7QUFLaEJDLGlCQUFTLEVBQUUsOEVBTEs7QUFNaEJDLGtCQUFVLEVBQUUsQ0FDUjtBQUNJQyxvQkFBVSxFQUFFLElBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTk4sd0JBQVksRUFBRSxDQURSO0FBRU5DLDBCQUFjLEVBQUUsQ0FGVjtBQUdORixvQkFBUSxFQUFFLElBSEo7QUFJTlEsa0JBQU0sRUFBRTtBQUpGO0FBRmQsU0FEUSxFQVVSO0FBQ0lGLG9CQUFVLEVBQUUsR0FEaEI7QUFFSUMsa0JBQVEsRUFBRTtBQUNOTix3QkFBWSxFQUFFLENBRFI7QUFFTkMsMEJBQWMsRUFBRSxDQUZWO0FBR05GLG9CQUFRLEVBQUUsSUFISjtBQUlOUSxrQkFBTSxFQUFFO0FBSkY7QUFGZCxTQVZRLEVBbUJSO0FBQ0lGLG9CQUFVLEVBQUUsR0FEaEI7QUFFSUMsa0JBQVEsRUFBRTtBQUNOTix3QkFBWSxFQUFFLENBRFI7QUFFTkMsMEJBQWMsRUFBRSxDQUZWO0FBR05GLG9CQUFRLEVBQUUsSUFISjtBQUlOUSxrQkFBTSxFQUFFO0FBSkY7QUFGZCxTQW5CUSxFQTRCUjtBQUNJRixvQkFBVSxFQUFFLEdBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTk4sd0JBQVksRUFBRSxDQURSO0FBRU5DLDBCQUFjLEVBQUUsQ0FGVjtBQUdORixvQkFBUSxFQUFFLElBSEo7QUFJTlEsa0JBQU0sRUFBRTtBQUpGO0FBRmQsU0E1QlE7QUFOSSxPQUFwQjtBQTZDSDs7OztFQWhEZ0JwRSw2RDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTnJCO0FBRUE7QUFDQTs7SUFJTW1GLE87Ozs7Ozs7Ozs7Ozs7MkJBRUs7QUFDSDtBQUNBQyxpQkFBVyxDQUFDLGVBQUQsRUFDUDtBQUNBLHFCQUFhO0FBQ1Qsb0JBQVU7QUFBQyxxQkFBUyxFQUFWO0FBQWMsdUJBQVc7QUFBQyx3QkFBVSxJQUFYO0FBQWlCLDRCQUFjO0FBQS9CO0FBQXpCLFdBREQ7QUFFVCxtQkFBUztBQUFDLHFCQUFTO0FBQVYsV0FGQTtBQUdULG1CQUFTO0FBQ0wsb0JBQVEsUUFESDtBQUVMLHNCQUFVO0FBQUMsdUJBQVMsQ0FBVjtBQUFhLHVCQUFTO0FBQXRCLGFBRkw7QUFHTCx1QkFBVztBQUFDLDBCQUFZO0FBQWIsYUFITjtBQUlMLHFCQUFTO0FBQUMscUJBQU8sZ0JBQVI7QUFBMEIsdUJBQVMsR0FBbkM7QUFBd0Msd0JBQVU7QUFBbEQ7QUFKSixXQUhBO0FBU1QscUJBQVc7QUFDUCxxQkFBUyxrQkFERjtBQUVQLHNCQUFVLElBRkg7QUFHUCxvQkFBUTtBQUFDLHdCQUFVLEtBQVg7QUFBa0IsdUJBQVMsQ0FBM0I7QUFBOEIsNkJBQWUsR0FBN0M7QUFBa0Qsc0JBQVE7QUFBMUQ7QUFIRCxXQVRGO0FBY1Qsa0JBQVE7QUFDSixxQkFBUyxDQURMO0FBRUosc0JBQVUsSUFGTjtBQUdKLG9CQUFRO0FBQUMsd0JBQVUsS0FBWDtBQUFrQix1QkFBUyxFQUEzQjtBQUErQiwwQkFBWSxHQUEzQztBQUFnRCxzQkFBUTtBQUF4RDtBQUhKLFdBZEM7QUFtQlQseUJBQWU7QUFDWCxzQkFBVSxJQURDO0FBRVgsd0JBQVksaUJBRkQ7QUFHWCxxQkFBUyxTQUhFO0FBSVgsdUJBQVcsbUJBSkE7QUFLWCxxQkFBUztBQUxFLFdBbkJOO0FBMEJULGtCQUFRO0FBQ0osc0JBQVUsSUFETjtBQUVKLHFCQUFTLGlCQUZMO0FBR0oseUJBQWEsTUFIVDtBQUlKLHNCQUFVLElBSk47QUFLSix3QkFBWSxLQUxSO0FBTUosd0JBQVksS0FOUjtBQU9KLHNCQUFVLEtBUE47QUFRSix1QkFBVztBQUFDLHdCQUFVLEtBQVg7QUFBa0IseUJBQVcsR0FBN0I7QUFBa0MseUJBQVc7QUFBN0M7QUFSUDtBQTFCQyxTQURiO0FBc0NBLHlCQUFpQjtBQUNiLHVCQUFhLFFBREE7QUFFYixvQkFBVTtBQUNOLHVCQUFXO0FBQUMsd0JBQVUsSUFBWDtBQUFpQixzQkFBUTtBQUF6QixhQURMO0FBRU4sdUJBQVc7QUFBQyx3QkFBVSxJQUFYO0FBQWlCLHNCQUFRO0FBQXpCLGFBRkw7QUFHTixzQkFBVTtBQUhKLFdBRkc7QUFPYixtQkFBUztBQUNMLG9CQUFRO0FBQUMsMEJBQVksR0FBYjtBQUFrQiw2QkFBZTtBQUFDLDJCQUFXO0FBQVo7QUFBakMsYUFESDtBQUVMLHNCQUFVO0FBQUMsMEJBQVksR0FBYjtBQUFrQixzQkFBUSxFQUExQjtBQUE4QiwwQkFBWSxDQUExQztBQUE2Qyx5QkFBVyxDQUF4RDtBQUEyRCx1QkFBUztBQUFwRSxhQUZMO0FBR0wsdUJBQVc7QUFBQywwQkFBWSxHQUFiO0FBQWtCLDBCQUFZO0FBQTlCLGFBSE47QUFJTCxvQkFBUTtBQUFDLDhCQUFnQjtBQUFqQixhQUpIO0FBS0wsc0JBQVU7QUFBQyw4QkFBZ0I7QUFBakI7QUFMTDtBQVBJLFNBdENqQjtBQXFEQSx5QkFBaUI7QUFyRGpCLE9BRE8sQ0FBWDtBQXdESDs7OztFQTVEaUJwRiw2RDs7Ozs7Ozs7Ozs7Ozs7QUNQdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztJQUVNcUYsSzs7Ozs7Ozs7Ozs7OzsyQkFDSztBQUNILFdBQUtuRixRQUFMLENBQWNRLElBQWQsQ0FBbUIsU0FBbkIsRUFBOEJvRSxJQUE5QixDQUFtQyxVQUFDQyxDQUFELEVBQUlPLEVBQUosRUFBVztBQUMxQyxZQUFJMUMsbURBQUosQ0FBUTBDLEVBQVI7QUFDSCxPQUZEO0FBSUEsV0FBS3BGLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixlQUFuQixFQUFvQ29FLElBQXBDLENBQXlDLFVBQUNDLENBQUQsRUFBSU8sRUFBSixFQUFXO0FBQ2hELFlBQUk5RSwrREFBSixDQUFjOEUsRUFBZDtBQUNILE9BRkQ7QUFJQSxXQUFLcEYsUUFBTCxDQUFjUSxJQUFkLENBQW1CLGFBQW5CLEVBQWtDb0UsSUFBbEMsQ0FBdUMsVUFBQ0MsQ0FBRCxFQUFJTyxFQUFKLEVBQVc7QUFDOUMsWUFBSUgsMkRBQUosQ0FBWUcsRUFBWjtBQUNILE9BRkQ7QUFJQSxXQUFLcEYsUUFBTCxDQUFjUSxJQUFkLENBQW1CLHFCQUFuQixFQUEwQ29FLElBQTFDLENBQStDLFVBQUNDLENBQUQsRUFBSU8sRUFBSixFQUFXO0FBQ3RELFlBQUlKLHlEQUFKLENBQVdJLEVBQVg7QUFDSCxPQUZEO0FBSUEsV0FBS3BGLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixlQUFuQixFQUFvQ29FLElBQXBDLENBQXlDLFVBQUNDLENBQUQsRUFBSU8sRUFBSixFQUFXO0FBQ2hELFlBQUliLCtEQUFKLENBQWNhLEVBQWQ7QUFDSCxPQUZEO0FBSUEsV0FBS3BGLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixVQUFuQixFQUErQm9FLElBQS9CLENBQW9DLFVBQUNDLENBQUQsRUFBSU8sRUFBSixFQUFXO0FBQzNDLFlBQUliLCtEQUFKLENBQWNhLEVBQWQ7QUFDSCxPQUZEO0FBSUEsV0FBS3BGLFFBQUwsQ0FBY1EsSUFBZCxDQUFtQixjQUFuQixFQUFtQ29FLElBQW5DLENBQXdDLFVBQUNDLENBQUQsRUFBSU8sRUFBSixFQUFXO0FBQy9DLFlBQUk3Qiw2REFBSixDQUFhNkIsRUFBYjtBQUNILE9BRkQ7QUFJQSxXQUFLcEYsUUFBTCxDQUFjUSxJQUFkLENBQW1CLFlBQW5CLEVBQWlDb0UsSUFBakMsQ0FBc0MsVUFBQ0MsQ0FBRCxFQUFJTyxFQUFKLEVBQVc7QUFDN0MsWUFBSWpCLHlEQUFKLENBQVdpQixFQUFYO0FBQ0gsT0FGRDtBQUlBLFdBQUtwRixRQUFMLENBQWNRLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDb0UsSUFBdEMsQ0FBMkMsVUFBQ0MsQ0FBRCxFQUFJTyxFQUFKLEVBQVc7QUFDbEQsWUFBSVYsa0VBQUosQ0FBZVUsRUFBZjtBQUNILE9BRkQ7QUFJQSxVQUFNQyxZQUFZLEdBQUcsRUFBckI7O0FBQ0EsVUFBR0MsUUFBUSxDQUFDQyxlQUFULENBQXlCQyxXQUF6QixJQUF3QyxHQUEzQyxFQUFnRDtBQUM1QyxXQUFHQyxPQUFILENBQVdDLElBQVgsQ0FBZ0JKLFFBQVEsQ0FBQ0ssZ0JBQVQsQ0FBMEIsbUJBQTFCLENBQWhCLEVBQStELFVBQUNiLElBQUQsRUFBUztBQUNwRSxjQUFHQSxJQUFJLENBQUNjLFdBQUwsQ0FBaUJ6RCxNQUFqQixJQUEyQmtELFlBQTlCLEVBQTRDO0FBQ3pDLGdCQUFJUSxNQUFNLEdBQUdmLElBQUksQ0FBQ2MsV0FBTCxDQUFpQkUsS0FBakIsQ0FBdUIsQ0FBdkIsRUFBeUJULFlBQXpCLENBQWI7QUFDQVAsZ0JBQUksQ0FBQ2MsV0FBTCxHQUFtQkMsTUFBTSxHQUFHLEtBQTVCO0FBQ0Y7O0FBQUE7QUFDSixTQUxEO0FBTUg7O0FBQUE7O0FBR0QsVUFBSVAsUUFBUSxDQUFDUyxZQUFULElBQXlCLE9BQU9DLElBQVAsQ0FBWUMsU0FBUyxDQUFDQyxTQUF0QixDQUE3QixFQUErRDtBQUMzREMscURBQU0sQ0FBQyxnQkFBRCxDQUFOLENBQXlCdkIsSUFBekIsQ0FBOEIsWUFBVTtBQUNwQyxjQUFJd0IsQ0FBQyxHQUFHRCw2Q0FBTSxDQUFDLElBQUQsQ0FBZDtBQUFBLGNBQ0lFLENBQUMsR0FBRyxTQUFTRCxDQUFDLENBQUN0RSxJQUFGLENBQU8sS0FBUCxDQUFULEdBQXlCLEdBRGpDO0FBQUEsY0FFSXdFLENBQUMsR0FBR0YsQ0FBQyxDQUFDRyxNQUFGLEVBRlI7QUFBQSxjQUdJQyxDQUFDLEdBQUdMLDZDQUFNLENBQUMsYUFBRCxDQUhkO0FBS0FHLFdBQUMsQ0FBQ2pFLE1BQUYsQ0FBU21FLENBQVQ7QUFDQUEsV0FBQyxDQUFDaEYsR0FBRixDQUFNO0FBQ0Ysc0JBQTBCNEUsQ0FBQyxDQUFDRyxNQUFGLEdBQVcvRSxHQUFYLENBQWUsUUFBZixDQUR4QjtBQUVGLCtCQUEwQixPQUZ4QjtBQUdGLGlDQUEwQixXQUh4QjtBQUlGLG1DQUEwQixTQUp4QjtBQUtGLGdDQUEwQjZFO0FBTHhCLFdBQU47QUFPQUQsV0FBQyxDQUFDdkUsSUFBRjtBQUNILFNBZkQ7QUFnQkg7QUFFSjs7OztFQXBFZTFCLG1EOztBQXVFcEIsSUFBTXNHLElBQUksR0FBRyxJQUFJdEIsS0FBSixFQUFiLEM7Ozs7Ozs7Ozs7O0FDbEZBLHVDIiwiZmlsZSI6ImFwcC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImFwcFwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdHZhciBqc29ucEFycmF5ID0gd2luZG93W1wid2VicGFja0pzb25wXCJdID0gd2luZG93W1wid2VicGFja0pzb25wXCJdIHx8IFtdO1xuIFx0dmFyIG9sZEpzb25wRnVuY3Rpb24gPSBqc29ucEFycmF5LnB1c2guYmluZChqc29ucEFycmF5KTtcbiBcdGpzb25wQXJyYXkucHVzaCA9IHdlYnBhY2tKc29ucENhbGxiYWNrO1xuIFx0anNvbnBBcnJheSA9IGpzb25wQXJyYXkuc2xpY2UoKTtcbiBcdGZvcih2YXIgaSA9IDA7IGkgPCBqc29ucEFycmF5Lmxlbmd0aDsgaSsrKSB3ZWJwYWNrSnNvbnBDYWxsYmFjayhqc29ucEFycmF5W2ldKTtcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gb2xkSnNvbnBGdW5jdGlvbjtcblxuXG4gXHQvLyBhZGQgZW50cnkgbW9kdWxlIHRvIGRlZmVycmVkIGxpc3RcbiBcdGRlZmVycmVkTW9kdWxlcy5wdXNoKFtcIi4vc3JjL3NjcmlwdHMvaW5kZXguanNcIixcInZlbmRvcnNcIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCJpbXBvcnQgJCBmcm9tIFwianF1ZXJ5L2Rpc3QvanF1ZXJ5XCI7XHJcblxyXG5jbGFzcyBCYXNlQ29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQpIHtcclxuICAgICAgICB0aGlzLiRlbGVtZW50ID0gJChlbGVtZW50KTtcclxuICAgICAgICB0aGlzLmluaXQoKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0KCkge31cclxufVxyXG5cclxuZXhwb3J0IHtCYXNlQ29tcG9uZW50fTsiLCJpbXBvcnQgJCBmcm9tIFwianF1ZXJ5L2Rpc3QvanF1ZXJ5XCI7XHJcblxyXG5jbGFzcyBCYXNlUGFnZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50KSB7XHJcbiAgICAgICAgdGhpcy4kZWxlbWVudCA9ICQoJ2JvZHknKTtcclxuXHJcbiAgICAgICAgJCh3aW5kb3cpLnJlYWR5KCgkKT0+e1xyXG4gICAgICAgICAgICB0aGlzLmluaXQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIC8vYWJzdHJhY3RcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IHtCYXNlUGFnZX07IiwiaW1wb3J0IHtCYXNlQ29tcG9uZW50fSBmcm9tIFwiLi4vYmFzZS1jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCAkIGZyb20gXCJqcXVlcnkvZGlzdC9qcXVlcnlcIjtcclxuXHJcblxyXG5jbGFzcyBBY2NvcmRlb24gZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIHRoaXMubGlua3MgPSB0aGlzLiRlbGVtZW50LmZpbmQoXCIuanMtYWNjb3JkZW9uLWxpbmtcIik7XHJcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuJGVsZW1lbnQuZmluZChcIi5qcy1hY2NvcmRlb24taXRlbVwiKTtcclxuICAgICAgICB0aGlzLmRvdCA9IHRoaXMuJGVsZW1lbnQuZmluZChcIi5qcy1hY2NvcmRlb24tZG90XCIpO1xyXG4gICAgICAgIHRoaXMuaW1hZ2VCbG9jayA9IHRoaXMuJGVsZW1lbnQuZmluZChcIi5qcy1hY2NvcmRlb24taW1hZ2UtYmxvY2tcIik7XHJcbiAgICAgICAgdGhpcy5kb3RUb3AgPSAwO1xyXG4gICAgICAgIHRoaXMub2xkQ29udGVudEhlaWdodCA9IDA7XHJcbiAgICAgICAgdGhpcy5ub3RXb3JrID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5zZXRBY3RpdmUodGhpcy5saW5rcy5maXJzdCgpKTtcclxuXHJcbiAgICAgICAgdGhpcy5saW5rcy5vbignY2xpY2snLCAoZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCAkZWwgPSAkKGUudGFyZ2V0KTtcclxuICAgICAgICAgICAgaWYgKCEkZWwuaGFzQ2xhc3MoJ2pvYl9fYWNjb3JkZW9uLWl0ZW1fYWN0aXZlJykpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0QWN0aXZlKCRlbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRBY3RpdmUoJGVsKSB7XHJcbiAgICAgICAgY29uc3QgJGN1cnJlbnRJdGVtID0gJGVsLmNsb3Nlc3QodGhpcy5pdGVtcyk7XHJcblxyXG4gICAgICAgIGlmICghJGN1cnJlbnRJdGVtLmhhc0NsYXNzKCdqb2JfX2FjY29yZGVvbi1pdGVtX2FjdGl2ZScpICYmIHRoaXMubm90V29yaykge1xyXG4gICAgICAgICAgICB0aGlzLm5vdFdvcmsgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5kb3QuY3NzKCd0cmFuc2Zvcm0nLCAndHJhbnNsYXRlWSgnICsgdGhpcy5kb3RUb3AgKyAncHgpIHNjYWxlKDAuNSknKTtcclxuICAgICAgICAgICAgY29uc3QgJG9sZEltYWdlID0gdGhpcy4kZWxlbWVudC5maW5kKFwiLmpzLWFjY29yZGVvbi1pbWFnZVwiKTtcclxuICAgICAgICAgICAgJG9sZEltYWdlLmFkZENsYXNzKCdqb2JfX25vdGVib29rX2VkaXQnKTtcclxuICAgICAgICAgICAgY29uc3QgJG5ld0ltYWdlID0gJG9sZEltYWdlLmNsb25lKCkuaGlkZSgpLmF0dHIoJ3NyYycsICRjdXJyZW50SXRlbS5kYXRhKCdpbWcnKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZG90VG9wID0gJGN1cnJlbnRJdGVtLnBvc2l0aW9uKCkudG9wO1xyXG5cclxuICAgICAgICAgICAgaWYgKCRjdXJyZW50SXRlbS5uZXh0QWxsKCcuam9iX19hY2NvcmRlb24taXRlbV9hY3RpdmUnKS5sZW5ndGggIT0gMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kb3RUb3AgPSB0aGlzLmRvdFRvcCAtIHRoaXMub2xkQ29udGVudEhlaWdodDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5pdGVtcy5yZW1vdmVDbGFzcygnam9iX19hY2NvcmRlb24taXRlbV9hY3RpdmUnKTtcclxuICAgICAgICAgICAgJGVsLmNsb3Nlc3QodGhpcy5pdGVtcykuYWRkQ2xhc3MoJ2pvYl9fYWNjb3JkZW9uLWl0ZW1fYWN0aXZlJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZG90LmNzcygndHJhbnNmb3JtJywgJ3RyYW5zbGF0ZVkoJyArIHRoaXMuZG90VG9wICsgJ3B4KSBzY2FsZSgwLjUpJyk7XHJcbiAgICAgICAgICAgIHRoaXMuaW1hZ2VCbG9jay5hcHBlbmQoJG5ld0ltYWdlKTtcclxuXHJcbiAgICAgICAgICAgICRuZXdJbWFnZS5mYWRlSW4oJ25vcm1hbCcsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICRuZXdJbWFnZS5yZW1vdmVDbGFzcygnam9iX19ub3RlYm9va19lZGl0Jyk7XHJcbiAgICAgICAgICAgICAgICAkb2xkSW1hZ2UucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvdC5jc3MoJ3RyYW5zZm9ybScsICd0cmFuc2xhdGVZKCcgKyB0aGlzLmRvdFRvcCArICdweCkgc2NhbGUoMSknKTtcclxuICAgICAgICAgICAgfSwgNTUwKTtcclxuXHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMub2xkQ29udGVudEhlaWdodCA9ICRjdXJyZW50SXRlbS5maW5kKCcuanMtYWNjb3JkZW9uLWhpZGRlbicpLm91dGVySGVpZ2h0KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdFdvcmsgPSB0cnVlO1xyXG4gICAgICAgICAgICB9LCA4MDApO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge0FjY29yZGVvbn07IiwiaW1wb3J0IHtCYXNlQ29tcG9uZW50fSBmcm9tIFwiLi4vYmFzZS1jb21wb25lbnRcIjtcclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeS9kaXN0L2pxdWVyeVwiO1xyXG5cclxuXHJcbmNsYXNzIEJ0biBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgJGJ0biA9IHRoaXMuJGVsZW1lbnQuZmluZChcIi5qcy1idG4taW5uZXJcIik7XHJcblxyXG4gICAgICAgICRidG4ubW91c2VlbnRlcihmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJlbnRPZmZzZXQgPSAkKHRoaXMpLm9mZnNldCgpO1xyXG4gICAgICAgICAgICBjb25zdCByZWxYID0gZS5wYWdlWCAtIHBhcmVudE9mZnNldC5sZWZ0O1xyXG4gICAgICAgICAgICBjb25zdCByZWxZID0gZS5wYWdlWSAtIHBhcmVudE9mZnNldC50b3A7XHJcbiAgICAgICAgICAgIGNvbnN0ICRjaXJjbGUgPSAkKHRoaXMpLnByZXYoXCIuYnRuX19jaXJjbGVcIik7XHJcbiAgICAgICAgICAgICRjaXJjbGUuY3NzKHtcImxlZnRcIjogcmVsWCwgXCJ0b3BcIjogcmVsWX0pO1xyXG4gICAgICAgICAgICAkY2lyY2xlLnJlbW92ZUNsYXNzKFwiZGVzcGxvZGUtY2lyY2xlXCIpO1xyXG4gICAgICAgICAgICAkY2lyY2xlLmFkZENsYXNzKFwiZXhwbG9kZS1jaXJjbGVcIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRidG4ubW91c2VsZWF2ZShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJlbnRPZmZzZXQgPSAkKHRoaXMpLm9mZnNldCgpO1xyXG4gICAgICAgICAgICBjb25zdCByZWxYID0gZS5wYWdlWCAtIHBhcmVudE9mZnNldC5sZWZ0O1xyXG4gICAgICAgICAgICBjb25zdCByZWxZID0gZS5wYWdlWSAtIHBhcmVudE9mZnNldC50b3A7XHJcbiAgICAgICAgICAgIGNvbnN0ICRjaXJjbGUgPSAkKHRoaXMpLnByZXYoXCIuYnRuX19jaXJjbGVcIik7XHJcbiAgICAgICAgICAgICRjaXJjbGUuY3NzKHtcImxlZnRcIjogcmVsWCwgXCJ0b3BcIjogcmVsWX0pO1xyXG4gICAgICAgICAgICAkY2lyY2xlLnJlbW92ZUNsYXNzKFwiZXhwbG9kZS1jaXJjbGVcIik7XHJcbiAgICAgICAgICAgICRjaXJjbGUuYWRkQ2xhc3MoXCJkZXNwbG9kZS1jaXJjbGVcIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge0J0bn07IiwiaW1wb3J0IHtCYXNlQ29tcG9uZW50fSBmcm9tIFwiLi4vYmFzZS1jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCAkIGZyb20gXCJqcXVlcnkvZGlzdC9qcXVlcnlcIjtcclxuaW1wb3J0IFwic2xpY2stY2Fyb3VzZWxcIjtcclxuXHJcblxyXG5jbGFzcyBDb21tZW50cyBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICBsZXQgcmVzID0gdGhpcy4kZWxlbWVudC5maW5kKCcuanMtc2xpZGVyJykuc2xpY2soe1xyXG4gICAgICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgIHByZXZBcnJvdzogXCI8YSBocmVmPVxcXCJqYXZhc2NyaXB0OnZvaWQoMCk7XFxcIiBjbGFzcz0ncGFydG5lcnNfX3ByZXYgYXJyb3cgYXJyb3dfcHJldic+PC9hPlwiLFxyXG4gICAgICAgICAgICBuZXh0QXJyb3c6IFwiPGEgaHJlZj1cXFwiamF2YXNjcmlwdDp2b2lkKDApO1xcXCIgY2xhc3M9J3BhcnRuZXJzX19uZXh0IGFycm93IGFycm93X25leHQnPjwvYT5cIixcclxuICAgICAgICAgICAgcmVzcG9uc2l2ZTogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDEyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFycm93czogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDcwMCxcclxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge0NvbW1lbnRzfTsiLCJpbXBvcnQge0Jhc2VDb21wb25lbnR9IGZyb20gXCIuLi9iYXNlLWNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeS9kaXN0L2pxdWVyeVwiO1xyXG5pbXBvcnQgXCJzbGljay1jYXJvdXNlbFwiO1xyXG5cclxuXHJcbmNsYXNzIEhlYWRlciBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgaW5uZXIgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1oZWFkZXItaW5uZXInKTtcclxuICAgICAgICBjb25zdCB0b2dnbGVCdXR0b24gPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1oYW1idXJnZXInKTtcclxuXHJcbiAgICAgICAgdG9nZ2xlQnV0dG9uLm9uKCdjbGljaycsICgpID0+IHtcclxuICAgICAgICAgICAgaW5uZXIudG9nZ2xlQ2xhc3MoJ2hlYWRlcl9faW5uZXJfb3BlbmVkJyk7XHJcbiAgICAgICAgICAgIHRvZ2dsZUJ1dHRvbi50b2dnbGVDbGFzcyhcImlzLWFjdGl2ZVwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCB7SGVhZGVyfTsiLCJpbXBvcnQge0Jhc2VDb21wb25lbnR9IGZyb20gXCIuLi9iYXNlLWNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeS9kaXN0L2pxdWVyeVwiO1xyXG5pbXBvcnQgXCJzbGljay1jYXJvdXNlbFwiO1xyXG5cclxuXHJcbmNsYXNzIEhpc3RvcmllcyBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuanMtc2xpZGVyJykuc2xpY2soe1xyXG4gICAgICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlLFxyXG4gICAgICAgICAgICBmYWRlOiB0cnVlLFxyXG4gICAgICAgICAgICBwcmV2QXJyb3c6IFwiPGEgaHJlZj1cXFwiamF2YXNjcmlwdDp2b2lkKDApO1xcXCIgY2xhc3M9J2hpc3Rvcmllc19fcHJldiBoaXN0b3JpZXNfX2Fycm93IGFycm93IGFycm93X3ByZXYnPjwvYT5cIixcclxuICAgICAgICAgICAgbmV4dEFycm93OiBcIjxhIGhyZWY9XFxcImphdmFzY3JpcHQ6dm9pZCgwKTtcXFwiIGNsYXNzPSdoaXN0b3JpZXNfX25leHQgaGlzdG9yaWVzX19hcnJvdyBhcnJvdyBhcnJvd19uZXh0Jz48L2E+XCIsXHJcbiAgICAgICAgICAgIHJlc3BvbnNpdmU6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiAxMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJvd3M6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQge0hpc3Rvcmllc307IiwiaW1wb3J0IHsgQmFzZUNvbXBvbmVudCB9IGZyb20gXCIuLi9iYXNlLWNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeS9kaXN0L2pxdWVyeVwiO1xyXG5pbXBvcnQgXCJzbGljay1jYXJvdXNlbFwiO1xyXG5cclxuXHJcbmNsYXNzIFNsaWRlckJsb2cgZXh0ZW5kcyBCYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIC8vIHRoaXMuJGVsZW1lbnQuc2xpY2soe1xyXG4gICAgICAgIC8vICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgLy8gICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAvLyAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgLy8gICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgLy8gICAgIGFkYXB0aXZlSGVpZ2h0OiB0cnVlXHJcbiAgICAgICAgLy8gfSk7XHJcblxyXG5cclxuICAgICAgICAvLyBsZXQgZG90cyA9ICQoYC5ibG9nX25hdmlnYXRlLWl0ZW1gKTtcclxuICAgICAgICAvLyBkb3RzLmVhY2goKGksIGl0ZW0pID0+IHtcclxuICAgICAgICAvLyAgICAgJChpdGVtKS5jbGljaygoKSA9PiB7XHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLmRlbGV0ZUFsbEFjdGl2ZSgpO1xyXG4gICAgICAgIC8vICAgICAgICAgJChpdGVtKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLiRlbGVtZW50LnNsaWNrKCdzbGlja0dvVG8nLCBpKVxyXG4gICAgICAgIC8vICAgICB9KVxyXG4gICAgICAgIC8vIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vIGxldCBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAgIC8vIHRoaXMuJGVsZW1lbnQub24oJ2FmdGVyQ2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIHNsaWNrLCBjdXJyZW50U2xpZGUsIG5leHRTbGlkZSl7XHJcbiAgICAgICAgLy8gICAgIF90aGlzLmRlbGV0ZUFsbEFjdGl2ZSgpO1xyXG4gICAgICAgIC8vICAgICBkb3RzW2N1cnJlbnRTbGlkZV0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcclxuICAgICAgICAvLyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBcclxuXHJcbiAgICBkZWxldGVBbGxBY3RpdmUoKSB7XHJcbiAgICAgICAgbGV0IGRvdHMgPSAkKGAuYmxvZ19uYXZpZ2F0ZS1pdGVtYCk7XHJcbiAgICAgICAgZG90cy5lYWNoKChpLCBpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCB7IFNsaWRlckJsb2cgfTsiLCJpbXBvcnQge0Jhc2VDb21wb25lbnR9IGZyb20gXCIuLi9iYXNlLWNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeS9kaXN0L2pxdWVyeVwiO1xyXG5pbXBvcnQgXCJzbGljay1jYXJvdXNlbFwiO1xyXG5cclxuXHJcbmNsYXNzIFNsaWRlciBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgdGhpcy4kZWxlbWVudC5zbGljayh7XHJcbiAgICAgICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiA3LFxyXG4gICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMyxcclxuICAgICAgICAgICAgcHJldkFycm93OiBcIjxhIGhyZWY9XFxcImphdmFzY3JpcHQ6dm9pZCgwKTtcXFwiIGNsYXNzPSdwYXJ0bmVyc19fcHJldiBhcnJvdyBhcnJvd19wcmV2Jz48L2E+XCIsXHJcbiAgICAgICAgICAgIG5leHRBcnJvdzogXCI8YSBocmVmPVxcXCJqYXZhc2NyaXB0OnZvaWQoMCk7XFxcIiBjbGFzcz0ncGFydG5lcnNfX25leHQgYXJyb3cgYXJyb3dfbmV4dCc+PC9hPlwiLFxyXG4gICAgICAgICAgICByZXNwb25zaXZlOiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogMTIwMCxcclxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogODAwLFxyXG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJvd3M6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiA2MDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFycm93czogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDQ1MCxcclxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IHtTbGlkZXJ9OyIsImltcG9ydCB7QmFzZUNvbXBvbmVudH0gZnJvbSBcIi4uL2Jhc2UtY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQgJCBmcm9tIFwianF1ZXJ5L2Rpc3QvanF1ZXJ5XCI7XHJcbmltcG9ydCBcInBhcnRpY2xlcy5qcy9wYXJ0aWNsZXNcIjtcclxuXHJcblxyXG5cclxuY2xhc3MgV2VsY29tZSBleHRlbmRzIEJhc2VDb21wb25lbnQge1xyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgLy8gY29uc3QgJGJnID0gdGhpcy4kZWxlbWVudC5maW5kKFwiLmpzLWJnXCIpO1xyXG4gICAgICAgIHBhcnRpY2xlc0pTKFwianMtd2VsY29tZS1iZ1wiLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgIFwicGFydGljbGVzXCI6IHtcclxuICAgICAgICAgICAgICAgIFwibnVtYmVyXCI6IHtcInZhbHVlXCI6IDMwLCBcImRlbnNpdHlcIjoge1wiZW5hYmxlXCI6IHRydWUsIFwidmFsdWVfYXJlYVwiOiA3MDB9fSxcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjoge1widmFsdWVcIjogXCIjMkNCOTAxXCJ9LFxyXG4gICAgICAgICAgICAgICAgXCJzaGFwZVwiOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiY2lyY2xlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHJva2VcIjoge1wid2lkdGhcIjogMSwgXCJjb2xvclwiOiBcIiM4MkVEN0JcIn0sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJwb2x5Z29uXCI6IHtcIm5iX3NpZGVzXCI6IDN9LFxyXG4gICAgICAgICAgICAgICAgICAgIFwiaW1hZ2VcIjoge1wic3JjXCI6IFwiaW1nL2dpdGh1Yi5zdmdcIiwgXCJ3aWR0aFwiOiAxMDAsIFwiaGVpZ2h0XCI6IDEwMH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcIm9wYWNpdHlcIjoge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjogMC4zOTI4MzU5NTQ5MTIwNTMxLFxyXG4gICAgICAgICAgICAgICAgICAgIFwicmFuZG9tXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJhbmltXCI6IHtcImVuYWJsZVwiOiBmYWxzZSwgXCJzcGVlZFwiOiAxLCBcIm9wYWNpdHlfbWluXCI6IDAuMSwgXCJzeW5jXCI6IGZhbHNlfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIFwic2l6ZVwiOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJ2YWx1ZVwiOiA1LFxyXG4gICAgICAgICAgICAgICAgICAgIFwicmFuZG9tXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJhbmltXCI6IHtcImVuYWJsZVwiOiBmYWxzZSwgXCJzcGVlZFwiOiA0MCwgXCJzaXplX21pblwiOiAwLjEsIFwic3luY1wiOiBmYWxzZX1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcImxpbmVfbGlua2VkXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICBcImVuYWJsZVwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiZGlzdGFuY2VcIjogMjg4LjYxNDE3MDk1NTc5NDEsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiMwM0MwMjVcIixcclxuICAgICAgICAgICAgICAgICAgICBcIm9wYWNpdHlcIjogMC4xOTI0MDk0NDczMDM4NjI3MixcclxuICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IDFcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcIm1vdmVcIjoge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZW5hYmxlXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzcGVlZFwiOiAxLjYwMzQxMjA2MDg2NTUyMyxcclxuICAgICAgICAgICAgICAgICAgICBcImRpcmVjdGlvblwiOiBcIm5vbmVcIixcclxuICAgICAgICAgICAgICAgICAgICBcInJhbmRvbVwiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIFwic3RyYWlnaHRcIjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvdXRfbW9kZVwiOiBcIm91dFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiYm91bmNlXCI6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiYXR0cmFjdFwiOiB7XCJlbmFibGVcIjogZmFsc2UsIFwicm90YXRlWFwiOiA2MDAsIFwicm90YXRlWVwiOiAxMjAwfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBcImludGVyYWN0aXZpdHlcIjoge1xyXG4gICAgICAgICAgICAgICAgXCJkZXRlY3Rfb25cIjogXCJjYW52YXNcIixcclxuICAgICAgICAgICAgICAgIFwiZXZlbnRzXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICBcIm9uaG92ZXJcIjoge1wiZW5hYmxlXCI6IHRydWUsIFwibW9kZVwiOiBcImdyYWJcIn0sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJvbmNsaWNrXCI6IHtcImVuYWJsZVwiOiB0cnVlLCBcIm1vZGVcIjogXCJwdXNoXCJ9LFxyXG4gICAgICAgICAgICAgICAgICAgIFwicmVzaXplXCI6IHRydWVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcIm1vZGVzXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICBcImdyYWJcIjoge1wiZGlzdGFuY2VcIjogNDAwLCBcImxpbmVfbGlua2VkXCI6IHtcIm9wYWNpdHlcIjogMC4yfX0sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJidWJibGVcIjoge1wiZGlzdGFuY2VcIjogNDAwLCBcInNpemVcIjogNDAsIFwiZHVyYXRpb25cIjogMiwgXCJvcGFjaXR5XCI6IDgsIFwic3BlZWRcIjogM30sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJyZXB1bHNlXCI6IHtcImRpc3RhbmNlXCI6IDIwMCwgXCJkdXJhdGlvblwiOiAwLjR9LFxyXG4gICAgICAgICAgICAgICAgICAgIFwicHVzaFwiOiB7XCJwYXJ0aWNsZXNfbmJcIjogNH0sXHJcbiAgICAgICAgICAgICAgICAgICAgXCJyZW1vdmVcIjoge1wicGFydGljbGVzX25iXCI6IDJ9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFwicmV0aW5hX2RldGVjdFwiOiB0cnVlXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCB7V2VsY29tZX07IiwiaW1wb3J0ICcuLi9zdHlsZXMvaW5kZXgubGVzcyc7XHJcbmltcG9ydCAnLi9wYWdlL2luZGV4LmpzJzsiLCJpbXBvcnQgeyBCYXNlUGFnZSB9IGZyb20gXCIuLi9iYXNlLXBhZ2VcIjtcclxuaW1wb3J0IHsgQnRuIH0gZnJvbSBcIi4uL2NvbXBvbmVudHMvYnRuXCI7XHJcbmltcG9ydCB7IFdlbGNvbWUgfSBmcm9tIFwiLi4vY29tcG9uZW50cy93ZWxjb21lXCI7XHJcbmltcG9ydCB7IEFjY29yZGVvbiB9IGZyb20gXCIuLi9jb21wb25lbnRzL2FjY29yZGVvblwiO1xyXG5pbXBvcnQgeyBTbGlkZXIgfSBmcm9tIFwiLi4vY29tcG9uZW50cy9zbGlkZXJcIjtcclxuaW1wb3J0IHsgSGlzdG9yaWVzIH0gZnJvbSBcIi4uL2NvbXBvbmVudHMvaGlzdG9yaWVzXCI7XHJcbmltcG9ydCB7IENvbW1lbnRzIH0gZnJvbSBcIi4uL2NvbXBvbmVudHMvY29tbWVudHNcIjtcclxuaW1wb3J0IHsgSGVhZGVyIH0gZnJvbSBcIi4uL2NvbXBvbmVudHMvaGVhZGVyXCI7XHJcbmltcG9ydCB7IFNsaWRlckJsb2cgfSBmcm9tIFwiLi4vY29tcG9uZW50cy9zbGlkZXItYmxvZ1wiO1xyXG5pbXBvcnQgalF1ZXJ5IGZyb20gJ2pxdWVyeSdcclxuXHJcbmNsYXNzIEluZGV4IGV4dGVuZHMgQmFzZVBhZ2Uge1xyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1idG4nKS5lYWNoKChpLCBlbCkgPT4ge1xyXG4gICAgICAgICAgICBuZXcgQnRuKGVsKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuanMtYWNjb3JkZW9uJykuZWFjaCgoaSwgZWwpID0+IHtcclxuICAgICAgICAgICAgbmV3IEFjY29yZGVvbihlbCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLmpzLXdlbGNvbWUnKS5lYWNoKChpLCBlbCkgPT4ge1xyXG4gICAgICAgICAgICBuZXcgV2VsY29tZShlbCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLmpzLXBhcnRuZXJzLXNsaWRlcicpLmVhY2goKGksIGVsKSA9PiB7XHJcbiAgICAgICAgICAgIG5ldyBTbGlkZXIoZWwpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1oaXN0b3JpZXMnKS5lYWNoKChpLCBlbCkgPT4ge1xyXG4gICAgICAgICAgICBuZXcgSGlzdG9yaWVzKGVsKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuanMtbmV3cycpLmVhY2goKGksIGVsKSA9PiB7XHJcbiAgICAgICAgICAgIG5ldyBIaXN0b3JpZXMoZWwpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1jb21tZW50cycpLmVhY2goKGksIGVsKSA9PiB7XHJcbiAgICAgICAgICAgIG5ldyBDb21tZW50cyhlbCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLmpzLWhlYWRlcicpLmVhY2goKGksIGVsKSA9PiB7XHJcbiAgICAgICAgICAgIG5ldyBIZWFkZXIoZWwpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1zbGlkZXItYmxvZycpLmVhY2goKGksIGVsKSA9PiB7XHJcbiAgICAgICAgICAgIG5ldyBTbGlkZXJCbG9nKGVsKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgbWF4VGl0bGVXb3JkID0gMzU7XHJcbiAgICAgICAgaWYoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRoIDw9IDQwMCkge1xyXG4gICAgICAgICAgICBbXS5mb3JFYWNoLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5jYXJkLWJsb2dfX3RpdGxlXCIpLChpdGVtKT0+IHtcclxuICAgICAgICAgICAgICAgIGlmKGl0ZW0udGV4dENvbnRlbnQubGVuZ3RoID49IG1heFRpdGxlV29yZCkge1xyXG4gICAgICAgICAgICAgICAgICAgbGV0IG5ld1N0ciA9IGl0ZW0udGV4dENvbnRlbnQuc2xpY2UoMCxtYXhUaXRsZVdvcmQpO1xyXG4gICAgICAgICAgICAgICAgICAgaXRlbS50ZXh0Q29udGVudCA9IG5ld1N0ciArIFwiLi4uXCJcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcblxyXG5cclxuICAgICAgICBpZiAoZG9jdW1lbnQuZG9jdW1lbnRNb2RlIHx8IC9FZGdlLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpKSB7XHJcbiAgICAgICAgICAgIGpRdWVyeSgnLmllLTExLWltZyBpbWcnKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICB2YXIgdCA9IGpRdWVyeSh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICBzID0gJ3VybCgnICsgdC5hdHRyKCdzcmMnKSArICcpJyxcclxuICAgICAgICAgICAgICAgICAgICBwID0gdC5wYXJlbnQoKSxcclxuICAgICAgICAgICAgICAgICAgICBkID0galF1ZXJ5KCc8ZGl2PjwvZGl2PicpO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICAgICAgcC5hcHBlbmQoZCk7XHJcbiAgICAgICAgICAgICAgICBkLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ2hlaWdodCcgICAgICAgICAgICAgICAgOiB0LnBhcmVudCgpLmNzcygnaGVpZ2h0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgJ2JhY2tncm91bmQtc2l6ZScgICAgICAgOiAnY292ZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kLXJlcGVhdCcgICAgIDogJ25vLXJlcGVhdCcsXHJcbiAgICAgICAgICAgICAgICAgICAgJ2JhY2tncm91bmQtcG9zaXRpb24nICAgOiAnNTAlIDIwJScsXHJcbiAgICAgICAgICAgICAgICAgICAgJ2JhY2tncm91bmQtaW1hZ2UnICAgICAgOiBzXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHQuaGlkZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBwYWdlID0gbmV3IEluZGV4KCk7IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==