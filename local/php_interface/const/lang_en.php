<?php
const IB_HISTORIES = 11;
const IB_COMMENTS = 9;
const IB_JOB = 15;
const IB_JOB_MAIN_TOP_SECTION = 14;
const IB_JOB_MAIN_BOTTOM_SECTION = 18;
const IB_JOB_PARTNERS_TOP_SECTION = 15;
const IB_JOB_SMART_SHOPPERS_TOP_SECTION = 15;

const IB_BLOG = 17;
const IB_BLOG_SECTION_ARTICLES = 21;
const IB_BLOG_SECTION_CASES = 20;
const IB_BLOG_SECTION_NEWS = 19;

const MENU_TOP = 'top_en';

