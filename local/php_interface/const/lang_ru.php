<?php
const IB_HISTORIES = 7;
const IB_COMMENTS = 8;
const IB_JOB = 14;
const IB_JOB_MAIN_TOP_SECTION = 9;
const IB_JOB_MAIN_BOTTOM_SECTION = 11;
const IB_JOB_PARTNERS_TOP_SECTION = 10;
const IB_JOB_SMART_SHOPPERS_TOP_SECTION = 12;

const IB_BLOG = 17;
const IB_BLOG_SECTION_ARTICLES = 21;
const IB_BLOG_SECTION_CASES = 20;
const IB_BLOG_SECTION_NEWS = 19;

const MENU_TOP = 'top';
