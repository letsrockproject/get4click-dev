<?php
/**
 * Пользовательские константы
 */

const ASSETS_PATH = '/local/assets/';
const PRESENTATION_PATH = '/get4click_presentation_online_stores.pdf';


/**
 * Highload блоки
 */

//const HL_CITY = 3;

/**
 * Инфоблоков
 */

const IB_REQUEST_MAIN = 5;
const IB_REQUEST_SMART_BUYERS = 16;
const IB_TRAFFIC_PARTNER = 6;
const IB_MARKET_PARTNER = 12;

/**
 * Таблица соответствия инфоблоков
 */

switch (LANGUAGE_ID) {
    case 'ru':
        require 'lang_ru.php';
        break;
    case 'en':
        require 'lang_en.php';
        break;
}
