<?
include $_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/const/userConstants.php';
require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';

if (isset($_GET['lang'])) {
    define(LANGUAGE_ID, $_GET['lang']);
}
