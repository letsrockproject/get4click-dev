<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID; ?>" lang="<?= LANGUAGE_ID; ?>">
<head>
    <? $APPLICATION->ShowProperty('AfterHeadOpen'); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? $APPLICATION->ShowTitle(); ?></title>    <?

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addCss(ASSETS_PATH . "css/style.css");
 	Asset::getInstance()->addCss(ASSETS_PATH . "css/custom.css");
	Asset::getInstance()->addCss(ASSETS_PATH . "css/cookieAllowAccess.css");

    if(strstr($APPLICATION->GetCurDir(),'/blog/')){
	    Asset::getInstance()->addCss(ASSETS_PATH . "css/app.css");
    } else {
        Asset::getInstance()->addCss(ASSETS_PATH . "css/news.css");
    }
    Asset::getInstance()->addJs(ASSETS_PATH . "js/script.bundle.js");
    Asset::getInstance()->addJs(ASSETS_PATH . "js/vendors.bundle.js");
    Asset::getInstance()->addJs(ASSETS_PATH . "js/app.bundle.js");
	Asset::getInstance()->addJs(ASSETS_PATH . "js/cookieAllowAccess.js");
	Asset::getInstance()->addJs("https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js");
	Asset::getInstance()->addJs("https://yastatic.net/share2/share.js");
	Asset::getInstance()->addJs("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js");
    $APPLICATION->ShowHead();
    IncludeTemplateLangFile(__FILE__);
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script>
        var modernBrowser = (
            'fetch' in window &&
            'assign' in Object
        );

        if (!modernBrowser) {
            var scriptElement = document.createElement('script');

            scriptElement.async = false;
            scriptElement.src = '<?=ASSETS_PATH?>js/polyfills.bundle.js';
            document.head.appendChild(scriptElement);
        }
    </script>

<!-- Google Analytics script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-35414905-1', 'get4click.ru');
ga('send', 'pageview');

</script>
<!-- END Google Analytics script -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(53455198, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53455198" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</head>
<body class="page">
<? $APPLICATION->ShowPanel() ?>
<div class="page__wrapper">
    <header class="header js-header">
        <div class="container js-header-inner header__inner">
            <a href="javascript:void(0);" class="header__hamburger js-hamburger">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </a>
            <a href="/" class="header__logo"></a>
            <nav class="header__navigation navigation navigation_type_desktop">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "top", [
                    "COMPONENT_TEMPLATE" => ".default",
                    "ROOT_MENU_TYPE" => MENU_TOP,    // Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "ADDITIONAL_CLASS" => 'header'
                ],
                    false
                ); ?>
            </nav>

            <div class="btn btn_light js-btn">
                <span class="btn__circle js-btn-effect desplode-circle header__btn-enter"></span>
                <a href="https://get4click.ru/system/login" class="btn__inner js-btn-inner js-submit" onclick="ym(53455198, 'reachGoal', 'partners_enter'); return true;">
                    <span class="btn__text"><?= GetMessage('HEADER_LOGIN_FOR_PARTNER') ?> </span>
                </a>
            </div>

            <? $APPLICATION->IncludeComponent("letsrock:lang", "", [],
                false
            ); ?>
        </div>
    </header>