<footer class="footer">
    <div class="footer__top">
        <div class="footer__top-inner container">
            <a href="/" class="footer__logo"></a>
            <nav class="navigation navigation_type_desktop">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "ROOT_MENU_TYPE" => MENU_TOP,	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "ADDITIONAL_CLASS" => 'footer'
                ),
                    false
                );?>
            </nav>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="footer__bottom-inner container">
            <div class="footer__block footer__block_left">
                <span class="footer__dark-text"><?=GetMessage('C_FOOTER_SEND_ME')?></span>
			<br>
				<span class="footer__dark-text">Email:</span>
                <a href="mailto:hello@get4click.ru"
                   class="footer__light-text footer__link">info@get4click.ru</a>
<br>
<span class="footer__dark-text">Телефон:</span>
                <a href="tel:+74993912252"
                   class="footer__light-text footer__link">+7(499)391-22-52</a>
            </div>
            <div class="footer__block footer__block_center">
                <a href="/policy/" class="footer__light-text footer__link"><?=GetMessage('C_FOOTER_PD')?></a>
                <span class="footer__dark-text mobile-hide"> | </span>
                <a href="/oferta/" class="footer__light-text footer__link"><?=GetMessage('C_FOOTER_OFERTA')?></a>
				<div><a href="https://www.facebook.com/get4click/" target="_blank"><img class="facebook-ico" src="/upload/medialibrary/702/702606929fa20955550265edf6b682b1.png"></a></div>
            </div>
            <div class="footer__block footer__block_right">
                <a href="https://letsrock.pro" class="letsrock-logo"></a>
            </div>
        </div>
    </div>
</footer>
</div>
<?
include_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/modals.php'
?>
</body>
</html>