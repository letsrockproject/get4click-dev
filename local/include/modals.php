<?php if (preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']) == '/smart-buyers/') { ?>
    <div class="modal modal_send-request js-modal-send-request">
        <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
            <div class="modal__close-line"></div>
            <div class="modal__close-line"></div>
        </a>
        <div class="modal__inner">
            <p class="modal__title">Спасибо за подписку</p>
            <p class="modal__subtitle">
                <div class="btn welcome__form-btn js-btn">
                    <span class="btn__circle js-btn-effect desplode-circle" style="left: 154.266px; top: 24px;"></span>
                    <a href="https://get4click.ru/ext/GQVJM5RU" class="btn__inner js-btn-inner js-submit">
                        <span class="btn__text">Получить скидку</span>
                    </a>
                </div>
            </p>
        </div>
    </div>
<?php } else { ?>
    <div class="modal modal_send-request js-modal-send-request">
        <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
            <div class="modal__close-line"></div>
            <div class="modal__close-line"></div>
        </a>
        <div class="modal__inner">
            <p class="modal__title"><?= GetMessage('MODALS_THANKS') ?></p>
            <p class="modal__subtitle"><?= GetMessage('MODALS_FORM_SEND') ?></p>
        </div>
    </div>
<?php } ?>
<? if (LANGUAGE_ID == 'ru'): ?>
    <div class="modal modal_video js-modal-video"
         data-izimodal-width="924px"
         data-izimodal-iframeHeight="520px"
         data-izimodal-iframe="true"
         data-izimodal-iframeURL="https://www.youtube.com/embed/UhDQnOw2HMw"
         id="youtube">
        <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
            <div class="modal__close-line"></div>
            <div class="modal__close-line"></div>
        </a>
    </div>
<? else: ?>
    <div class="modal modal_video js-modal-video"
         data-izimodal-width="924px"
         data-izimodal-iframeHeight="520px"
         data-izimodal-iframe="true"
         data-izimodal-iframeURL="https://www.youtube.com/embed/C6cggJHrAps"
         id="youtube">
        <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
            <div class="modal__close-line"></div>
            <div class="modal__close-line"></div>
        </a>
    </div>
<? endif; ?>


<div class="modal modal_form-request js-modal-form-request" data-izimodal-width="1193">
    <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
        <div class="modal__close-line"></div>
        <div class="modal__close-line"></div>
    </a>
    <div class="request__inner">
        <div class="request__form-wrap">
            <form class="request__form form js-request-form js-tasks-form" action="/ajax/forms/main/">
                <p class="form__title"><?= GetMessage('C_REQUEST_TITLE') ?></p>
                <div class="form__input-wrap">
                    <input type="text" name="imr_e" class="form__input email required" id="remail" required>
                    <label class="form__label" for="remail"><?= GetMessage('C_REQUEST_EMAIL') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="imr_n" class="form__input name required" id="rname" required>
                    <label class="form__label" for="rname"><?= GetMessage('C_REQUEST_NAME') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="imr_t" class="form__input tel required" id="rtel" required>
                    <label class="form__label" for="rtel"><?= GetMessage('C_REQUEST_PHONE') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="imr_s" class="form__input" id="site" required>
                    <label class="form__label" for="site"><?= GetMessage('C_REQUEST_SITE') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="imr_c" class="form__input ignore" id="comment" required>
                    <label class="form__label" for="comment"><?= GetMessage('C_REQUEST_COMMENT') ?></label>
                </div>
                <div class="form__input-wrap gray-select">
                    <select name="imr_th" class="js-select" tabindex="1">
                        <option value="<?= GetMessage('C_REQUEST_THEME_1') ?>"><?= GetMessage('C_REQUEST_THEME_1') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_2') ?>"><?= GetMessage('C_REQUEST_THEME_2') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_3') ?>"><?= GetMessage('C_REQUEST_THEME_3') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_4') ?>" selected="selected"><?= GetMessage('C_REQUEST_THEME_4') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_5') ?>"><?= GetMessage('C_REQUEST_THEME_5') ?></option>
                    </select>
                </div>
                <div class="form__input-wrap">
                	<input type="checkbox" name="AGR" class="form__checkbox required" id="AGR_imr" checked="checked" required>
                	<label class="form__label agreement_label" for="AGR_imr">Я даю согласие на <a href="/policy/" target="_blank">обработку персональных данных</a></label>
                </div>
                <div class="btn welcome__form-btn js-btn">
                        <span class="btn__circle js-btn-effect">
                        </span>
                    <a href="javascript:void(0);" class="btn__inner js-btn-inner js-submit">
                        <span class="btn__text"><?= GetMessage('C_REQUEST_SEND') ?></span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal_form-request js-modal-tasks" data-izimodal-width="1193">
    <a href="javascript:void(0);" data-izimodal-close="" class="modal__close-container">
        <div class="modal__close-line"></div>
        <div class="modal__close-line"></div>
    </a>
    <div class="request__inner">
        <div class="request__form-wrap">
            <form class="request__form form js-tasks-form" action="/ajax/forms/main/">
                <p class="form__title form__title_big js-title"><?= GetMessage('C_REQUEST_TITLE') ?></p>
                <p class="form__subtitle"><?= GetMessage('C_REQUEST_SUBTITLE') ?></p>
                <div class="form__input-wrap">
                    <input type="text" name="im_n" class="form__input name required" id="tname" required>
                    <label class="form__label" for="tname"><?= GetMessage('C_REQUEST_NAME') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="im_s" class="form__input" id="tsite" required>
                    <label class="form__label" for="tsite"><?= GetMessage('C_REQUEST_SITE') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="im_e" class="form__input email required" id="temail" required>
                    <label class="form__label" for="temail"><?= GetMessage('C_REQUEST_EMAIL') ?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="im_t" class="form__input tel required" id="ttel" required>
                    <label class="form__label" for="ttel"><?= GetMessage('C_REQUEST_PHONE') ?>*</label>

                </div>
                <div class="form__input-wrap">
                    <select name="THEME" id="ttheme" class="js-select" tabindex="1">
                        <option value="1"><?= GetMessage('C_REQUEST_THEME_1') ?></option>
                        <option value="2"><?= GetMessage('C_REQUEST_THEME_2') ?></option>
                        <option value="3"><?= GetMessage('C_REQUEST_THEME_3') ?></option>
                        <option value="4"><?= GetMessage('C_REQUEST_THEME_4') ?></option>
                        <option value="5"><?= GetMessage('C_REQUEST_THEME_5') ?></option>
                    </select>
                    <label class="form__label form__label_active"
                           for="ttheme"><?= GetMessage('C_REQUEST_THEME') ?></label>
                </div>
                <div class="form__input-wrap form__input-wrap_auto">
                    <textarea name="im_c" class="form__input form__textarea ignore" id="tcomment"
                              required></textarea>
                    <label class="form__label" for="tcomment"><?= GetMessage('C_REQUEST_COMMENT') ?></label>
                </div>
                <div class="form__input-wrap">
                	<input type="checkbox" name="AGR" class="form__checkbox required" id="AGR_im" checked="checked" required>
                	<label class="form__label agreement_label" for="AGR_im">Я даю согласие на <a href="/policy/" target="_blank">обработку персональных данных</a></label>
                </div>
                <div class="btn welcome__form-btn js-btn">
                        <span class="btn__circle js-btn-effect">
                        </span>
                    <a href="javascript:void(0);" class="btn__inner js-btn-inner js-submit">
                        <span class="btn__text"><?= GetMessage('C_REQUEST_SEND') ?></span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>