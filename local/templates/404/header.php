<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID; ?>" lang="<?= LANGUAGE_ID; ?>">
<head>
    <? $APPLICATION->ShowProperty('AfterHeadOpen'); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <?

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addCss(ASSETS_PATH . "css/style.css");
    Asset::getInstance()->addJs(ASSETS_PATH . "js/script.bundle.js");
    Asset::getInstance()->addJs(ASSETS_PATH . "js/vendors.bundle.js");

    $APPLICATION->ShowHead();
    IncludeTemplateLangFile(__FILE__);
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        var modernBrowser = (
            'fetch' in window &&
            'assign' in Object
        );

        if (!modernBrowser) {
            var scriptElement = document.createElement('script');

            scriptElement.async = false;
            scriptElement.src = '<?=ASSETS_PATH?>js/polyfills.bundle.js';
            document.head.appendChild(scriptElement);
        }
    </script>
</head>
<body class="page not-found">
