<?
$MESS["C_REQUEST_TITLE"] = "Place your offer on Get4Click TITLE";
$MESS["C_REQUEST_FORM_TITLE"] = "Send a request to connect to Get4Click";
$MESS["C_REQUEST_EMAIL"] = "E-mail";
$MESS["C_REQUEST_NAME"] = "Name and surname";
$MESS["C_REQUEST_PHONE"] = "Phone";
$MESS["C_REQUEST_SITE"] = "Website";
$MESS["C_REQUEST_COMMENT"] = "Comments";
$MESS["C_REQUEST_SEND"] = "Send";
$MESS["C_REQUEST_THEME"] = "Theme";
$MESS["C_REQUEST_THEME_1"] = "Client Mailings";
$MESS["C_REQUEST_THEME_2"] = "Sales Growth";
$MESS["C_REQUEST_THEME_3"] = "Loyalty program";
$MESS["C_REQUEST_THEME_4"] = "Collaboration";
$MESS["C_REQUEST_THEME_5"] = "Subscribe to mailing lists";

$MESS["C_FOOTER_SEND_ME"] = "E-mail:";
$MESS["C_FOOTER_PD"] = "Privacy policy";
$MESS["C_FOOTER_OFERTA"] = "Terms of Use";

