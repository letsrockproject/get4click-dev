<?
$MESS["C_REQUEST_TITLE"] = "Разместить свой оффер в Get4Click";
$MESS["C_REQUEST_FORM_TITLE"] = "Отправьте заявку на подключение к Get4Click";
$MESS["C_REQUEST_EMAIL"] = "Ваш e-mail";
$MESS["C_REQUEST_NAME"] = "Ваше имя";
$MESS["C_REQUEST_PHONE"] = "Ваш телефон";
$MESS["C_REQUEST_SITE"] = "Адрес сайта";
$MESS["C_REQUEST_COMMENT"] = "Комментарии";
$MESS["C_REQUEST_SEND"] = "Отправить";
$MESS["C_REQUEST_THEME"] = "Тема";
$MESS["C_REQUEST_THEME_1"] = "Клиентские рассылки";
$MESS["C_REQUEST_THEME_2"] = "Рост продаж";
$MESS["C_REQUEST_THEME_3"] = "Программа лояльности";
$MESS["C_REQUEST_THEME_4"] = "Сотрудничество";
$MESS["C_REQUEST_THEME_5"] = "Подписка на рассылки";

$MESS["C_FOOTER_SEND_ME"] = "Свяжитесь с нами:";
$MESS["C_FOOTER_PD"] = "Политика обработки ПД";
$MESS["C_FOOTER_OFERTA"] = "Договор публичной оферты";

