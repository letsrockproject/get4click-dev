<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>

<?
//тестовый текст 456
?>
<?
//для каждого элемента находим его тип (раздел)
foreach ($arResult['ITEMS'] as &$item){
    $db_old_groups = CIBlockElement::GetElementGroups($item['ID'], false);
    if($parentGroupByItem = $db_old_groups->Fetch()) {
        $item['SECTION_NAME'] = $parentGroupByItem['NAME'];
        $item['SECTION_CODE'] = $parentGroupByItem['CODE'];
    }

    if (empty($item['PREVIEW_PICTURE']) and !empty($item['DETAIL_PICTURE'])){
        $item['PICTURE_ID'] = $item['DETAIL_PICTURE']['ID'];
    } elseif(!empty($item['PREVIEW_PICTURE'])) {
        $item['PICTURE_ID'] = $item['PREVIEW_PICTURE']['ID'];
    } else {
         switch ($item['IBLOCK_SECTION_ID']) {
                case IB_BLOG_SECTION_ARTICLES:
                    $item['PICTURE_ID'] = 347;
                    break;
                case IB_BLOG_SECTION_CASES:
                    $item['PICTURE_ID'] = 343;
                    break;
                case IB_BLOG_SECTION_NEWS:
                    $item['PICTURE_ID'] = 345;
                    break;
            }
    }

}
//вычисляем главный пост (сверху страницы)
$arResult['MAIN_POST'] = array();
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL","DETAIL_PICTURE",'SECTION_NAME',"PREVIEW_TEXT");
$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

if (!empty($arResult['SECTION']['PATH']['0']['ID']))
    $arFilter['SECTION_ID'] = $arResult['SECTION']['PATH']['0']['ID'];

$res = CIBlockElement::GetList(Array('property_ON_TOP'=>'DESC', 'date_active_from'=>'DESC'), $arFilter, false, Array("nTopCount"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $tempResult = $ob->GetFields();
    if (empty($tempResult['DETAIL_PICTURE'])) {
        switch ($tempResult['IBLOCK_SECTION_ID']) {
            case IB_BLOG_SECTION_ARTICLES:
                $tempResult['DETAIL_PICTURE'] = 346;
                break;
            case IB_BLOG_SECTION_CASES:
                $tempResult['DETAIL_PICTURE'] = 342;
                break;
            case IB_BLOG_SECTION_NEWS:
                $tempResult['DETAIL_PICTURE'] = 344;
                break;
        }
    }

    $arResult['MAIN_POST'] = $tempResult;
}
unset($tempResult);
//получаем популярные посты
$arResult['POPULAR_POSTS'] = array();
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL","PREVIEW_PICTURE",'SECTION_NAME',"PREVIEW_TEXT");
$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

if (!empty($arResult['SECTION']['PATH']['0']['ID']))
    $arFilter['SECTION_ID'] = $arResult['SECTION']['PATH']['0']['ID'];

$res = CIBlockElement::GetList(Array('show_counter'=>'DESC', 'date_active_from'=>'DESC'), $arFilter, false, Array("nTopCount"=>3), $arSelect);
while($ob = $res->GetNextElement())
{
    $tempResult = $ob->GetFields();
    $db_old_groups = CIBlockElement::GetElementGroups($tempResult['ID'], false);
    if($parentGroupByItem = $db_old_groups->Fetch()) {
        $tempResult['SECTION_NAME'] = $parentGroupByItem['NAME'];
        $tempResult['SECTION_CODE'] = $parentGroupByItem['CODE'];
    }

    if (empty($tempResult['PREVIEW_PICTURE'])){
        switch ($tempResult['IBLOCK_SECTION_ID']) {
            case IB_BLOG_SECTION_ARTICLES:
                $tempResult['PREVIEW_PICTURE'] = 347;
                break;
            case IB_BLOG_SECTION_CASES:
                $tempResult['PREVIEW_PICTURE'] = 343;
                break;
            case IB_BLOG_SECTION_NEWS:
                $tempResult['PREVIEW_PICTURE'] = 345;
                break;
        }
    }

    $arResult['POPULAR_POSTS'][] = $tempResult;
}
?>