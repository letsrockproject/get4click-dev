function getPager(html){
    var txt = html.split('<!-- pagination-container -->');
    return txt[1];
}
function getLi(html){
    var txt = html.split('<!-- items-container -->');
    return txt[1];
}
$(document).on('click','[data-click]',function (e) {
    if ($(this).data('prevent') == "Y")
        e.preventDefault();
    var fun = $(this).data('click');
    var $thisb=$(this);
    switch (fun) {
        case 'nextPage':
            var pagerBlock = $thisb.closest('.btn');
            var blockItems = $(document).find('div.items__blog');
            pagerBlock.hide();
            $.ajax({
                url: $thisb.attr('href'),
                success: function(data) {
                    blockItems.append(getLi(''+data+''));
                    $(document).find('.showmore').html(getPager(''+data+''));
                }
            });
            break;
    }
});

const maxTitleWord = 35;
if(document.documentElement.clientWidth <= 400) {
    [].forEach.call(document.querySelectorAll(".card-blog__title"),(item)=> {
        if(item.textContent.length >= maxTitleWord) {
            let newStr = item.textContent.slice(0,maxTitleWord);
            item.textContent = newStr + "..."
        };
    });
};