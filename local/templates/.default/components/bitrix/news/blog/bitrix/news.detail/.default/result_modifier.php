<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>

<?

$arResult['PICTURE_ID'];
if(!empty($arResult['DETAIL_PICTURE'])){
    $arResult['PICTURE_ID'] = $arResult['DETAIL_PICTURE']['ID'];
} elseif(!empty($arResult['DETAIL_PICTURE'])) {
    $arResult['PICTURE_ID'] = $arResult['PREVIEW_PICTURE']['ID'];
} else {
    switch ($arResult['IBLOCK_SECTION_ID']) {
        case IB_BLOG_SECTION_ARTICLES:
            $arResult['PICTURE_ID'] = 346;
            break;
        case IB_BLOG_SECTION_CASES:
            $arResult['PICTURE_ID'] = 342;
            break;
        case IB_BLOG_SECTION_NEWS:
            $arResult['PICTURE_ID'] = 344;
            break;
    }
}

$arResult['YOUR_INTERESTING'] = array();


$arSelect = Array("ID", "NAME", "ACTIVE_FROM",'PREVIEW_PICTURE','DETAIL_PICTURE','IBLOCK_SECTION_ID','SHOW_COUNTER','DETAIL_PAGE_URl');
$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y",'!=ID'=>$arResult['ID']);


//если свойство "рекомендуемые элементы" заполнено, то получаем только элементы из этого свойства
if (!empty($arResult['PROPERTIES']['RECOMMEND_ELEMS']['VALUE'])){
    $arFilter['ID'] = $arResult['PROPERTIES']['RECOMMEND_ELEMS']['VALUE'];

    //это нужно для последующей сортировки
    $re = array_flip($arResult['PROPERTIES']['RECOMMEND_ELEMS']['VALUE']);
}

$res = CIBlockElement::GetList(Array('IBLOCK_SECTION_ID'=>'desc','active_from'=>'desc'), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arTemp = $ob->GetFields();
    $parentsGroup = CIBlockElement::GetElementGroups($arTemp['ID'], false);
    while($parentGroup = $parentsGroup->Fetch()) {
        $arTemp['PARENT_SECTION_NAME'] = $parentGroup['NAME'];
        $arTemp['PARENT_SECTION_URL'] = "/blog/".$parentGroup['CODE']."/";
    }


    if(!empty($arTemp['PREVIEW_PICTURE'])){
        $arTemp['PICTURE_ID'] = $arTemp['PREVIEW_PICTURE'];
    } elseif(!empty($arTemp['DETAIL_PICTURE'])) {
        $arTemp['PICTURE_ID'] = $arTemp['DETAIL_PICTURE'];
    } else {
        switch ($arTemp['IBLOCK_SECTION_ID']) {
            case IB_BLOG_SECTION_ARTICLES:
                $arTemp['PICTURE_ID'] = 347;
                break;
            case IB_BLOG_SECTION_CASES:
                $arTemp['PICTURE_ID'] = 343;
                break;
            case IB_BLOG_SECTION_NEWS:
                $arTemp['PICTURE_ID'] = 345;
                break;
        }
    }

    if (!empty($arResult['PROPERTIES']['RECOMMEND_ELEMS']['VALUE'])) {
        $arTemp['SORT_CUSTOM'] =   $re[$arTemp['ID']];
    } else {
        $arTemp['CURR_SECTION'] =$arResult['IBLOCK_SECTION_ID'];
    }
    $arResult['YOUR_INTERESTING'][] = $arTemp;
}

// Сортируем и выводим получившийся массив
if (empty($arResult['PROPERTIES']['RECOMMEND_ELEMS']['VALUE'])) {
    global $currSectionID;
    $currSectionID = $arResult['IBLOCK_SECTION_ID'];

    function cmp($a)
    {
        if ($a['IBLOCK_SECTION_ID'] == $a['CURR_SECTION']) {
            return -1;
        } else {
            return 1;
        }
    }

    uasort($arResult['YOUR_INTERESTING'], 'cmp');
    $arResult['YOUR_INTERESTING'] = array_slice($arResult['YOUR_INTERESTING'], 0, 3);
} else {
    //сортируем элементы в том порядке, в котором их заполнили в админке
    function cmp2($a, $b)
    {
        if ($a['SORT_CUSTOM']== $b['SORT_CUSTOM']) {
            return 0;
        }
        return ($a['SORT_CUSTOM'] < $b['SORT_CUSTOM']) ? -1 : 1;
    }

    usort($arResult['YOUR_INTERESTING'], "cmp2");
}
?>
