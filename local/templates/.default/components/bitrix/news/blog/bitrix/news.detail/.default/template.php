<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

/*вычисляются в result_modifier:
 * $arResult['PICTURE_ID']
 * $arResult['YOUR_INTERESTING']
 *
 * */
$this->setFrameMode(true);


?>
<section class="welcome welcome_page_contacts js-welcome article__welcome">
    <div class="welcome__bg" id="js-welcome-bg"></div>
    <div class="welcome__inner container">
        <div class="article__back">
            <a href="/blog/">Блог</a>
        </div>
        <h1 class="welcome__title article__title"><?=$arResult['NAME']?></h1>
    </div>
    <div class="blog__slider-wrapper">
        <div class="article__item">
            <div class="card-blog card-blog_article">
                <a class="card-blog__icon ie-11-img">
                    <?
                    $picBig = CFile::ResizeImageGet($arResult['PICTURE_ID'], array('width'=>1200, 'height'=>450), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    ?>
                    <img src="<?=$picBig['src']?>" alt="icon article">
                </a>
                <div class="card-blog__row">
                    <div class="card-blog__descr">
                        <a class="card-blog__tag" href="<?=$arResult['SECTION']['PATH'][0]['SECTION_PAGE_URL']?>"><?=$arResult['SECTION']['PATH'][0]['NAME']?></a>
                        <div class="card-blog__date"><?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult['ACTIVE_FROM'], CSite::GetDateFormat()));?></div>
                    </div>
                    <div class="card-blog__views"><?=$arResult['SHOW_COUNTER']?></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="article__descr">
    <div class="article__wrapper">
        <p><?=$arResult['DETAIL_TEXT']?></p>
        <div class="article__social">
            <div class="article__row">
                <span class="article__share">Поделиться с друзьями:</span>
                <?/*
                <span class="icon icon_f"></span>
                <span class="icon icon_tw"></span>
                */?>
                <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="https://yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="facebook,twitter"></div>
            </div>
        </div>
    </div>
</section>
<section class="blog__all article__all">
    <h2 class="blog__title">Вас также могут заинтересовать:</h2>
    <div class="blog__row">
        <?foreach ($arResult['YOUR_INTERESTING'] as $elem){?>
            <div class="card-blog">
                <a class="card-blog__icon ie-11-img" href="<?=$elem['DETAIL_PAGE_URL']?>">
                    <?$pic = CFile::ResizeImageGet($elem['PICTURE_ID'], array('width'=>700, 'height'=>442), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                    <img src="<?=$pic['src']?>" alt="icon article">
                </a>
                <div class="card-blog__descr">
                    <a class="card-blog__tag" href="<?=$elem['PARENT_SECTION_URL']?>"><?=$elem['PARENT_SECTION_NAME']?></a>
                    <div class="card-blog__date"><?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($elem['ACTIVE_FROM'], CSite::GetDateFormat()));?></div>
                </div>
                <?echo var_dump()['DETAIL_PAGE_URL'];?>
                <a href="<?=$elem['DETAIL_PAGE_URL']?>" class="card-blog__title"><?=$elem['NAME']?></a>
                <div class="card-blog__views"><?=$elem['SHOW_COUNTER']?></div>
            </div>
        <?}?>
    </div>
</section>
