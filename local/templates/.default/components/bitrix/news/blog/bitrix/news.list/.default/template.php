<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

/*вычисляются в result_modifier:
 * $arResult['MAIN_POST']
 * $arResult['POPULAR_POSTS']
 * $arResult['ITEMS']['№ элемента']['SECTION_NAME']
 * */

$this->setFrameMode(true);
?>
<?//главный пост?>
<section class="welcome welcome_page_contacts js-welcome">
    <div class="welcome__bg" id="js-welcome-bg"></div>
    <div class="welcome__inner container">
        <h1 class="welcome__title">Блог</h1>
    </div>
    <div class="blog__slider-wrapper">
        <div class="blog__naviate-slider">
            <ul>
                <?
                $isActive = false;
                $arFilter = Array('IBLOCK_ID'=>17, 'GLOBAL_ACTIVE'=>'Y');
                $db_list = CIBlockSection::GetList(Array('SORT'=>'ASC'), $arFilter, true);
                while($section = $db_list->GetNext()){
                ?>
                <li class="blog_navigate-item <?if ($section['ID']==$arResult['SECTION']['PATH']['0']['ID'] and !$isActive){echo 'active'; $isActive=true;}?>">
                    <a href="<?= $section['SECTION_PAGE_URL'] ?>"> <?= $section['NAME'] ?></a>
                </li>
                <?}?>
                <li class="blog_navigate-item <?echo ($isActive ? '': 'active')?> "><a href="/blog/"> Показать все</a></li>
            </ul>
        </div>
        <div class="blog__slider js-slider-blog">
            <div class="blog__slider-item">
                <a href="<?=$arResult['MAIN_POST']['DETAIL_PAGE_URL']?>" class="blog__slider-link"></a>
                <?
                $picBig = CFile::ResizeImageGet($arResult['MAIN_POST']['DETAIL_PICTURE'], array('width'=>1200, 'height'=>450), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                ?>
                <img src="<?=$picBig['src']?>" alt="icon article">
                <div class="card-blog card-blog_slider">
                    <div class="card-blog__icon card-blog__mobile-icon">
                        <?
                        $picSmall = CFile::ResizeImageGet($arResult['MAIN_POST']['DETAIL_PICTURE'], array('width'=>900, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                        ?>
                        <img src="<?=$picSmall['src']?>" alt="icon article">
                    </div>
                    <div class="card-blog__descr">
                        <?
                        $db_old_groups = CIBlockElement::GetElementGroups($arResult['MAIN_POST']['ID'], false);
                        while($ar_group = $db_old_groups->Fetch()) {
                        ?>
                            <a class="card-blog__tag" href="/blog/<?=$ar_group['CODE']?>/"><?=$ar_group["NAME"]?></a>
                        <?}?>
                        <div class="card-blog__date">
                            <?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult['MAIN_POST']['DATE_ACTIVE_FROM'], CSite::GetDateFormat()));?>
                        </div>
                    </div>
                    <div class="card-blog__title"><?=$arResult['MAIN_POST']['NAME']?></div>
                    <div class="card-blog__content">
                        <?=$arResult['MAIN_POST']['PREVIEW_TEXT']?>
                    </div>
                    <a href="<?=$arResult['MAIN_POST']['DETAIL_PAGE_URL']?>" class="card-blog__more">
                        <span>Читать полностью</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?//популярное?>
<section class="blog__popular">
    <h2 class="blog__title blog__title_white">Самое популярное</h2>
    <div class="blog__row">
        <? foreach ($arResult['POPULAR_POSTS'] as $popular) {?>
            <div class="card-blog">
                <a class="card-blog__icon" href="<?=$popular['DETAIL_PAGE_URL']?>">
                    <?
                    $picPopular = CFile::ResizeImageGet($popular['PREVIEW_PICTURE'], array('width'=>700, 'height'=>442), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    ?>
                    <img src="<?=$picPopular['src']?>" alt="icon article">
                </a>
                <div class="card-blog__descr">
                    <a class="card-blog__tag" href="/blog/<?=$popular['SECTION_CODE']?>/"><?=$popular['SECTION_NAME']?></a>
                    <div class="card-blog__date">
                        <?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($popular['ACTIVE_FROM'], CSite::GetDateFormat()));?>
                    </div>
                </div>
                <a href="<?=$popular['DETAIL_PAGE_URL']?>" class="card-blog__title"><?=$popular['NAME']?></a>
                <div class="card-blog__views"><?=$popular['SHOW_COUNTER']?></div>
            </div>
        <? } ?>
    </div>
</section>
<?//общий список?>
<section class="blog__all">
    <h2 class="blog__title">Все  <?echo (strtolower($arResult['SECTION']['PATH']['0']['NAME']) ?: "записи")?></h2>
    <div class="blog__row items__blog">
        <!-- items-container -->
        <?foreach ($arResult['ITEMS'] as $arItem){
            ?>
            <div class="card-blog">
                <a class="card-blog__icon" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <?
                    $picItem = CFile::ResizeImageGet($arItem['PICTURE_ID'], array('width'=>700, 'height'=>442), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    ?>
                    <img src="<?=$picItem['src']?>" alt="icon article">
                </a>
                <div class="card-blog__descr">
                    <a class="card-blog__tag" href="/blog/<?=$arItem['SECTION_CODE']?>/"><?=$arItem["SECTION_NAME"]?></a>
                    <div class="card-blog__date">
                        <?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem['ACTIVE_FROM'], CSite::GetDateFormat()));?>
                    </div>
                </div>
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card-blog__title">
                    <?=$arItem['NAME']?></a>
                <div class="card-blog__views"><?=$arItem['SHOW_COUNTER']?></div>
            </div>
        <?}?>
        <!-- items-container -->
    </div>
    <!-- pagination-container -->
    <? if ($arResult['NAV_RESULT']->PAGEN < $arResult['NAV_RESULT']->NavPageCount) { ?>
        <div class="blog__row showmore" style="justify-content: center;">
            <div class="btn">
                <a href="?PAGEN_1=<?=++$arResult['NAV_RESULT']->PAGEN;?>" class="btn__inner js-btn-inner js-submit" data-click="nextPage" data-prevent="Y">
                    <span class="btn__text">Показать ещё</span>
                </a>
            </div>
        </div>
    <?}?>
    <!-- pagination-container -->
</section>


