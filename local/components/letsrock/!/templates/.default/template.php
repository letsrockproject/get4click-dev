<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<!-- Start Slider -->
<section id="mu-slider">
	<? foreach ($arResult as $item): ?>
	<div class="mu-slider-single">
		<div class="mu-slider-img" style="background-image: url('<?=$item['PROPERTY_BG_VALUE']['src']?>')">
		</div>
		<div class="mu-slider-content">
			<? if(!empty($item['PROPERTY_TOP_TEXT_VALUE'])): ?>
			<h4><?=$item['PROPERTY_TOP_TEXT_VALUE']?></h4>
			<span></span>
			<? endif; ?>
			<h2><?=$item['NAME']?></h2>
			<? if(!empty($item['PROPERTY_DESC_TEXT_VALUE'])): ?>
			<p><?=$item['PROPERTY_DESC_TEXT_VALUE']?></p>
			<? endif; ?>
			<? if(!empty($item['PROPERTY_BUTTON_TEXT_VALUE'])): ?>
			<a href="<?=$item['PROPERTY_BUTTON_LINK_VALUE']?>" class="mu-read-more-btn"><?=$item['PROPERTY_BUTTON_TEXT_VALUE']?></a>
			<? endif; ?>
		</div>
	</div>
	<? endforeach; ?>

</section>
<!-- End Slider -->