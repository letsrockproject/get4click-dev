<? use Letsrock\Lib\Models\Comments;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

class CommentsComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = [
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
        ];
        return $result;
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $partnersModel = new Comments();
            $arFields = $partnersModel->getAll();
            $this->arResult['COMMENTS'] = $arFields;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>