<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="comments js-comments">
    <div class="comments__inner container">
        <h3 class="comments__title"><?= GetMessage('C_COMMENTS_TITLE') ?></h3>
        <p class="comments__subtitle"><?= GetMessage('C_COMMENTS_SUBTITLE') ?></p>
        <div class="comments__slider js-slider">
            <? foreach ($arResult['COMMENTS'] as $item): ?>
                <div class="comments__slide js-slide">
                    <div class="comments__slide-inner">
                        <div class="comments__slide-image"
                             style="background-image: url('<?= $item['PREVIEW_PICTURE']['src'] ?>')"></div>

                        <p class="comments__slide-text"><?= $item['PREVIEW_TEXT'] ?></p>
                        <p class="comments__slide-title"><?= $item['PROPERTY_P_NAME_VALUE'] ?></p>
                        <p class="comments__slide-subtitle"><?= $item['PROPERTY_POSITION_VALUE'] ?></p>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
