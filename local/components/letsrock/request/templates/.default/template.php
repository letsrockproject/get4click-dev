<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<section class="request">
	<div class="request__inner container">
		<h3 class="request__title"><?= GetMessage('C_R_TITLE') ?></h3>
		<div class="request__form-wrap">
			<form class="request__form form js-request-form  js-tasks-form" action="/ajax/forms/main/">
				<p class="form__title"><?= GetMessage('C_R_FORM_TITLE') ?></p>
				<div class="form__input-wrap">
					<input type="text" name="r_e" class="form__input email required" id="remail" required>
					<label class="form__label" for="remail"><?= GetMessage('C_R_EMAIL') ?>*</label>
				</div>
				<div class="form__input-wrap">
					<input type="text" name="r_n" class="form__input name required" id="rname" required>
					<label class="form__label" for="rname"><?= GetMessage('C_R_NAME') ?>*</label>
				</div>
				<div class="form__input-wrap">
					<input type="text" name="r_t" class="form__input tel required" id="rtel" required>
					<label class="form__label" for="rtel"><?= GetMessage('C_R_PHONE') ?>*</label>
				</div>
				<div class="form__input-wrap">
					<input type="text" name="r_s" class="form__input" id="site" required>
					<label class="form__label" for="site"><?= GetMessage('C_R_SITE') ?>*</label>
				</div>
				<div class="form__input-wrap">
					<input type="text" name="r_c" class="form__input ignore" id="comment" required>
					<label class="form__label" for="comment"><?= GetMessage('C_R_COMMENT') ?></label>
				</div>
                <div class="form__input-wrap gray-select">
                    <select name="r_th" class="js-select" tabindex="1">
                        <option value="<?= GetMessage('C_REQUEST_THEME_1') ?>"><?= GetMessage('C_REQUEST_THEME_1') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_2') ?>"><?= GetMessage('C_REQUEST_THEME_2') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_3') ?>"><?= GetMessage('C_REQUEST_THEME_3') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_4') ?>"><?= GetMessage('C_REQUEST_THEME_4') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_5') ?>" selected="selected"><?= GetMessage('C_REQUEST_THEME_5') ?></option>
                    </select>
                    <label class="form__label form__label_active"><?= GetMessage('C_REQUEST_THEME') ?></label>
                </div>
                <div class="form__input-wrap">
                	<input type="checkbox" name="AGR" class="form__checkbox required" id="AGR_rd" checked="checked" required>
                	<label class="form__label agreement_label" for="AGR_rd">Я даю согласие на <a href="/policy/" target="_blank">обработку персональных данных</a></label>
                </div>
				<div class="btn welcome__form-btn js-btn">
                        <span class="btn__circle js-btn-effect">
                        </span>
					<a href="javascript:void(0);" class="btn__inner js-btn-inner js-submit" onclick="ym(53455198, 'reachGoal', 'sent_message'); return true;"
>
						<span class="btn__text"><?= GetMessage('C_R_SEND') ?></span>
					</a>
				</div>
			</form>
		</div>
	</div>
</section>