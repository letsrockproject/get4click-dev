<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<div class="not-found">
    <h3 class="not-found__title"><?=GetMessage('C_404_TITLE')?></h3>
    <div class="btn js-btn">
                        <span class="btn__circle js-btn-effect">
                        </span>
        <a href="/" class="btn__inner js-btn-inner">
            <span class="btn__text"><?= GetMessage('C_404_MAIN') ?></span>
        </a>
    </div>

</div>
