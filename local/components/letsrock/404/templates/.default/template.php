<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<div class="not-found">
    <a href="/" class="not-found__logo"></a>
    <h3 class="not-found__title"><?=GetMessage('C_404_TITLE')?></h3>
    <p class="not-found__subtitle"><?=GetMessage('C_404_SUBTITLE')?></p>
    <? $APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
        "COMPONENT_TEMPLATE" => ".default",
        "ROOT_MENU_TYPE" => MENU_TOP,    // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N",    // Тип кеширования
        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
        "MAX_LEVEL" => "1",    // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N",    // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
        "ADDITIONAL_CLASS" => "navigation",
        "CLASS" => 'navigation_404'
    ),
        false
    ); ?>

</div>
