<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="header__btn-lang btn-header btn-header_lang">
    <a class="btn-header__flag <?= $arResult['CURRENT_LANG'] ?>"></a>
    <? foreach ($arResult['LANGS'] as $name => $path): ?>
        <a href="<?=$path?>" class="btn-header__flag <?=$name?>">
            <span class="btn-header__flag-text"><?=ucfirst($name)?></span>
        </a>
    <? endforeach; ?>
</div>