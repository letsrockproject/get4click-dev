<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

class LangComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = [
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
        ];
        return $result;
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $langs = [
                'en' => 'https://get4click.com/',
                'ru' => 'https://get4click.ru/',
            ];

            unset($langs[LANGUAGE_ID]);
            $this->arResult['CURRENT_LANG'] = LANGUAGE_ID;
            $this->arResult['LANGS'] = $langs;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>