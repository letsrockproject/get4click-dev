<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Bitrix\Main\Loader::includeModule('letsrock.lib');

use Letsrock\Lib\Models\TrafficPartner;

class PartnersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = [
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
            "IBLOCK_ID" => isset($arParams["IBLOCK_ID"]) ? $arParams["IBLOCK_ID"] : IB_TRAFFIC_PARTNER,
        ];
        return $result;
    }

    /**
     * @return array|mixed
     */
    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $partnersModel = new TrafficPartner();
            $arFields = $partnersModel->getAll($this->arParams['IBLOCK_ID']);
            $this->arResult['BRENDS'] = $arFields;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>