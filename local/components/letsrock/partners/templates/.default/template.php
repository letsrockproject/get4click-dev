<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="partners">
    <div class="partners__inner container">
        <h3 class="partners__title"><?= GetMessage('C_PARTNERS_TITLE')?></h3>
        <p class="partners__subtitle"><?= GetMessage('C_PARTNERS_SUBTITLE')?></p>
        <div class="partners__logo-slider js-partners-slider">
            <? foreach ($arResult['BRENDS'] as $item): ?>
                <div class="partners__logo-slide">
                    <img class="partners__logo" src="<?= $item['PREVIEW_PICTURE']['src'] ?>" alt="">
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>

