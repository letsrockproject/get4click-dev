<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="advantages">
	<div class="advantages__triangle"></div>
	<div class="advantages__inner container">
		<div class="advantages__top-carts">
			<div class="advantages__cart">
				<p class="advantages__cart-title"><?=GetMessage('C_ADV_CART_1_NAME')?></p>
				<p class="advantages__cart-text"><?=GetMessage('C_ADV_CART_1_VALUE')?></p>
			</div>
			<div class="advantages__cart">
				<p class="advantages__cart-title"><?=GetMessage('C_ADV_CART_2_NAME')?></p>
				<p class="advantages__cart-text"><?=GetMessage('C_ADV_CART_2_VALUE')?></p>
			</div>
			<div class="advantages__cart">
				<p class="advantages__cart-title"><?=GetMessage('C_ADV_CART_3_NAME')?></p>
				<p class="advantages__cart-text"><?=GetMessage('C_ADV_CART_3_VALUE')?></p>
			</div>
		</div>
		<div class="advantages__bottom-carts">
			<div class="advantages__b-cart">
				<p class="advantages__b-cart-title"><?=GetMessage('C_ADV_CART_4_NAME')?></p>
				<p class="advantages__b-cart-text"><?=GetMessage('C_ADV_CART_4_VALUE')?></p>
			</div>
			<div class="advantages__b-cart">
				<p class="advantages__b-cart-title"><?=GetMessage('C_ADV_CART_5_NAME')?></p>
				<p class="advantages__b-cart-text"><?=GetMessage('C_ADV_CART_5_VALUE')?></p>
			</div>
			<div class="advantages__b-cart">
				<p class="advantages__b-cart-title"><?=GetMessage('C_ADV_CART_6_NAME')?></p>
				<p class="advantages__b-cart-text"><?=GetMessage('C_ADV_CART_6_VALUE')?></p>
			</div>
			<div class="advantages__b-cart">
				<p class="advantages__b-cart-title"><?=GetMessage('C_ADV_CART_7_NAME')?></p>
				<p class="advantages__b-cart-text"><?=GetMessage('C_ADV_CART_7_VALUE')?></p>
			</div>
		</div>
	</div>
</section>