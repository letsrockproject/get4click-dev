<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<section class="welcome js-welcome">
	<div class="welcome__bg" id="js-welcome-bg"></div>
	<div class="welcome__inner container">
		<h1 class="welcome__title welcome__title_partner text-centred"><?= GetMessage('C_WELCOME_TITLE') ?></h1>
		<p class="welcome__sub-title text-centred"><?= GetMessage('C_WELCOME_SUBTITLE') ?></p>
		<div class="welcome__bottom">
			<div class="welcome__cart">
				<p class="welcome__cart-title"><?= GetMessage('C_WELCOME_CART_1_NAME') ?></p>
				<p class="welcome__cart-text"><?= GetMessage('C_WELCOME_CART_1_VALUE') ?></p>
			</div>
			<div class="welcome__cart">
				<p class="welcome__cart-title"><?= GetMessage('C_WELCOME_CART_2_NAME') ?></p>
				<p class="welcome__cart-text"><?= GetMessage('C_WELCOME_CART_2_VALUE') ?></p>
			</div>
			<div class="welcome__cart">
				<p class="welcome__cart-title"><?= GetMessage('C_WELCOME_CART_3_NAME') ?></p>
				<p class="welcome__cart-text"><?= GetMessage('C_WELCOME_CART_3_VALUE') ?></p>
			</div>
			<div class="welcome__cart">
				<p class="welcome__cart-title"><?= GetMessage('C_WELCOME_CART_4_NAME') ?></p>
				<p class="welcome__cart-text"><?= GetMessage('C_WELCOME_CART_4_VALUE') ?></p>
			</div>
		</div>
	</div>
</section>
