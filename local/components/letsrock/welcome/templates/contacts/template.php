<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<section class="welcome welcome_page_contacts js-welcome">
	<div class="welcome__bg" id="js-welcome-bg"></div>
	<div class="welcome__inner container">
		<h1 class="welcome__title"><?= GetMessage('C_WELCOME_TITLE') ?></h1>
		<p class="welcome__sub-title"><?= GetMessage('C_WELCOME_SUBTITLE') ?></p>
		<div class="welcome__btn-block">
			<div class="btn welcome__btn js-btn">
				<div class="btn__circle js-btn-effect">
				</div>
				<a href="<?=PRESENTATION_PATH?>" target="_blank" class="btn__inner js-btn-inner">
					<span class="btn__text"><?= GetMessage('C_WELCOME_DOWNLOAD') ?></span>
				</a>
			</div>
			<a href="javascript:void(0);" class="welcome__btn welcome__btn_light js-send-request"><?= GetMessage('C_WELCOME_REQUEST') ?></a>
		</div>
	</div>
</section>