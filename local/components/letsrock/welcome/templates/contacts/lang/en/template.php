<?
$MESS["C_WELCOME_TITLE"] = "CPA program Get4Click:";
$MESS["C_WELCOME_SUBTITLE"] = "We know how to bring active online users to ecommerce and give an additional 7-10% increase in conversion rate for your project.";
$MESS["C_WELCOME_DOWNLOAD"] = "Download presentation";
$MESS["C_WELCOME_REQUEST"] = "Send request";
