<?
$MESS["C_WELCOME_TITLE"] = "Партнерская программа Get4Click:";
$MESS["C_WELCOME_SUBTITLE"] = "Приводим активных онлайн-покупателей в e-commerce.
                    Даем дополнительные 7-10% конверсии для вашего проекта";
$MESS["C_WELCOME_DOWNLOAD"] = "Скачать презентацию";
$MESS["C_WELCOME_REQUEST"] = "Отправьте заявку на подключение к Get4Click";
$MESS["C_WELCOME_EMAIL"] = "Ваш e-mail";
$MESS["C_WELCOME_NAME"] = "Ваше имя";
$MESS["C_WELCOME_PHONE"] = "Ваш телефон";
$MESS["C_WELCOME_SEND"] = "Присоединиться";
$MESS["C_WELCOME_CART_1_NAME"] = "Более 30 000";
$MESS["C_WELCOME_CART_1_VALUE"] = "покупателей внутри партнерской сети в день";
$MESS["C_WELCOME_CART_2_NAME"] = "ТОП-100 e‑commerce";
$MESS["C_WELCOME_CART_2_VALUE"] = "ваши партнеры по обмену трафиком";
$MESS["C_WELCOME_CART_3_NAME"] = "1 день";
$MESS["C_WELCOME_CART_3_VALUE"] = "нужен для интеграции в партнерскую программу";
$MESS["C_WELCOME_CART_4_NAME"] = "+7-10% конверсии";
$MESS["C_WELCOME_CART_4_VALUE"] = "дополнительно для вашего проекта";
$MESS["C_REQUEST_THEME_1"] = "Рост продаж";
$MESS["C_REQUEST_THEME_2"] = "Подписка на рассылки";
$MESS["C_REQUEST_THEME_3"] = "Программа лояльности";
$MESS["C_REQUEST_THEME_4"] = "Клиентские рассылки";
$MESS["C_REQUEST_THEME_5"] = "Сотрудничество";