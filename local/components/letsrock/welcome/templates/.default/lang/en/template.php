<?
$MESS["C_WELCOME_TITLE"] = "CPA program Get4Click:";
$MESS["C_WELCOME_SUBTITLE"] = "We know how to bring active online users to ecommerce and give an additional 7-10% increase in conversion rate for your project.";
$MESS["C_WELCOME_DOWNLOAD"] = "Скачать презентацию";
$MESS["C_WELCOME_REQUEST"] = "Register now to be a part of Get4Click";
$MESS["C_WELCOME_EMAIL"] = "Your e-mail";
$MESS["C_WELCOME_NAME"] = "Your name";
$MESS["C_WELCOME_PHONE"] = "Your mobile phone";
$MESS["C_WELCOME_SEND"] = "Sign up";
$MESS["C_WELCOME_CART_1_NAME"] = "30 000+";
$MESS["C_WELCOME_CART_1_VALUE"] = "buyers inside the network daily";
$MESS["C_WELCOME_CART_2_NAME"] = "Ecommerce TOP-100";
$MESS["C_WELCOME_CART_2_VALUE"] = "your traffic exchange partners";
$MESS["C_WELCOME_CART_3_NAME"] = "1 day";
$MESS["C_WELCOME_CART_3_VALUE"] = "to get registered in the program";
$MESS["C_WELCOME_CART_4_NAME"] = "+7-10% conversion";
$MESS["C_WELCOME_CART_4_VALUE"] = "for your project";