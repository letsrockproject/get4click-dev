<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="welcome welcome_main  js-welcome js-tasks-form">
    <div class="welcome__bg" id="js-welcome-bg"></div>
    <div class="welcome__inner container">
        <div class="welcome__top">
            <div class="welcome__text-block">
                <h1 class="welcome__title"><?=GetMessage('C_WELCOME_TITLE')?></h1>
                <p class="welcome__sub-title"><?=GetMessage('C_WELCOME_SUBTITLE')?></p>
            </div>
            <a href="javascript:void(0);" class="welcome__play-video js-play-video">
                <div class='welcome__wave'></div>
                <div class='welcome__wave welcome__wave_delay1'></div>
                <div class='welcome__wave welcome__wave_delay2'></div>
                <span class="welcome__play-icon"></span>
            </a>
            <form class="welcome__form form js-welcome-form" action="/ajax/forms/main/">
                <p class="form__title"><?=GetMessage('C_WELCOME_REQUEST')?></p>
                <div class="form__input-wrap">
                    <input type="text" name="wd_e" class="form__input email required" id="email" required>
                    <label class="form__label" for="email"><?=GetMessage('C_WELCOME_EMAIL')?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="wd_n" class="form__input name required" id="name" required>
                    <label class="form__label" for="name"><?=GetMessage('C_WELCOME_NAME')?>*</label>
                </div>
                <div class="form__input-wrap">
                    <input type="text" name="wd_t" class="form__input tel required" id="tel" required>
                    <label class="form__label" for="tel"><?=GetMessage('C_WELCOME_PHONE')?>*</label>
                </div>
                <div class="form__input-wrap gray-select">
                    <select name="wd_th" class="js-select" tabindex="1">
                        <option value="<?= GetMessage('C_REQUEST_THEME_1') ?>"><?= GetMessage('C_REQUEST_THEME_1') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_2') ?>"><?= GetMessage('C_REQUEST_THEME_2') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_3') ?>"><?= GetMessage('C_REQUEST_THEME_3') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_4') ?>"><?= GetMessage('C_REQUEST_THEME_4') ?></option>
                        <option value="<?= GetMessage('C_REQUEST_THEME_5') ?>" selected="selected"><?= GetMessage('C_REQUEST_THEME_5') ?></option>
                    </select>
                    <label class="form__label form__label_active"><?= GetMessage('C_REQUEST_THEME') ?></label>
                </div>
                <div class="form__input-wrap">
                	<input type="checkbox" name="AGR" class="form__checkbox required" id="AGR_wd" checked="checked" required>
                	<label class="form__label agreement_label" for="AGR_wd">Я даю согласие на <a href="/policy/" target="_blank">обработку персональных данных</a></label>
                </div>
                <div class="btn welcome__form-btn js-btn">
                    <span class="btn__circle js-btn-effect"></span>
                    <a href="javascript:void(0);" class="btn__inner js-btn-inner js-submit" onclick="ym(53455198, 'reachGoal', 'partner_click'); return true;">
                          <span class="btn__text">
                            <?=GetMessage('C_WELCOME_SEND')?>
                          </span>
                    </a>
                </div>
            </form>
        </div>
        <div class="welcome__bottom">
            <div class="welcome__cart">
                <p class="welcome__cart-title"><?=GetMessage('C_WELCOME_CART_1_NAME')?></p>
                <p class="welcome__cart-text"><?=GetMessage('C_WELCOME_CART_1_VALUE')?></p>
            </div>
            <div class="welcome__cart">
                <p class="welcome__cart-title"><?=GetMessage('C_WELCOME_CART_2_NAME')?></p>
                <p class="welcome__cart-text"><?=GetMessage('C_WELCOME_CART_2_VALUE')?></p>
            </div>
            <div class="welcome__cart">
                <p class="welcome__cart-title"><?=GetMessage('C_WELCOME_CART_3_NAME')?></p>
                <p class="welcome__cart-text"><?=GetMessage('C_WELCOME_CART_3_VALUE')?></p>
            </div>
            <div class="welcome__cart">
                <p class="welcome__cart-title"><?=GetMessage('C_WELCOME_CART_4_NAME')?></p>
                <p class="welcome__cart-text"><?=GetMessage('C_WELCOME_CART_4_VALUE')?></p>
            </div>
        </div>
    </div>
</section>
