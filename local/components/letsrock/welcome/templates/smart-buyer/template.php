<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="welcome js-welcome welcome-smart-buyer">
    <div class="welcome__bg" id="js-welcome-bg"></div>
    <div class="welcome__inner container">
        <h1 class="welcome__title welcome__title_sb text-centred"><?= GetMessage('C_W_TITLE') ?></h1>
        <p class="welcome__sub-title welcome__sub-title_sb text-centred"><?= GetMessage('C_W_SUBTITLE') ?></p>
        <form class="welcome__form form form_sb js-welcome-form welcome__" action="/ajax/forms/main/">
            <input type="hidden" name="ft" value="ws">
            <div class="form__sale-wrap">
                <div class="form__sale-block">
                    <span class="form__sale-text"><?= GetMessage('C_W_SALE') ?></span>
                </div>
            </div>
            <div class="form__input-wrap">
                <input type="text" name="ws_e" class="form__input email required" id="email" required>
                <label class="form__label" for="email"><?= GetMessage('C_W_FORM_EMAIL') ?>*</label>
            </div>
            <div class="form__input-wrap">
                <input type="text" name="ws_n" class="form__input name required" id="name" required>
                <label class="form__label" for="name"><?= GetMessage('C_W_FORM_NAME') ?>*</label>
            </div>
                <div class="form__input-wrap">
                	<input type="checkbox" name="AGR" class="form__checkbox required" id="AGR_ws" checked="checked" required>
                	<label class="form__label agreement_label" for="AGR_ws">Я даю согласие на <a href="/policy/" target="_blank">обработку персональных данных</a></label>
                </div>
            <div class="btn welcome__form-btn js-btn">
                <span class="btn__circle js-btn-effect"></span>
                <a href="javascript:void(0);" class="btn__inner js-btn-inner js-submit" onclick="ym(53455198, 'reachGoal', 'smart_buyers'); return true;">
                    <span class="btn__text"><?= GetMessage('C_W_FORM_BUTTON') ?></span>
                </a>
            </div>
        </form>
    </div>
</section>