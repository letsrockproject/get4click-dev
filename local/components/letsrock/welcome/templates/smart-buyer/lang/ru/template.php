<?
$MESS["C_W_TITLE"] = "Get4Click: скидки и промокоды для умных покупателей";
$MESS["C_W_SUBTITLE"] = "<strong>Подпишитесь</strong> на рассылку и получите возможность
<strong>выбрать скидку</strong> от ведущих интернет-магазинов России!";
$MESS["C_W_FORM_EMAIL"] = "Ваш e-mail";
$MESS["C_W_FORM_NAME"] = "Ваше имя";
$MESS["C_W_FORM_TEXT"] = "Подписываясь на рассылку вы принимаете Договор публичной оферты и Политику обработки персональных данных.";
$MESS["C_W_FORM_BUTTON"] = "Присоединиться";
$MESS["C_W_SALE"] = "Скидка";
?>