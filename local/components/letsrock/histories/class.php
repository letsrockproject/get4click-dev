<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Bitrix\Main\Loader::includeModule('letsrock.lib');

use Letsrock\Lib\Models\Histories;

class HistoriesComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = [
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
        ];
        return $result;
    }

    /**
     * @return array|mixed
     */
    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $partnersModel = new Histories();
            $arFields = $partnersModel->getAll();
            $this->arResult['HISTORIES'] = $arFields;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>