<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="histories histories_edge js-histories">
    <div class="histories__edge">
        <img src="/local/assets/img/job-bottom-edge.png" alt="">
    </div>
    <div class="histories__bottom">
        <div class="histories__inner container">
            <h3 class="histories__title"><?= GetMessage('C_HISTORIES_TITLE') ?></h3>
            <div class="histories__slider js-slider">
                <? foreach ($arResult['HISTORIES'] as $key => $item): ?>
                    <div class="histories__slide js-slide">
                        <div class="histories__slide-inner">
                            <div class="histories__slider-description">
                                <p class="histories__slider-number"><?= GetMessage('C_HISTORIES_KEYS') . ($key + 1) ?></p>
                                <p class="histories__slider-title"><?= $item['NAME'] ?></p>
                                <p class="histories__slider-description-text"><?= $item['PREVIEW_TEXT'] ?></p>
                                <div class="histories__slider-cart-table">
                                    <? if ($item['PROPERTY_CR_VALUE']): ?>
                                        <div class="histories__slider-cart">
                                            <p class="histories__slider-cart-title"><?= $item['PROPERTY_CR_VALUE'] ?></p>
                                            <p class="histories__slider-cart-subtitle">CR</p>
                                        </div>
                                    <? endif; ?>
                                    <? if ($item['PROPERTY_ROMI_VALUE']): ?>
                                        <div class="histories__slider-cart">
                                            <p class="histories__slider-cart-title"><?= $item['PROPERTY_ROMI_VALUE'] ?></p>
                                            <p class="histories__slider-cart-subtitle">ROMI</p>
                                        </div>
                                    <? endif; ?>
                                </div>
<!--                                <a href="javascript:void(0);"-->
<!--                                   class="histories__slider-link link">--><?//= GetMessage('C_HISTORIES_READ_ALL') ?><!--</a>-->
                            </div>
                            <div class="histories__slider-image-block">
                                <img src="<?= $item['PREVIEW_PICTURE']['src'] ?>" class="histories__slider-img"
                                     alt="<?= $item['NAME'] ?>">
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</section>


