<?
$MESS["C_TASKS_TITLE"] = "Ready solutions for main ecommerce challenges";
$MESS["C_TASKS_SUBTITLE"] = "You get a solution in all channles of ecommerce internet marketing by a single click.";
$MESS["C_TASKS_ITEM_1_TITLE"] = "Increase in sales";
$MESS["C_TASKS_ITEM_1_TEXT"] = "Get4Click advertisers get access to an audience that likes shopping. Providing individual discounts to such a clients you increase purchase possibility at your online store by many times.";
$MESS["C_TASKS_ITEM_1_B_TEXT"] = "JOIN";
$MESS["C_TASKS_ITEM_2_TITLE"] = "New loyal clients for your email customer base";
$MESS["C_TASKS_ITEM_2_TEXT"] = "Get4Click offers its clients newsletter subscription in the only right moment, when they see a full range of discount opportunities. And offers are too good to pass up! CTR of our base is +30%.";
$MESS["C_TASKS_ITEM_2_B_TEXT"] = "CONNECT";

$MESS["C_TASKS_ITEM_3_TITLE"] = "Your personal customer motivation program";
$MESS["C_TASKS_ITEM_3_TEXT"] = "Encourage your customers with offers from partners and provide discounts to partner network clients. Building a loyalty program has never been easier and conversion rates higher.";
$MESS["C_TASKS_ITEM_3_B_TEXT"] = "BUILD A PROGRAM";

$MESS["C_TASKS_ITEM_4_TITLE"] = "Get4Click client newsletter";
$MESS["C_TASKS_ITEM_4_TEXT"] = "Day by day Get4Click client database increases by new smart shoppers, that follow special offers and discounts. They are ready to start buying right now.
We can tell tham about youby two ways: discount digest (3-5 partners in a newsletter) or individual branded newsletter woth your unique offers. ";
$MESS["C_TASKS_ITEM_4_B_TEXT"] = "ORDER NEWSLETTER";

?>