<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<section class="tasks js-tasks">
    <div class="tasks__edge">
        <img src="/local/assets/img/job-bottom-edge.png" alt="">
    </div>
    <div class="tasks__inner container">
        <h3 class="tasks__title"><?= GetMessage('C_TASKS_TITLE') ?></h3>
        <p class="tasks__subtitle"><?= GetMessage('C_TASKS_SUBTITLE') ?></p>
        <div class="tasks__table">
            <div class="tasks__item tasks__item_wide">
                <p class="tasks__item-title"><?= GetMessage('C_TASKS_ITEM_1_TITLE') ?></p>
                <p class="tasks__item-text"><?= GetMessage('C_TASKS_ITEM_1_TEXT') ?></p>
                <a href="javascript:void(0);"
                   class="tasks__item-link link js-popup-link" data-select="1" data-title="Увеличить продажи" onclick="ym(53455198, 'reachGoal', 'order_dist'); return true;"><?= GetMessage('C_TASKS_ITEM_1_B_TEXT') ?></a>
            </div>
            <div class="tasks__item">
                <p class="tasks__item-title"><?= GetMessage('C_TASKS_ITEM_2_TITLE') ?></p>
                <p class="tasks__item-text"><?= GetMessage('C_TASKS_ITEM_2_TEXT') ?></p>
                <a href="javascript:void(0);"
                   class="tasks__item-link link js-popup-link" data-select="2" data-title="Увеличить продажи" onclick="ym(53455198, 'reachGoal', 'connect_system'); return true;"><?= GetMessage('C_TASKS_ITEM_2_B_TEXT') ?></a>
            </div>
            <div class="tasks__item">
                <p class="tasks__item-title"><?= GetMessage('C_TASKS_ITEM_3_TITLE') ?></p>
                <p class="tasks__item-text"><?= GetMessage('C_TASKS_ITEM_3_TEXT') ?></p>
                <a href="javascript:void(0);"
                   class="tasks__item-link link js-popup-link" data-select="5" data-title="Подписывать на рассылки" onclick="ym(53455198, 'reachGoal', 'connect_option'); return true;"><?= GetMessage('C_TASKS_ITEM_3_B_TEXT') ?></a>
            </div>
            <div class="tasks__item">
                <p class="tasks__item-title"><?= GetMessage('C_TASKS_ITEM_4_TITLE') ?></p>
                <p class="tasks__item-text"><?= GetMessage('C_TASKS_ITEM_4_TEXT') ?></p>
                <a href="javascript:void(0);"
                   class="tasks__item-link link js-popup-link" data-select="3" data-title="Подключить программу лояльности" onclick="ym(53455198, 'reachGoal', 'set_program'); return true;"><?= GetMessage('C_TASKS_ITEM_4_B_TEXT') ?></a>
            </div>
        </div>
    </div>
</section>