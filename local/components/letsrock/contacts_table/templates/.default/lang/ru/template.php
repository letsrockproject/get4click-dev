<?
$MESS["TITLE"] = "Контакты";
$MESS["SUBTITLE"] = "Свяжитесь с нами, и мы найдём для Вас индивидуальное решение.";
$MESS["TEL"] = "Телефон";
$MESS["EMAIL"] = "Е-mail";
$MESS["NAME"] = "Полное название";
$MESS["NAME_VALUE"] = "ООО «Гет Фор Клик»";
$MESS["UR_ADDRESS"] = "Адрес";
$MESS["UR_ADDRESS_VALUE"] = "г. Москва, ул. Ленинская Слобода, д.19, стр.1";
$MESS["INN"] = "ИНН";
$MESS["KPP"] = "КПП";
$MESS["OGRN"] = "ОГРН";
$MESS["RC"] = "Р/С";


?>