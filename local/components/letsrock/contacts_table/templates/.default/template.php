<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="contacts">
	<div class="contacts__inner container">
		<h3 class="contacts__title"><?=GetMessage('TITLE')?></h3>
		<p class="contacts__subtitle"><?=GetMessage('SUBTITLE')?></p>
		<div class="contacts__table">
			<div class="contacts__column">
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('TEL')?></span>
					<span class="ya-phone">+7(499)3912252</span>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('EMAIL')?></span>
					<a href="mailto:info@get4click.ru" class="contacts__value">info@get4click.ru</a>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('NAME')?></span>
					<span class="contacts__value"><?=GetMessage('NAME_VALUE')?></span>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('UR_ADDRESS')?></span>
					<span class="contacts__value"><?=GetMessage('UR_ADDRESS_VALUE')?></span>
				</div>
			</div>
			<div class="contacts__column">
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('INN')?></span>
					<span class="contacts__value">7730667150</span>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('KPP')?></span>
					<span class="contacts__value">772501001</span>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('OGRN')?></span>
					<span class="contacts__value">1127746486537</span>
				</div>
				<div class="contacts__row">
					<span class="contacts__type"><?=GetMessage('RC')?></span>
					<span class="contacts__value">40702810622000037011</span>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
.ya-phone 
	{
font-weight: bold;
text-align: right;
color: #262525;
display: block;
flex: 1 1 0px;
text-decoration: none;
	}

</style>