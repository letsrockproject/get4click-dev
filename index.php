<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Get4Click");
?>
<?
$APPLICATION->IncludeComponent('letsrock:welcome', '', [
    'MAIN' => 'Y'
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:job', '', [
    'BOTTOM_SECTION' => IB_JOB_MAIN_BOTTOM_SECTION,
    'TOP_SECTION' => IB_JOB_MAIN_TOP_SECTION,
    'MAIN' => 'Y'
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:tasks', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:partners', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:histories', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:comments', '', []);
?>

    <section class="histories js-histories news">
        <div class="histories__inner container">
            <h3 class="histories__title">Новости и статьи Get4Click</h3>
            <div class="histories__slider js-slider">
                <?

                CModule::IncludeModule("iblock");
                $arSelect = Array("ID", "NAME", "ACTIVE_FROM", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE",'IBLOCK_SECTION_ID');
                $arFilter = Array("IBLOCK_ID" => 17, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

                $res = CIBlockElement::GetList(Array('property_ON_INDEX' => 'DESC', 'date_active_from' => 'DESC'), $arFilter, false, Array("nTopCount" => 3), $arSelect);
                while ($ob = $res->GetNextElement()) {
                    $post = $ob->GetFields();
                    if (empty($tempResult['DETAIL_PICTURE'])) {
                        switch ($post['IBLOCK_SECTION_ID']) {
                            case IB_BLOG_SECTION_ARTICLES:
                                $tempResult['PREVIEW_PICTURE'] = 347;
                                break;
                            case IB_BLOG_SECTION_CASES:
                                $tempResult['PREVIEW_PICTURE'] = 343;
                                break;
                            case IB_BLOG_SECTION_NEWS:
                                $tempResult['PREVIEW_PICTURE'] = 345;
                                break;
                        }
                    }

                ?>

                <div class="histories__slide js-slide">
                    <div class="histories__slide-inner">
                        <a href="<?=$post['DETAIL_PAGE_URL']?>" class="histories__slide-link"></a>
                        <div class="histories__slider-description">
                            <div class="card-blog__descr">
                                <?
                                $db_old_groups = CIBlockElement::GetElementGroups($post['ID'], false);
                                if($parentGroup = $db_old_groups->Fetch()) {
                                    ?>
                                <a class="card-blog__tag" href="/blog/<?=$parentGroup['CODE']?>/"><?=$parentGroup['NAME']?></a>
                                    <?}?>
                                <div class="card-blog__date">
                                    <?=CIBlockFormatProperties::DateFormat('j M Y', MakeTimeStamp($post['ACTIVE_FROM'], CSite::GetDateFormat()));?>
                                </div>
                            </div>
                            <p class="histories__slider-title"><?=$post['NAME']?></p>
                            <p class="histories__slider-description-text">
                                <?
                                if (strlen($post['PREVIEW_TEXT'])>200){
                                    $previewText = strip_tags($post['PREVIEW_TEXT']);
                                    $previewText = substr($previewText, 0, 250);
                                    $previewText = rtrim($previewText, "!,.-");
                                    $previewText = substr($previewText, 0, strrpos($previewText, ' '));
                                    $previewText .= ' ...';
                                } else {
                                    $previewText = $post['PREVIEW_TEXT'];
                                }
                                ?>
                                <?=$previewText?>
                            </p>
                            <a href="<?=$post['DETAIL_PAGE_URL']?>" class="histories__slider-link link">Читать полностью</a>
                        </div>
                        <div class="histories__slider-image-block">
                            <?
                            $picPost = CFile::ResizeImageGet($post['PREVIEW_PICTURE'], array('width'=>700, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            ?>
                            <img src="<?=$picPost['src']?>" class="histories__slider-img"
                                 alt="<?=$post['NAME']?>">
                        </div>
                    </div>
                </div>
                <?}?>
            </div>
        </div>
    </section>
<?
$APPLICATION->IncludeComponent('letsrock:request', '', []);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>