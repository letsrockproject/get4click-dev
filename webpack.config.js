const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
    mode: 'development',
    entry: {
        'js/polyfills': './src/scripts/polyfills.js',
        'js/script': './src/scripts/index.js',
        'css/style': './src/styles/index.less',
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './local/assets',
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/pages/index.html',
            excludeChunks: ['polyfills'],
        }),
        new HtmlWebpackPlugin({
            template: './src/pages/contacts.html',
            excludeChunks: ['polyfills'],
            filename: 'contacts.html',
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        })],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, './local/assets'),
        publicPath: '',
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'js/vendors',
                    chunks: 'all',
                },
            },
        },
    },
    externals: {
        jQuery: {
            commonjs: 'jQuery',
            commonjs2: 'jQuery',
            amd: 'jQuery',
            root: 'jQuery',
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.(less)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'less-loader',
                ],
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)/,
                use: 'url-loader',
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'img',
                            publicPath: '/local/assets/img/',
                            name: '[name].[ext]',
                        },
                    },
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            interpolate: true
                        }
                    },
                ],
            },
        ],
    },
};