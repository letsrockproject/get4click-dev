import {BasePage} from "../base-page";
import {Btn} from "../components/btn";
import {Welcome} from "../components/welcome";
import {Accordeon} from "../components/accordeon";
import {Slider} from "../components/slider";
import {Histories} from "../components/histories";
import {Comments} from "../components/comments";
import {Header} from "../components/header";
import {MainForm} from "../form/main";
import {Tasks} from "../components/tasks";
import {TasksForm} from "../form/tasks";

import $ from "jquery/dist/jquery";
import "../vendor/izimodal";
import "select2/dist/js/select2.full.js";
import "../vendor/autosize.js";


class Index extends BasePage {
    init() {
        $(".modal").iziModal({
            'border-radius': '10px',
            onClosed: function () {
                const video = $("#youtube");
                const videoPath = video.attr("src");
                video.attr("src","");
                video.attr("src",videoPath);
            },
            onOpening: function () {
                $('#youtube').find('iframe:first').attr('allowfullscreen', 'allowfullscreen');
            }
        });

        $('.js-autosize').autosizeInput();

        this.$element.find('.js-btn').each((i, el) => {
            new Btn(el);
        });

        this.$element.find('.js-accordeon').each((i, el) => {
            new Accordeon(el);
        });

        this.$element.find('.js-welcome').each((i, el) => {
            new Welcome(el);
        });

        this.$element.find('.js-partners-slider').each((i, el) => {
            new Slider(el);
        });

        this.$element.find('.js-histories').each((i, el) => {
            new Histories(el);
        });

        this.$element.find('.js-comments').each((i, el) => {
            new Comments(el);
        });

        this.$element.find('.js-header').each((i, el) => {
            new Header(el);
        });

        this.$element.find('.js-welcome-form').each((i, el) => {
            new MainForm(el);
        });

        this.$element.find('.js-request-form').each((i, el) => {
            new MainForm(el);
        });

        this.$element.find('.js-tasks').each((i, el) => {
            new Tasks(el);
        });

        this.$element.find('.js-tasks-form').each((i, el) => {
            new TasksForm(el);
        });

    }
}

const page = new Index();