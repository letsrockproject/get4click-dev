import {BasePage} from "../base-page";
import {Btn} from "../components/btn";
import {Header} from "../components/header";
import {MainForm} from "../form/main";

import $ from "jquery/dist/jquery";
import  "../vendor/izimodal";
import "select2/dist/js/select2.full.js";


class Base extends BasePage {
    init() {
        $(".modal").iziModal();

        this.$element.find('.js-btn').each((i, el) => {
            new Btn(el);
        });

        this.$element.find('.js-header').each((i, el) => {
            new Header(el);
        });

        this.$element.find('.js-welcome-form').each((i, el) => {
            new MainForm(el);
        });

        this.$element.find('.js-request-form').each((i, el) => {
            new MainForm(el);
        });

    }
}

const base = new Base();