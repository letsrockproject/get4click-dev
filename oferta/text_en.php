<?
global $APPLICATION;
$APPLICATION->SetTitle("Privacy policy");
?>
<h2> Terms of Use </h2>
<p>These Get4Click Terms of Use (this “<strong>Agreement</strong>”) apply to your consumer use of the Get4Click website
    at www.get4click.com and all affiliated websites owned and operated solely by Get4Click (collectively the “Site”).
    This Agreement, together with the documents referred to on the Site, tells you the conditions and terms on which you
    may make use of the Site whether as a guest or a registered user. Please read this Agreement carefully before you
    start to use the Site. By using our Site, you indicate that you accept this Agreement and that you agree to abide by
    them. If you do not agree to this Agreement, please refrain from using our Site.</p>
<h3>INFORMATION ABOUT US</h3>
<p>This Site is operated by GET4CLICK Inc. («We» or „Get4Click”), a corporation registered in the State of Delaware.</p>
<h3>1. ACCESSING OUR SITE</h3>
<p>Access to our Site is permitted on a temporary basis, and we provide a services allowing you to request vouchers for
    a service or product offered by a Get4Click partner (“Voucher Services”) or to subscribe to our newsletter (the
    „Newsletter Services”; together with Voucher Services — “Services”). We reserve the right to withdraw or amend the
    Services at any time at our sole discretion. We will not be liable if for any reason our Site is unavailable at any
    time or for any period. From time to time, we may restrict your access to our Site or some part of it.</p>
<p>If you choose, or you are provided with, a user identification code, password or any other piece of information as
    part of our security procedures, you must treat such information as confidential, and you must not disclose it to
    any third party. We have the right to disable any user identification code or password, whether chosen by you or
    allocated by us, at any time, if in our opinion you have failed to comply with any of the provisions of this
    Agreement.</p>
<p>You are responsible for making all arrangements necessary for you to have access to our Site. You are also
    responsible for ensuring that all persons who access our Site through your internet connection are aware of these
    terms, and that they comply with them.</p>
<h3>2. REGISTRATION FOR VOUCHERS AND NEWSLETTER SERVICES</h3>
<p>If you request vouchers on our website (“Voucher Services”) or request to receive our newsletter (the „Newsletter
    Services”; together “Services”), you have to register on the relevant registration forms provided on our Site. In
    some instances, we may use your email address and name as supplied by a get4click partner, to automatically
    pre-populate a registration form or to enable us to send you the voucher you have requested without completion of a
    form. You will not be sent any offer from us unless you have requested it. By requesting a voucher or by signing up
    to any newsletter on our website you confirm that you agree with this Agreement and with our Privacy Policy . In
    order to receive our Voucher Services and Newsletter Services, the provision of your e-mail address is usually
    sufficient. However, additional data which you will be able to supply voluntarily on our Site may help us to improve
    communications with you.</p>
<p>You will receive a confirmation e-mail from us for each voucher requested and newsletter which you have subscribed
    for, after submission of the information required for the get4click Voucher Services or the get4click Newsletter
    Services registration. The get4click subscription can be terminated at any time by clicking on the unsubscribe link
    provided with the newsletters.</p>
<p>The get4click Voucher Services and the get4click Newsletter Services are provided subject to availability on the
    basis of the current technical, legal and economic conditions of the internet. You have no legal entitlement to
    receive the Voucher Services or the Newsletter Services and will be able to cancel the Newsletter Services at any
    time.</p>
<p>We hereby disclaim all liability and responsibility arising from any reliance placed by you on the contents of the
    vouchers and newsletters, as we cannot guarantee the accuracy and completeness of the contents of the vouchers and
    newsletters.</p>
<h3>3. INTELLECTUAL PROPERTY RIGHTS</h3>
<p>We are the owner or the licensee of all intellectual property rights in our Site, and in the material published on
    it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved. You may
    print off one copy, and may download extracts, of any page(s) from our Site for your personal reference and you may
    draw the attention of others within your organisation to material posted on our Site.</p>
<p>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and
    you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any
    accompanying text. Our status (and that of any identified contributors) as the authors of material on our Site must
    always be acknowledged. You must not use any part of the materials on our Site for commercial purposes without
    obtaining a licence to do so from us or our licensors.</p>
<p>If you print off, copy or download any part of our Site in breach of this Agreement, your right to use our Site will
    cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>
<h3>4. RELIANCE ON INFORMATION POSTED</h3>
<p>Commentary and other materials posted on our Site are not intended to amount to advice on which reliance should be
    placed. We therefore disclaim arising from any reliance placed on such materials by any visitor to our Site, or by
    anyone who may be informed of any of its contents.</p>
<h3>5. OUR SITE CHANGES REGULARLY</h3>
<p>We aim to update our Site regularly, and may change the content at any time. If the need arises, we may suspend
    access to our Site, or close it indefinitely. Any of the material on our Site may be out of date at any given time,
    and we are under no obligation to update such material.</p>
<h3>6. LIABILITY</h3>
<p>THE MATERIAL DISPLAYED ON OUR SITE IS PROVIDED WITHOUT ANY GUARANTEES, CONDITIONS OR WARRANTIES AS TO ITS ACCURACY.
    TO THE EXTENT PERMITTED BY LAW, WE, OTHER MEMBERS OF OUR GROUP OF COMPANIES AND THIRD PARTIES CONNECTED TO US HEREBY
    EXPRESSLY EXCLUDE:</p>
<p>ALL CONDITIONS , WARRANTIES, AND OTHER TERMS WHICH MIGHT OTHERWISE BE IMPLIED BY STATUTE, COMMON LAW OR THE LAW OF
    EQUITY<br>
    ANY LIABILITY FOR ANY DIRECT, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL LOSS OR DAMAGES INCURRED BY
    YOU IN CONNECTION WITH OUR SITE OR IN CONNECTION WITH THE USE, INABILITY TO USE OR RESULTS OF THE USE OF OUR SITE
    AND ANY WEBSITES LINKED TO IT AND ANY MATERIALS POSTED ON IT, INCLUDING, WITHOUT LIMITATION, ANY LIABILITY FOR LOSS
    OF INCOME OR REVENUE, LOSS OF BUSINESS, LOSS OF PROFITS OR CONTRACTS, LOSS OF ANTICIPATED SAVINGS, LOSS OF DATA,
    LOSS OF GOODWILL, WASTED MANAGEMENT, OR OFFICE TIME, AND FOR ANY OTHER LOSS OR DAMAGE OF ANY KIND, HOWEVER ARISING
    AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORSEEABLE provided that
    this condition shall not prevent claims for loss of or damage to your tangible property or any other claims for
    direct financial loss that are not excluded by any of the categories set out above.</p>
<p>This does not affect our liability for death or personal injury arising from our negligence, nor our liability for
    fraudulent misrepresentation or misrepresentation as to a fundamental matter, nor any other liability which cannot
    be excluded or limited under applicable law.</p>
<h3>7. DISCLAIMER OF WARRANTIES</h3>
<p>THE NEWSLETTER SERVICES, VOUCHER SERVICES, AND ANY OTHER INFORMATION, PRODUCTS, AND MATERIALS CONTAINED IN OR
    ACCESSED THROUGH THE SERVICES, ARE PROVIDED TO YOU ON AN “AS IS” BASIS AND WITHOUT WARRANTY OF ANY KIND. GET4CLICK
    EXPRESSLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES, CONDITIONS, OR INDEMNITIES, EXPRESS OR IMPLIED, INCLUDING,
    WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, OR NON-INFRINGEMENT,
    OR ANY WARRANTY ARISING FROM A COURSE OF DEALING, PERFORMANCE, OR TRADE USAGE. GET4CLICK DOES NOT WARRANT THAT YOUR
    USE OF THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, THAT GET4CLICK WILL REVIEW THE INFORMATION OR MATERIALS
    MADE AVAILABLE THROUGH THE SERVICES FOR ACCURACY OR THAT IT WILL PRESERVE OR MAINTAIN ANY SUCH INFORMATION OR
    MATERIALS WITHOUT LOSS. GET4CLICK SHALL NOT BE LIABLE FOR DELAYS, INTERRUPTIONS, SERVICE FAILURES, OR OTHER PROBLEMS
    INHERENT IN USE OF THE INTERNET AND ELECTRONIC COMMUNICATIONS OR OTHER SYSTEMS OUTSIDE THE REASONABLE CONTROL OF
    GET4CLICK.</p>
<p>THE FOREGOING DISCLAIMERS APPLY TO THE MAXIMUM EXTENT PERMITTED BY LAW. YOU MAY HAVE OTHER STATUTORY RIGHTS. HOWEVER,
    THE DURATION OF STATUTORILY REQUIRED WARRANTIES, IF ANY, SHALL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.
</p>
<h3>8. VIRUSES, HACKING AND OTHER OFFENCES</h3>
<p>You must not misuse our Site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is
    malicious or technologically harmful. You must not attempt to gain unauthorised access to our Site, the server on
    which our Site is stored or any server, computer or database connected to our Site. You must not attack our Site via
    a denial-of-service attack or a distributed denial-of service attack.</p>
<p>By breaching this provision, you would commit a criminal offence under the relevant federal or state acts. We will
    report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by
    disclosing your identity to them. In the event of such a breach, your right to use our Site will cease
    immediately.</p>
<p>We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other
    technologically harmful material that may infect your computer equipment, computer programs, data or other
    proprietary material due to your use of our Site or to your downloading of any material posted on it, or on any
    webSite linked to it.</p>
<h3>9. LINKING TO OUR SITE AND LINKS FROM OUR SITE</h3>
<p>Our Site must not be framed on any other Site, nor may you create a link to any part of our Site without prior
    written permission. We reserve the right to withdraw linking permission without notice. If you wish to make any use
    of material on our Site please address your request to <a href="mailto:info@get4click.com">info@get4click.com</a>
</p>
<p>Where our Site contains links to other Sites and resources provided by third parties, these links are provided for
    your information only. We have no control over the contents of those Sites or resources, and accept no
    responsibility for them or for any loss or damage that may arise from your use of them.</p>
<h3>10. SEVERABILITY</h3>
<p>If any of the provisions, or portions thereof, of this Agreement are found to be invalid under any applicable statute
    or rule of law, then, that provision (or portion thereof) notwithstanding, this Agreement shall remain in full force
    and effect and such provision or portion thereof shall be deemed omitted.</p>
<h3>11. ASSIGNMENT</h3>
<p>This Agreement and the rights granted and obligations undertaken hereunder may not be transferred, assigned, or
    delegated by you in any manner, but may be freely transferred, assigned, or delegated by Get4Click.</p>
<h3>12. WAIVER</h3>
<p>Any waiver of any provision of this Agreement, or a delay by any party in the enforcement of any right hereunder,
    shall neither be construed as a continuing waiver nor create an expectation of non-enforcement of that or any other
    provision or right.</p>
<h3>13. ARBITRATION AGREEMENT AND JURY TRIAL WAIVER, CLASS ACTION WAIVER, AND FORUM SELECTION CLAUSE</h3>
<p>Any and all controversies, disputes, demands, counts, claims, or causes of action (including the interpretation and
    scope of this clause, and the arbitrability of the controversy, dispute, demand, count, claim, or cause of action)
    between you and the Get4Click or their successors or assigns shall exclusively be settled through binding and
    confidential arbitration.</p>
<p>Arbitration shall be subject to the Federal Arbitration Act and not any state arbitration law. The arbitration shall
    be conducted before one commercial arbitrator with substantial experience in resolving commercial contract disputes
    from the American Arbitration Association (“<strong>AAA</strong>”). As modified by this Agreement, and unless
    otherwise agreed upon by the parties in writing, the arbitration will be governed by the AAA’s Commercial
    Arbitration Rules and, if the arbitrator deems them applicable, the Supplementary Procedures for Consumer Related
    Disputes (collectively, the “<strong>Rules and Procedures</strong>”).</p>
<p>You are thus GIVING UP YOUR RIGHT TO GO TO COURT to assert or defend your rights EXCEPT for matters that you file in
    small claims court. Your rights will be determined by a NEUTRAL ARBITRATOR and NOT a judge or jury. You are entitled
    to a FAIR HEARING, BUT the arbitration procedures are SIMPLER AND MORE LIMITED THAN RULES APPLICABLE IN COURT.
    Arbitrator decisions are as enforceable as any court order and are subject to VERY LIMITED REVIEW BY A COURT.</p>
<p>You and Get4Click must abide by the following rules: (1) ANY CLAIMS BROUGHT BY YOU OR GET4CLICK MUST BE BROUGHT IN
    THE PARTY’S INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR REPRESENTATIVE
    PROCEEDING; (2) THE ARBITRATOR MAY NOT CONSOLIDATE MORE THAN ONE PERSON’S CLAIMS, MAY NOT OTHERWISE PRESIDE OVER ANY
    FORM OF A REPRESENTATIVE OR CLASS PROCEEDING, AND MAY NOT AWARD CLASS-WIDE RELIEF; (3) in the event that you are
    able to demonstrate that the costs of arbitration will be prohibitive as compared to costs of litigation, Get4Click
    will pay as much of your filing and hearing fees in connection with the arbitration as the arbitrator deems
    necessary to prevent the arbitration from being cost-prohibitive as compared to the cost of litigation, (4)
    Get4Click also reserves the right in its sole and exclusive discretion to assume responsibility for all of the costs
    of the arbitration; (5) the arbitrator shall honor claims of privilege and privacy recognized at law; (6) the
    arbitration shall be confidential, and neither you nor we may disclose the existence, content or results of any
    arbitration, except as may be required by law or for purposes of enforcement of the arbitration award; (7) the
    arbitrator may award any individual relief or individual remedies that are permitted by applicable law; and (8) each
    side pays its own attorneys’ fees and expenses unless there is a statutory provision that requires the prevailing
    party to be paid its fees and litigation expenses, and, in such instance, the fees and costs awarded shall be
    determined by the applicable law.</p>
<p>Notwithstanding the foregoing, either you or Get4Click may bring an individual action in small claims court. Further,
    claims of defamation, violation of the Computer Fraud and Abuse Act, and infringement or misappropriation of the
    other party’s patent, copyright, trademark, or trade secret shall not be subject to this arbitration agreement. Such
    claims shall be exclusively brought in the state or federal courts located in New York, New York. Additionally,
    notwithstanding this agreement to arbitrate, either party may seek emergency equitable relief before the state or
    federal courts located in New York, New York in order to maintain the status quo pending arbitration, and hereby
    agree to submit to the exclusive personal jurisdiction of the courts located within New York, New York for such
    purpose. A request for interim measures shall not be deemed a waiver of the right to arbitrate.</p>
<p>With the exception of subparts (1) and (2) in this Section (prohibiting arbitration on a class or collective basis),
    if any part of this arbitration provision is deemed to be invalid, unenforceable, or illegal, or otherwise conflicts
    with the Rules and Procedures, then the balance of this arbitration provision shall remain in effect and shall be
    construed in accordance with its terms as if the invalid, unenforceable, illegal or conflicting part was not
    contained herein. If, however, either subpart (1) or (2) is found to be invalid, unenforceable, or illegal, then the
    entirety of this arbitration provision shall be null and void, and neither you nor Get4Click shall be entitled to
    arbitration. If for any reason a claim proceeds in court rather than in arbitration, the dispute shall be
    exclusively brought in state or federal court located in New York, New York.</p>
<p>For more information on AAA, the Rules and Procedures, or the process for filing an arbitration claim, you may call
    AAA at 800-778-7879 or visit the AAA website at http://www.adr.org.</p>
<h3>14. PAGE AMENDMENTS</h3>
<p>We may revise this Agreement at any time by amending this page. You are expected to check this page from time to time
    to take notice of any changes we made, as they are binding on you. Some of the provisions contained in this
    Agreement may also be superseded by provisions or notices published elsewhere on our Site.</p>
<h3>15. INFORMATION ABOUT YOU AND YOUR VISITS TO OUR SITE</h3>
<p>We process information about you in accordance with our <a href="/privacy/">Privacy Policy</a>. By using our Site,
    you consent to such processing and you warrant that all data provided by you is accurate. </p>
<h3>16.&nbsp;CONTACT</h3>
<p>If you have any concerns about material which appears on our Site, please email us at <a
            href="mailto:info@get4click.com">info@get4click.com</a> or write to us at:</p>
<p>
    GET4CLICK Inc.<br><br>
    Telephone: +1 617-580-3545<br><br>
    Registered Office: 210 Broadway, Suite 201<br><br>
    Cambridge, MA 02139
</p>
<p>Thank you for visiting our Site.</p>