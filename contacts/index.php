<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage('CONTACTS'));
?>
<?
$APPLICATION->IncludeComponent('letsrock:welcome', 'contacts', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:advantages', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:contacts_table', '', []);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>