<?
global $APPLICATION;
$APPLICATION->SetTitle("Privacy policy");
?>
<h2>Privacy policy</h2>
<p>This privacy policy (this “ Privacy Policy”) together with our terms of use and any other documents referred to on
    it) sets out the basis on which any personal data is collected, used and disclosed by GET4CLICK Inc («We”).</p>
<p>Please read the following carefully to understand our views and practices regarding your personal data and how we
    will treat it. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same
    meanings as in our Terms of Service</p>
<h3>1. INFORMATION WE MAY COLLECT FROM YOU</h3>
<p>We may collect and process the following data about you:</p>
<ul>
    <li>Information that you provide by filling in forms on this webite – [www.get4click.com] („Site”) including but not
        limited to your email address, name and title. This includes information provided at the time of registering to
        use our site, subscribing to our Services, posting material or requesting further services. We may also ask you
        for information when you report a problem with our Site.
    </li>
    <li>If you contact us, we may keep a record of that correspondence.</li>
    <li>We may also ask you to complete surveys that we use for research purposes, although you do not have to respond
        to them.
    </li>
    <li>Details of transactions you carry out through our site and of the fulfilment of your orders for services
        provided by us including but not limited to details of any vouchers or newsletters requested by you.
    </li>
    <li>Details of your visits to our site including, but not limited to, timing data, traffic data, location data,
        weblogs and other communication data, whether this is required for our own administrative purposes or otherwise
        and the resources that you access.
    </li>
</ul>
<h3>2. IP ADDRESSES</h3>
<p>We may collect information about your computer, including. where available, your IP address, operating system and
    browser type for system administration which shall include security checks made by us from time to time to ensure
    the proper functioning of the system. This is statistical data about our users’ browsing actions and patterns, and
    does not identify any individual.</p>
<h3>3. COOKIES</h3>
<p>For the same reason, we may obtain information about your general internet usage by using a cookie file which is
    stored on your browser or the hard drive of your computer. Cookies contain information that is transferred to your
    computer’s hard drive. They help us to improve our site and to deliver a better and more personalised service. Some
    of the cookies we use are essential for the site to operate. If you register with us or if you continue to use our
    site, you agree to our use of cookies.</p>
<p>Cookies are widely used in order to make websites work, or work more efficiently, as well as to provide information
    to the owners of the site.</p>
<p>We may include cookies on the websites of our business partners participating in the services we offer (<strong>“Get4click
        Partners”</strong>) that allow us to present you with advertising or offers on our site based on interests
    inferred from your web browsing and/or web transaction activity and your use of our Service.</p>
<p>Please note that our advertisers and <strong>Get4click</strong> Partners may also use cookies, over which we have no
    control. You can block cookies by activating the setting on your browser which allows you to refuse the setting of
    all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies)
    you may not be able to access all or parts of our site. Unless you have adjusted your browser setting so that it
    will refuse cookies, our system will issue cookies.<br>
    If you have any questions about the cookies used on our site please do not hesitate to email us at <a
            href="emailto:info@get4click.com">info@get4click.com</a>.
</p>
<h3>WHAT ARE COOKIES</h3>
<p>Cookies are small text files created by a web page server that are placed on your computer or mobile phone when you
    browse websites. These cookies help make the interaction between you and a website faster and easier because they
    can remember things like the last time you visited a site, websites you have previously visited and which products
    you are interested in. This information is then used to serve you more relevant information based on your shopping
    behaviour giving you a richer experience when shopping online.</p>
<p>There are two different types of cookies — the ones we use on <strong>Get4click.com</strong> (we call these first
    party cookies) and the ones we place on our partners’ websites (we call these third party cookies).
</p>
<h3>HOW DOES <strong>Get4click</strong> USE COOKIES?</h3>
<p>We use first party cookies to tell us that you have previously requested a voucher from the
    <strong>Get4click</strong> voucher hub and have already accepted our privacy policy and terms and conditions. This
    means that we do not need to ask you to fill in your name and email address again, and read and agree to the terms
    every time you choose a voucher from our voucher hub, thus saving you time and effort. We use third party cookies to
    identify if you have previously shopped on one of our partners’ websites. These are placed when you buy something at
    a partner site in order to allow us to present you with the most relevant offers and discounts when you visit our
    voucher hub. If you have disabled third party cookies on some or all of the websites you shop at, then this may
    affect the vouchers <strong>Get4click</strong> selects for you.</p>
<p>To understand how to enable or disable your cookies see the Managing your cookies section. To request vouchers on the
    <strong>Get4click</strong> voucher hub you do not actually need to have either your first party or third party
    cookies enabled but disabling these will prevent us from giving you the best service and most relevant offers. For
    more detailed information about cookies please go to <a href="http://www.aboutcookies.org" target="_blank">www.aboutcookies.org</a>
</p>
<h3>GRANTING US PERMISSION TO USE COOKIES</h3>
<p>If the settings on your software that you are using to view the Site (your browser) are adjusted to accept cookies,
    we take this and your continued use of our site to mean that you are happy about this. Should you wish to remove or
    not use cookies from our site you can learn how to do this by going to the Managing your cookies section</p>
<p>Nevertheless, you should be aware that disabling cookies is likely to limit the functionality of both our website and
    a large proportion of the world’s websites as cookies are a standard part of most modern websites.</p>
<h3>MANAGING YOUR COOKIES</h3>
<p>You can enable or disable the cookie settings on your computer. How you do this depends on what type of computer you
    have (MAC or PC) and what browser you use.</p>
<p>PC: In order to check your cookie settings on a PC you need to click Help and then choose About. Follow the link
    about cookies. There are a lot of different browsers so check which one you are using before you follow the
    instructions below.</p>
<h4>Microsoft Internet Explorer 6.0, 7.0, 8.0</h4>
<ol>
    <li>Click on Tools at the top of your browser window, select Internet options, then click on the Privacy tab.</li>
    <li>Ensure that your Privacy level is set to Medium or below, which will enable cookies in your browser.</li>
    <li>Settings above Medium will disable cookies.</li>
</ol>
<h4>Mozilla Firefox</h4>
<ol>
    <li>Click on ‘Tools’ at the top of your browser window, and then select ‘Options’, afterwards select the Privacy
        icon.
    </li>
    <li>Under History, select ‘Use custom settings for history’ from the Firefox will drop-down box.</li>
    <li>Choose whether to ‘Accept cookies from sites’ and/or ‘Accept third-party cookies.</li>
</ol>
<h4>Google Chrome</h4>
<ol>
    <li>Click the wrench icon on the browser toolbar.</li>
    <li>Select Settings.</li>
    <li>Click Show advanced settings.</li>
    <li>In the ‘Privacy’ section, click the Content settings button.</li>
    <li>In the ‘Cookies’ section, you can change the following cookies settings:
        <ul>
            <li>Delete cookie</li>
            <li>Block cookies by default</li>
            <li>Allow cookies by default</li>
            <li>Make exceptions for cookies from specific websites or domains</li>
            <ul>

            </ul>
        </ul>
    </li>
</ol>
<h5>Safari</h5>
<ol>
    <li>Click on the Cog icon at the top of your browser window and select the Preferences option</li>
    <li>Click on Privacy, and choose whether you want to always block cookies from third parties and advertisers, always
        block all cookies or never block any cookies.
    </li>
    <li>You can also view all current cookies currently stored on your computer.</li>
</ol>
<p>Macs: To check cookies are enabled for Macs click on the apple menu and select the option called About</p>
<h4>Safari on OSX</h4>
<ol>
    <li>Click on Safari at the top of your browser window and select the Preferences option</li>
    <li>Click on Security then ‘Accept cookies’</li>
    <li>Click on Privacy, and choose whether you want to always block cookies from third parties and advertisers, always
        block all cookies or never block any cookies.
    </li>
    <li>You can also view all current cookies currently stored on your computer.</li>
</ol>
<h4>Mozilla Firefox on OSX</h4>
<ol>
    <li>Click on Firefox at the top of your browser window and select the Preferences option, then select the Privacy
        icon
    </li>
    <li>Under ‘History’, select ‘Use custom settings for history’ from the Firefox will drop-down box</li>
    <li>Choose whether to ‘Accept cookies from sites’ and/or ‘Accept third-party cookies’</li>
</ol>
<h4>Microsoft Internet Explorer 5.0 on OSX</h4>
<ol>
    <li>Click on Explorer at the top of your browser window and select Preferences options</li>
    <li>Scroll down until you see Cookies under Receiving Files</li>
    <li>Select the ‘Never Ask’ option</li>
</ol>
<h4>Netscape on OSX</h4>
<ol>
    <li>Click on ‘Netscape’ at the top of your browser window and select the ‘Preferences’ option</li>
    <li>Scroll down until you see cookies under ‘Privacy &amp; Security’</li>
    <li>Select ‘Enable cookies for the originating web site only’</li>
</ol>
<h4>Opera</h4>
<ol>
    <li>Click on Opera Next at the top of your browser window and select Settings</li>
    <li>Then select the Advanced tab</li>
    <li>Choose (on the left) Cookies</li>
    <li>Then select ‘Accept cookies’ option</li>
</ol>
<h3>4.&nbsp;WHERE WE STORE YOUR PERSONAL DATA</h3>
<p>The data that we collect from you is currently transferred and stored on servers within the USA but may be
    transferred to, and stored at, a destination outside the United States of America («USA»). It may also be processed
    by staff operating outside the USA who work for us or for one of our suppliers. Such staff maybe engaged in, among
    other things, the fulfilment of your order and the provision of support services. By submitting your personal data,
    you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your
    data is treated securely and in accordance with this privacy policy.</p>
<p>All information you provide to us is stored on our secure servers. Where we have given you (or where you have chosen)
    a password which enables you to access certain parts of our site, you are responsible for keeping this password
    confidential. We ask you not to share a password with anyone.<br>
    Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our
    best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any
    transmission is at your own risk. Once we have received your information, we will use strict procedures and security
    features to try to prevent unauthorised access.</p>
<h3>5. USES MADE OF THE INFORMATION</h3>
<p>We use information held about you in the following ways:</p>
<ul>
    <li><span class="underline">Newsletters</span>: to send you by e-mail a confirmation in respect of any newsletters
        ordered by you.
    </li>
    <li>If you subscribe to receive our <strong>Get4click</strong> Newsletter, we will send this to you by e-mail.</li>
    <li>If you subscribe to receive newsletters from any of our <strong>Get4click</strong> Partners we shall forward
        your name and e-mail address to the relevant <strong>Get4click</strong> Partner(s) whose newsletter(s) you have
        requested.
    </li>
    <li><span class="underline">Vouchers</span>: to send you by e-mail the requested voucher codes and to send related
        confirmations and correspondence (e.g. reminders).
    </li>
    <li>To ensure that content from our site is presented in the most effective manner for you and for your computer.
    </li>
    <li>Where you have consented to be contacted for such purposes, to provide you with information, products or
        services that you request from us or which we feel may interest you. Where this may require further personal
        data, you will have provided this information by completing a form with the full knowledge of the usage of the
        aforementioned data.
    </li>
    <li>Subject to your consent, to compile site user statistics for the purpose of improving the internet services
        offered by us to provide you with updated information, and for the preparation of market surveys and research.
    </li>
    <li>To notify you about changes to our service.</li>
</ul>
<p>We do not without your express consent disclose information about identifiable individuals to third parties, but we
    may provide third parties with aggregate information about our users. We may also use such aggregate information to
    help advertisers reach the kind of audience they want to target (for example, women in SW1). We may make use of the
    personal data we have collected from you to enable us to comply with our advertisers’ wishes by displaying their
    advertisement to that target audience.</p>
<h3>6.&nbsp;DISCLOSURE OF YOUR INFORMATION</h3>
<p>We will disclose your information to the relevant <strong>Get4click</strong> Partners for the processing of your
    newsletter requests.We may disclose your personal information to any member of our group, which means our
    subsidiaries, our ultimate holding company and its subsidiaries.</p>
<p>Whilst you are visiting our Site, we may also collect statistical data about the use of the site for the purpose of
    providing a targeted offering of our services, advertising and market research. The data so collected may be shared
    with other members of our group.</p>
<p>We may also disclose your personal information to third parties:</p>
<ul>
    <li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the
        prospective seller or buyer of such business or assets.
    </li>
    <li>If we or substantially all of our assets are acquired by a third party, in which case personal data held by it
        about its customers will be one of the transferred assets.
    </li>
    <li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or
        in order to enforce or apply our <a href="/termsofuse/">terms of use</a> and other agreements; or to protect the
        rights, property, or safety of our company, our customers, or others. This includes exchanging information with
        other companies and organisations for the purposes of fraud protection and credit risk reduction.
    </li>
</ul>
<h4>Summary</h4>
<p>We will share data about you with members of our group. Save for the exceptions stated above, data will not be shared
    with third parties unless we have received your express consent or unless it is necessary for the purpose of
    performing our obligations to you. Our Site may, from time to time, contain links to and from the websites of our
    partner networks, advertisers and affiliates (including the Get4click Partners). If you follow a link to any of
    these websites, please note that these websites have their own privacy policies and that we do not accept any
    responsibility or liability for these policies. Please check these policies before you submit any personal data to
    these websites.</p>
<h3>7. CHANGES TO OUR PRIVACY POLICY</h3>
<p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate,
    notified to you by e-mail.</p>
<h3>8.&nbsp;CONTACT</h3>
<p>If you have any questions about our use of your information, you wish to let us know about changes to your personal
    details held by us, or if at any time you wish to stop receiving information from us or third parties, then please
    email us at <a href="mailto:info@get4click.com">info@get4click.com</a> or write to us at</p>
<p>
    GET4CLICK Inc.<br><br>
    Ter&lt;--Telephone: +1 617-580-3545--&gt;<br><br>
    Registered Office:210 Broadway, Suite 201<br><br>
    Cambridge, MA 02139
</p>
<p>Questions, comments and requests regarding this privacy policy are also welcomed and should be submitted to the above
    address.</p>
