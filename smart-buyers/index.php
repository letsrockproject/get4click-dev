<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage('SMART_BUYERS'));
?>
<?
$APPLICATION->IncludeComponent('letsrock:welcome', 'smart-buyer', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:job', '', [
    'TOP_SECTION' => IB_JOB_SMART_SHOPPERS_TOP_SECTION,
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:partners', 'market-partners', ['IBLOCK_ID' => IB_MARKET_PARTNER]);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>