<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Get4Click");
?>
<?
$APPLICATION->IncludeComponent('letsrock:welcome', '', [
    'MAIN' => 'Y'
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:job', '', [
    'BOTTOM_SECTION' => IB_JOB_MAIN_BOTTOM_SECTION,
    'TOP_SECTION' => IB_JOB_MAIN_TOP_SECTION,
    'MAIN' => 'Y'
]);
?>
<?
$APPLICATION->IncludeComponent('letsrock:tasks', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:partners', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:histories', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:comments', '', []);
?>
<?
$APPLICATION->IncludeComponent('letsrock:request', '', []);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>